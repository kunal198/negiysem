//
//  DKCamera.swift
//  DKCameraDemo
//
//  Created by ZhangAo on 15/8/30.
//  Copyright (c) 2015年 ZhangAo. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMotion



public class DKCameraPassthroughView: UIView
{
    public override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView?
    {
        let hitTestingView = super.hitTest(point, withEvent: event)
        return hitTestingView == self ? nil : hitTestingView
    }
}

public class DKCamera: UIViewController
{
    
    public class func checkCameraPermission(handler: (granted: Bool) -> Void)
    {
        func hasCameraPermission() -> Bool
        {
            return AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) == .Authorized
        }
        
        func needsToRequestCameraPermission() -> Bool
        {
            return AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) == .NotDetermined
        }
        
        hasCameraPermission() ? handler(granted: true) : (needsToRequestCameraPermission() ?
            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { granted in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    hasCameraPermission() ? handler(granted: true) : handler(granted: false)
                })
            }) : handler(granted: false))
    }
    
    public var didCancel: (() -> Void)?
    public var didFinishCapturingImage: ((image: UIImage) -> Void)?
    
    public var cameraOverlayView: UIView?
        {
        didSet
        {
            if let cameraOverlayView = cameraOverlayView
            {
                self.view.addSubview(cameraOverlayView)
            }
        }
    }
    
    /// The flashModel will to be remembered to next use.
    public var flashMode:AVCaptureFlashMode!
        {
        didSet {
            self.updateFlashButton()
            self.updateFlashMode()
            self.updateFlashModeToUserDefautls(self.flashMode)
        }
    }
    
    public class func isAvailable() -> Bool
    {
        return UIImagePickerController.isSourceTypeAvailable(.Camera)
    }
    
    /// Determines whether or not the rotation is enabled.
    public var allowsRotate = false
    
    public let captureSession = AVCaptureSession()
    public var previewLayer: AVCaptureVideoPreviewLayer!
    private var beginZoomScale: CGFloat = 1.0
    private var zoomScale: CGFloat = 1.0
    
    public var currentDevice: AVCaptureDevice?
    public var captureDeviceFront: AVCaptureDevice?
    public var captureDeviceBack: AVCaptureDevice?
    private weak var stillImageOutput: AVCaptureStillImageOutput?
    
    public var contentView = UIView()
    
    public var originalOrientation: UIDeviceOrientation!
    public var currentOrientation: UIDeviceOrientation!
    public let motionManager = CMMotionManager()
    
    public lazy var flashButton: UIButton = {
        let flashButton = UIButton()
        flashButton.addTarget(self, action: #selector(DKCamera.switchFlashMode), forControlEvents: .TouchUpInside)
        
        return flashButton
    }()
    public var cameraSwitchButton: UIButton!
    public var newImage = UIImage(named: "")
    public var MyProfileButton: UIButton!
    public var MyPostsButton: UIButton!
    public var MyBestPostsButton: UIButton!
    
    
    
    override public func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setupDevices()
        self.setupUI()
        self.beginSession()
        
        self.setupMotionManager()
        
        GoogleAnalyticsRegisterView()
    }
    
    
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "DKCameraView")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    
    
    public override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        
        UIApplication.sharedApplication().statusBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
        
        
        
        if ShareImageFromCamera == true
        {
           // print("new image clicked in voting = \(newImage)")
            
            let votingScreen = storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
           // votingScreen.newImage = newImage
           // self.navigationController?.presentViewController(votingScreen, animated: true, completion: nil)//(votingScreen, animated: true)
            self.navigationController?.pushViewController(votingScreen, animated: true)
            ShareImageFromCamera = false
        }
        
        
        if !self.captureSession.running {
            self.captureSession.startRunning()
        }
        
        if !self.motionManager.accelerometerActive {
            self.motionManager.startAccelerometerUpdatesToQueue(NSOperationQueue.currentQueue()!, withHandler: { accelerometerData, error in
                if error == nil
                {
                    let currentOrientation = accelerometerData!.acceleration.toDeviceOrientation() ?? self.currentOrientation
                    if self.originalOrientation == nil
                    {
                        self.initialOriginalOrientationForOrientation()
                        self.currentOrientation = self.originalOrientation
                    }
                    if let currentOrientation = currentOrientation where self.currentOrientation != currentOrientation
                    {
                        self.currentOrientation = currentOrientation
                        self.updateContentLayoutForCurrentOrientation()
                    }
                }
                else
                {
                    print("error while update accelerometer: \(error!.localizedDescription)", terminator: "")
                }
            })
        }
        
    }
    
    public override func viewDidLayoutSubviews()
    {
        super.viewDidLayoutSubviews()
        
        if self.originalOrientation == nil
        {
            self.contentView.frame = self.view.bounds
            self.previewLayer.frame = self.view.bounds
        }
    }
    
    public override func viewDidDisappear(animated: Bool)
    {
        super.viewDidDisappear(animated)
        
        self.captureSession.stopRunning()
        self.motionManager.stopAccelerometerUpdates()
    }
    
    override public func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    
    public func setupDevices()
    {
        let devices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo) as! [AVCaptureDevice]
        
        for device in devices
        {
            if device.position == .Back
            {
                self.captureDeviceBack = device
            }
            
            if device.position == .Front
            {
                self.captureDeviceFront = device
            }
        }
        
        self.currentDevice = self.captureDeviceBack ?? self.captureDeviceFront
    }
    
    let bottomView = UIView()
    
    public func setupUI()
    {
        self.view.backgroundColor = UIColor.blackColor()
        self.view.addSubview(self.contentView)
        self.contentView.backgroundColor = UIColor.clearColor()
       // self.contentView.frame = self.view.bounds
        self.contentView.bounds.size = CGSize(width: self.view.bounds.width, height:418)
        
        
        var leftSwipe = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipes:"))
        var rightSwipe = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipes:"))
         var UpToDownSwipe = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipes:"))
        
        leftSwipe.direction = .Left
        rightSwipe.direction = .Right
        UpToDownSwipe.direction = .Down
        
        self.contentView.addGestureRecognizer(leftSwipe)
        self.contentView.addGestureRecognizer(rightSwipe)
        self.contentView.addGestureRecognizer(UpToDownSwipe)
        
        let bottomViewHeight: CGFloat = 150
        bottomView.bounds.size = CGSize(width: contentView.bounds.width, height: bottomViewHeight)
        bottomView.frame.origin = CGPoint(x: 0, y: contentView.bounds.height - bottomViewHeight)
        bottomView.autoresizingMask = [.FlexibleWidth, .FlexibleTopMargin]
        //bottomView.backgroundColor = UIColor(white: 0, alpha: 0.4)
        bottomView.backgroundColor = UIColor(red: 246.0/255, green: 246.0/255, blue: 246.0/255, alpha: 1.0)
        contentView.addSubview(bottomView)
        
    /*    // switch button
        let cameraSwitchButton: UIButton = {
            let cameraSwitchButton = UIButton()
            cameraSwitchButton.addTarget(self, action: #selector(DKCamera.switchCamera), forControlEvents: .TouchUpInside)
            cameraSwitchButton.setImage(DKCameraResource.cameraSwitchImage(), forState: .Normal)
            cameraSwitchButton.sizeToFit()
            
            return cameraSwitchButton
        }()
        
        
        
        cameraSwitchButton.frame.origin = CGPoint(x: bottomView.bounds.width - cameraSwitchButton.bounds.width - 15,
                                                  y: (bottomView.bounds.height - cameraSwitchButton.bounds.height) / 2)
        cameraSwitchButton.autoresizingMask = [.FlexibleLeftMargin, .FlexibleTopMargin, .FlexibleBottomMargin]
        bottomView.addSubview(cameraSwitchButton)
        self.cameraSwitchButton = cameraSwitchButton */
        
        
        
      /*  // cancel button ----- Not Needed for close btn
        let cancelButton: UIButton = {
            let cancelButton = UIButton()
            cancelButton.addTarget(self, action: #selector(DKCamera.dismiss), forControlEvents: .TouchUpInside)
            cancelButton.setImage(DKCameraResource.cameraCancelImage(), forState: .Normal)
            cancelButton.sizeToFit()
            
            return cancelButton
        }()
        
        cancelButton.frame.origin =  CGPoint(x: bottomView.bounds.width - cancelButton.bounds.width - 15,y: (bottomView.bounds.height - cancelButton.bounds.height) / 2)
        cancelButton.autoresizingMask = [.FlexibleBottomMargin, .FlexibleLeftMargin]
        bottomView.addSubview(cancelButton) */
        
        
        
        // cancel button
        let myBestPostsButton: UIButton = {
            let myBestPostsButton = UIButton()
            myBestPostsButton.addTarget(self, action: #selector(DKCamera.HandleMyBestPosts), forControlEvents: .TouchUpInside)
            myBestPostsButton.setImage(DKCameraResource.cameraCancelImage(), forState: .Normal)
            myBestPostsButton.sizeToFit()
            
            return myBestPostsButton
        }()
        
        myBestPostsButton.frame.origin =  CGPoint(x: bottomView.bounds.width - myBestPostsButton.bounds.width - 15,y: (bottomView.bounds.height - myBestPostsButton.bounds.height) / 2)
        myBestPostsButton.autoresizingMask = [.FlexibleBottomMargin, .FlexibleLeftMargin]
        bottomView.addSubview(myBestPostsButton)
     
         self.MyBestPostsButton = myBestPostsButton
        
        
    
        
        // capture button
        let captureButton: UIButton = {
            
            class DKCaptureButton: UIButton {
                private override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
                    self.backgroundColor = UIColor.clearColor()
                    return true
                }
                
                private override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
                    self.backgroundColor = UIColor.clearColor()
                    return true
                }
                
                private override func endTrackingWithTouch(touch: UITouch?, withEvent event: UIEvent?) {
                    self.backgroundColor = nil
                }
                
                private override func cancelTrackingWithEvent(event: UIEvent?) {
                    self.backgroundColor = nil
                }
            }
            
            let captureButton = DKCaptureButton()
            captureButton.addTarget(self, action: #selector(DKCamera.takePicture), forControlEvents: .TouchUpInside)
            captureButton.setImage(DKCameraResource.cameraCaptureImage(), forState: .Normal)
            captureButton.bounds.size = CGSizeApplyAffineTransform(CGSize(width: 100,
                height: 100), CGAffineTransformMakeScale(0.9, 0.9))
            captureButton.layer.cornerRadius = captureButton.bounds.height / 2
            captureButton.layer.borderColor = UIColor.clearColor().CGColor
            captureButton.layer.borderWidth = 2
            captureButton.layer.masksToBounds = true
            
            return captureButton
        }()
        
        captureButton.center = CGPoint(x: bottomView.bounds.width / 2, y: bottomView.bounds.height / 2)
        captureButton.autoresizingMask = [.FlexibleLeftMargin, .FlexibleRightMargin]
        bottomView.addSubview(captureButton)
        
//        let mybutton = DKCaptureButton()
//            mybutton.addTarget(self, action: #selector(DKCamera.takePicture), forControlEvents: .TouchUpInside)
//                        mybutton.setImage(DKCameraResource.cameraCaptureImage(), forState: .Normal)
//                        mybutton.bounds.size = CGSizeApplyAffineTransform(CGSize(width: 200,
//                            height: 200), CGAffineTransformMakeScale(0.9, 0.9))
//                        mybutton.layer.cornerRadius = mybutton.bounds.height / 2
//                        mybutton.layer.borderColor = UIColor.clearColor().CGColor
//                        mybutton.layer.borderWidth = 2
//                        mybutton.layer.masksToBounds = true
//            
//                        return mybutton
//                    }()
//            
//                    mybutton.center = CGPoint(x: bottomView.bounds.width / 2, y: bottomView.bounds.height / 2)
//                    mybutton.autoresizingMask = [.FlexibleLeftMargin, .FlexibleRightMargin]
//                    bottomView.addSubview(mybutton)

        
        
        
       /* // cancel button
        let cancelButton: UIButton = {
            let cancelButton = UIButton()
            cancelButton.addTarget(self, action: #selector(DKCamera.dismiss), forControlEvents: .TouchUpInside)
            cancelButton.setImage(DKCameraResource.cameraCancelImage(), forState: .Normal)
            cancelButton.sizeToFit()
            
            return cancelButton
        }()
        
        cancelButton.frame.origin = CGPoint(x: contentView.bounds.width - cancelButton.bounds.width - 15, y: 25)
        cancelButton.autoresizingMask = [.FlexibleBottomMargin, .FlexibleLeftMargin]
        contentView.addSubview(cancelButton)*/
        
        
        
        // switch button
        let cameraSwitchButton: UIButton = {
            let cameraSwitchButton = UIButton()
            cameraSwitchButton.addTarget(self, action: #selector(DKCamera.switchCamera), forControlEvents: .TouchUpInside)
            cameraSwitchButton.setImage(DKCameraResource.cameraSwitchImage(), forState: .Normal)
            cameraSwitchButton.sizeToFit()
            
            return cameraSwitchButton
        }()
        
        
        
        cameraSwitchButton.frame.origin = CGPoint(x: contentView.bounds.width - myBestPostsButton.bounds.width - 15, y: 20)

        cameraSwitchButton.autoresizingMask = [.FlexibleLeftMargin, .FlexibleTopMargin, .FlexibleBottomMargin]
        contentView.addSubview(cameraSwitchButton)
        self.cameraSwitchButton = cameraSwitchButton
      
        
        
        
        
        
        
        // My Profile button
        let MyProfileButton: UIButton = {
            let MyProfileButton = UIButton()
            MyProfileButton.addTarget(self, action: #selector(HandleMyProfile), forControlEvents: .TouchUpInside)
            MyProfileButton.setImage(DKCameraResource.cameraProfileImage(), forState: .Normal)
            MyProfileButton.sizeToFit()
            
            return MyProfileButton
        }()
        
        
        
        MyProfileButton.frame.origin = CGPoint(x: contentView.bounds.width/2 - 23, y: 20)
        
        MyProfileButton.autoresizingMask = [.FlexibleLeftMargin, .FlexibleTopMargin, .FlexibleBottomMargin]
        contentView.addSubview(MyProfileButton)
        self.MyProfileButton = MyProfileButton
        
        

        
        // profile button
        let MyPostsButton: UIButton = {
            let MyPostsButton = UIButton()
            MyPostsButton.addTarget(self, action: #selector(DKCamera.HandleMyPosts), forControlEvents: .TouchUpInside)
            MyPostsButton.setImage(DKCameraResource.cameraPostsImage(), forState: .Normal)
            //ProfileButton.setTitle("Profile", forState: UIControlState.Normal)
            MyPostsButton.sizeToFit()
            
            return MyPostsButton
        }()
        
        MyPostsButton.frame.origin = CGPoint(x: 20, y: (bottomView.bounds.height - cameraSwitchButton.bounds.height) / 2)
        MyPostsButton.autoresizingMask = [.FlexibleBottomMargin, .FlexibleLeftMargin]
        bottomView.addSubview(MyPostsButton)
        
        
        self.MyPostsButton = MyPostsButton
        
        
        
        self.flashButton.frame.origin = CGPoint(x: 5, y: 20)
        contentView.addSubview(self.flashButton)
        
        contentView.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(DKCamera.handleZoom(_:))))
        contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(DKCamera.handleFocus(_:))))
        
        
        
    }
    
    
    
    func handleSwipe(sender: UISwipeGestureRecognizer)
    {
        print(sender.direction)
    }
    
    
    
    func handleSwipes(sender:UISwipeGestureRecognizer)
    {
        if (sender.direction == .Left)
        {
           // print("Swipe Left")
            
            myBestPostsBool = true
            self.didCancel?()
         // self.dismissViewControllerAnimated(false, completion: nil)
        }
        
        if (sender.direction == .Right)
        {
           // print("Swipe Right")
            
            myPostsBool = true
            self.didCancel?()
            //self.dismissViewControllerAnimated(false, completion: nil)

           
        }
        
        
        if (sender.direction == .Down)
        {
          //  print("swipe to down")
            
            myProfileBool = true
            self.didCancel?()
            //self.dismissViewControllerAnimated(false, completion: nil)

        }
    }
    
    
     public override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
    
    }
    
    // MARK: - Callbacks
    
    internal func dismiss() {
        self.didCancel?()
    }
    
    public func takePicture() {
        let authStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        if authStatus == .Denied {
            return
        }
        
        if let stillImageOutput = self.stillImageOutput {
            dispatch_async(dispatch_get_global_queue(0, 0), {
                let connection = stillImageOutput.connectionWithMediaType(AVMediaTypeVideo)
                
                if connection == nil
                {
                    return
                }
                
                connection.videoOrientation = self.currentOrientation.toAVCaptureVideoOrientation()
                connection.videoScaleAndCropFactor = self.zoomScale
                
                stillImageOutput.captureStillImageAsynchronouslyFromConnection(connection, completionHandler: { (imageDataSampleBuffer, error: NSError?) -> Void in
                    
                    if error == nil
                    {
                        let imageData =
                            AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                        
                        
                        if let didFinishCapturingImage = self.didFinishCapturingImage, takenImage = UIImage(data: imageData) {
                    
                            let cropTakenImage = UIImage(CGImage: takenImage.CGImage!, scale: 1, orientation: .Right)
                            
                            didFinishCapturingImage(image: cropTakenImage)
    
                        }
                    }
                    else
                    {
                        print("error while capturing still image: \(error!.localizedDescription)", terminator: "")
                    }
                })
            })
        }
        
    }
    
    // MARK: - Handles Zoom
    
    public func handleZoom(gesture: UIPinchGestureRecognizer)
    {
        if gesture.state == .Began
        {
            self.beginZoomScale = self.zoomScale
        } else if gesture.state == .Changed {
            self.zoomScale = min(4.0, max(1.0, self.beginZoomScale * gesture.scale))
            CATransaction.begin()
            CATransaction.setAnimationDuration(0.025)
            self.previewLayer.setAffineTransform(CGAffineTransformMakeScale(self.zoomScale, self.zoomScale))
            CATransaction.commit()
        }
    }
    
    // MARK: - Handles Focus
    
    public func handleFocus(gesture: UITapGestureRecognizer) {
        if let currentDevice = self.currentDevice where currentDevice.focusPointOfInterestSupported {
            let touchPoint = gesture.locationInView(self.view)
            self.focusAtTouchPoint(touchPoint)
        }
    }
    
    // MARK: - Handles Switch Camera
    
    internal func switchCamera() {
        self.currentDevice = self.currentDevice == self.captureDeviceBack ?
            self.captureDeviceFront : self.captureDeviceBack
        
        self.setupCurrentDevice()
    }
    
    
    // MARK: - Handles My Posts
    internal func HandleMyPosts()
    {
     //   print("my posts btn clicked")
        myPostsBool = true
        self.didCancel?()
//        let left = self.storyboard!.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
//       self.navigationController?.pushViewController(left, animated: true)
       // self.presentViewController(left, animated:true, completion: nil)
    }
    
    
    // MARK: - Handles My profile button
     func HandleMyProfile()
    {
       // print("my profile btn clicked")
        
         myProfileBool = true
        self.didCancel?()
//        let left = self.storyboard!.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
//        self.navigationController?.pushViewController(left, animated: true)

    }
    
    
    // MARK: - Handles My Best Posts button
    internal func HandleMyBestPosts()
    {
       // print("my best posts btn clicked")
        myBestPostsBool = true
         self.didCancel?()
//        let left = self.storyboard!.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
//        self.navigationController?.pushViewController(left, animated: true)
    }
    
    
    
    // MARK: - Handles Flash
    
    internal func switchFlashMode()
    {
        switch self.flashMode! {
        case .Auto:
            self.flashMode = .Off
        case .On:
            self.flashMode = .Auto
        case .Off:
            self.flashMode = .On
        }
    }
    
    public func flashModeFromUserDefaults() -> AVCaptureFlashMode {
        let rawValue = NSUserDefaults.standardUserDefaults().integerForKey("DKCamera.flashMode")
        return AVCaptureFlashMode(rawValue: rawValue)!
    }
    
    public func updateFlashModeToUserDefautls(flashMode: AVCaptureFlashMode) {
        NSUserDefaults.standardUserDefaults().setInteger(flashMode.rawValue, forKey: "DKCamera.flashMode")
    }
    
    public func updateFlashButton() {
        struct FlashImage {
            
            static let images = [
                AVCaptureFlashMode.Auto : DKCameraResource.cameraFlashAutoImage(),
                AVCaptureFlashMode.On : DKCameraResource.cameraFlashOnImage(),
                AVCaptureFlashMode.Off : DKCameraResource.cameraFlashOffImage()
            ]
            
        }
        let flashImage: UIImage = FlashImage.images[self.flashMode]!
        
        self.flashButton.setImage(flashImage, forState: .Normal)
        self.flashButton.sizeToFit()
    }
    
    // MARK: - Capture Session
    public func beginSession()
    {
        self.captureSession.sessionPreset = AVCaptureSessionPresetPhoto
        
        self.setupCurrentDevice()
        
        let stillImageOutput = AVCaptureStillImageOutput()
        if self.captureSession.canAddOutput(stillImageOutput)
        {
            self.captureSession.addOutput(stillImageOutput)
            self.stillImageOutput = stillImageOutput
        }
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.previewLayer.frame = self.view.bounds
        
        let rootLayer = self.view.layer
        rootLayer.masksToBounds = true
        rootLayer.insertSublayer(self.previewLayer, atIndex: 0)
    }
    
    public func setupCurrentDevice()
    {
        if let currentDevice = self.currentDevice
        {
            
            if currentDevice.flashAvailable
            {
                self.flashButton.hidden = false
                self.flashMode = self.flashModeFromUserDefaults()
            }
            else
            {
                self.flashButton.hidden = true
            }
            
            for oldInput in self.captureSession.inputs as! [AVCaptureInput]
            {
                self.captureSession.removeInput(oldInput)
            }
            
            let frontInput = try? AVCaptureDeviceInput(device: self.currentDevice)
            if self.captureSession.canAddInput(frontInput)
            {
                self.captureSession.addInput(frontInput)
            }
            
            try! currentDevice.lockForConfiguration()
            if currentDevice.isFocusModeSupported(.ContinuousAutoFocus)
            {
                currentDevice.focusMode = .ContinuousAutoFocus
            }
            
            if currentDevice.isExposureModeSupported(.ContinuousAutoExposure)
            {
                currentDevice.exposureMode = .ContinuousAutoExposure
            }
            
            currentDevice.unlockForConfiguration()
        }
    }
    
    public func updateFlashMode()
    {
        if let currentDevice = self.currentDevice
            where currentDevice.flashAvailable && currentDevice.isFlashModeSupported(self.flashMode)
        {
            try! currentDevice.lockForConfiguration()
            currentDevice.flashMode = self.flashMode
            currentDevice.unlockForConfiguration()
        }
    }
    
    public func focusAtTouchPoint(touchPoint: CGPoint) {
        
        func showFocusViewAtPoint(touchPoint: CGPoint) {
            
            struct FocusView {
                static let focusView: UIView = {
                    let focusView = UIView()
                    let diameter: CGFloat = 100
                    focusView.bounds.size = CGSize(width: diameter, height: diameter)
                    focusView.layer.borderWidth = 2
                    focusView.layer.cornerRadius = diameter / 2
                    focusView.layer.borderColor = UIColor.clearColor().CGColor
                    
                    return focusView
                }()
            }
            FocusView.focusView.transform = CGAffineTransformIdentity
            FocusView.focusView.center = touchPoint
            self.view.addSubview(FocusView.focusView)
            UIView.animateWithDuration(0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.1,
                                       options: .CurveEaseInOut, animations: { () -> Void in
                                        FocusView.focusView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.6, 0.6)
            }) { (Bool) -> Void in
                FocusView.focusView.removeFromSuperview()
            }
        }
        
        if self.currentDevice == nil || self.currentDevice?.flashAvailable == false {
            return
        }
        
        let focusPoint = self.previewLayer.captureDevicePointOfInterestForPoint(touchPoint)
        
        showFocusViewAtPoint(touchPoint)
        
        if let currentDevice = self.currentDevice {
            try! currentDevice.lockForConfiguration()
            currentDevice.focusPointOfInterest = focusPoint
            currentDevice.exposurePointOfInterest = focusPoint
            
            currentDevice.focusMode = .ContinuousAutoFocus
            
            if currentDevice.isExposureModeSupported(.ContinuousAutoExposure) {
                currentDevice.exposureMode = .ContinuousAutoExposure
            }
            
            currentDevice.unlockForConfiguration()
        }
        
    }
    
    // MARK: - Handles Orientation
    
    public override func shouldAutorotate() -> Bool
    {
        return true
    }
    
    public func setupMotionManager() {
        self.motionManager.accelerometerUpdateInterval = 0.5
        self.motionManager.gyroUpdateInterval = 0.5
    }
    
    public func initialOriginalOrientationForOrientation()
    {
        self.originalOrientation = UIApplication.sharedApplication().statusBarOrientation.toDeviceOrientation()
        if let connection = self.previewLayer.connection
        {
            connection.videoOrientation = self.originalOrientation.toAVCaptureVideoOrientation()
        }
    }
    
    public func updateContentLayoutForCurrentOrientation()
    {
        let newAngle = self.currentOrientation.toAngleRelativeToPortrait() - self.originalOrientation.toAngleRelativeToPortrait()
        
        if self.allowsRotate
        {
            var contentViewNewSize: CGSize!
            let width = self.view.bounds.width
            let height = self.view.bounds.height
            if UIDeviceOrientationIsLandscape(self.currentOrientation)
            {
                contentViewNewSize = CGSize(width: max(width, height), height: min(width, height))
            }
            else
            {
                contentViewNewSize = CGSize(width: min(width, height), height: max(width, height))
            }
            
            
            
            print("contentViewNewSize=\(contentViewNewSize)")
            
            UIView.animateWithDuration(0.2)
            {
                self.contentView.bounds.size = contentViewNewSize
                self.contentView.transform = CGAffineTransformMakeRotation(newAngle)
            }
        }
        else
        {
            let rotateAffineTransform = CGAffineTransformRotate(CGAffineTransformIdentity, newAngle)
            
            UIView.animateWithDuration(0.2)
            {
                self.flashButton.transform = rotateAffineTransform
                self.cameraSwitchButton.transform = rotateAffineTransform
                self.MyProfileButton.transform = rotateAffineTransform
                self.MyPostsButton.transform = rotateAffineTransform
                self.MyBestPostsButton.transform = rotateAffineTransform
            //    self.cameraSwitchButton.transform = rotateAffineTransform
            }
        }
    }
    
}

// MARK: - Utilities

public extension UIInterfaceOrientation {
    
    func toDeviceOrientation() -> UIDeviceOrientation {
        switch self {
        case .Portrait:
            return .Portrait
        case .PortraitUpsideDown:
            return .PortraitUpsideDown
        case .LandscapeRight:
            return .LandscapeLeft
        case .LandscapeLeft:
            return .LandscapeRight
        default:
            return .Portrait
        }
    }
}

public extension UIDeviceOrientation {
    
    func toAVCaptureVideoOrientation() -> AVCaptureVideoOrientation {
        switch self {
        case .Portrait:
            return .Portrait
        case .PortraitUpsideDown:
            return .PortraitUpsideDown
        case .LandscapeRight:
            return .LandscapeLeft
        case .LandscapeLeft:
            return .LandscapeRight
        default:
            return .Portrait
        }
    }
    
    func toInterfaceOrientationMask() -> UIInterfaceOrientationMask {
        switch self {
        case .Portrait:
            return .Portrait
        case .PortraitUpsideDown:
            return .PortraitUpsideDown
        case .LandscapeRight:
            return .LandscapeLeft
        case .LandscapeLeft:
            return .LandscapeRight
        default:
            return .Portrait
        }
    }
    
    func toAngleRelativeToPortrait() -> CGFloat
    {
        switch self
        {
        case .Portrait:
            return 0
        case .PortraitUpsideDown:
            return CGFloat(M_PI)
        case .LandscapeRight:
            return CGFloat(-M_PI_2)
        case .LandscapeLeft:
            return CGFloat(M_PI_2)
        default:
            return 0
        }
    }
    
}

//public extension CMAcceleration {
//    func toDeviceOrientation() -> UIDeviceOrientation? {
//        if self.x >= 0.75 {
//            return .LandscapeRight
//        } else if self.x <= -0.75 {
//            return .LandscapeLeft
//        } else if self.y <= -0.75 {
//            return .Portrait
//        } else if self.y >= 0.75 {
//            return .PortraitUpsideDown
//        } else {
//            return nil
//        }
//    }
//}


public extension CMAcceleration {
    func toDeviceOrientation() -> UIDeviceOrientation? {
        if self.x >= 0.75 {
            return .LandscapeRight
        } else if self.x <= -0.75 {
            return .LandscapeLeft
        } else if self.y <= -0.75 {
            return .Portrait
        } else if self.y >= 0.75 {
            return .PortraitUpsideDown
        } else {
            return nil
        }
    }
}

// MARK: - Rersources

public extension NSBundle {
    
    class func cameraBundle() -> NSBundle {
        let assetPath = NSBundle(forClass: DKCameraResource.self).resourcePath!
        return NSBundle(path: (assetPath as NSString).stringByAppendingPathComponent("DKCameraResource.bundle"))!
    }
    
}

public class DKCameraResource {
    
    public class func imageForResource(name: String) -> UIImage {
        let bundle = NSBundle.cameraBundle()
        let imagePath = bundle.pathForResource(name, ofType: "png", inDirectory: "Images")
        let image = UIImage(contentsOfFile: imagePath!)
        return image!
    }
    
    class func cameraCancelImage() -> UIImage {
        return imageForResource("Artboard-1-copy-5")
    }
    
    class func cameraFlashOnImage() -> UIImage {
        return imageForResource("camera_flash_on")
    }
    
    class func cameraFlashAutoImage() -> UIImage {
        return imageForResource("camera_flash_auto")
    }
    
    class func cameraFlashOffImage() -> UIImage {
        return imageForResource("camera_flash_off")
    }
    
    class func cameraSwitchImage() -> UIImage {
        return imageForResource("Artboard-1-copy-8")
    }
    
    
    
    class func cameraCaptureImage() -> UIImage {
        return imageForResource("f1")
    }
    
    class func cameraPostsImage() -> UIImage {
        return imageForResource("Artboard-1-copy-3")
    }
    
    
    
    class func cameraProfileImage() -> UIImage {
        return imageForResource("Artboard-1-copy-7")
    }
}

