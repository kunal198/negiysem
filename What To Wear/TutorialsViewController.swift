//
//  TutorialsViewController.swift
//  What To Wear
//
//  Created by brst on 8/20/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class TutorialsViewController: UIViewController,UIScrollViewDelegate
{

     //MARK:- VARIABLES DECLARATION
     var BestPostStr = String()
     var newImage = UIImage(named: "")
    var DescriptionBtnCheck = String()
    var sizeHeight = UIScreen.mainScreen().bounds.height
    var frame: CGRect = CGRectMake(0, 0, 0, 0)
    var index:Int = Int()
    var imagesArray = [UIColor]()
    var BackgroundImagesArray = [String]()
    var SliderImagesArray =  [String]()
    var frontImageArray = [String]()
    var x_scrollView = CGFloat()
    var y_scrollView = CGFloat()
    var z_scrollView = CGFloat()
    var contentSrollViewWidth:CGFloat = CGFloat()
    var contentScrollViewHeight:CGFloat = CGFloat()
    
     //MARK:- OUTLETS DECLARATION
    @IBOutlet weak var NextLbl: UILabel!
    @IBOutlet var ScrollImage: UIImageView!
    @IBOutlet var ScrollView: UIScrollView!
    
    
    
    
    
    //MARK:- VIEW-DID-LOAD METHOD
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
////////////////////////////////////////// GoogleAnalyticsRegisterView STARTS ////////////////////////////////////////////////
        
        GoogleAnalyticsRegisterView()
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
        
        
        
//////////////////////////////////////////////////////////// Localized Strings /////////////////////////////////////////////////////////////////////
        
        self.NextLbl.text = NSLocalizedString("NEXT_TITLE", comment: "The next btn title")
        
////////////////////////////////////////////////////////////////////////// ENDS //////////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
        
        
        
        
//////////////////////////////////////////////////////////// Scroll View Settings /////////////////////////////////////////////////////////////////////
        
        // imagesArray = ["Instagram Icon","Instagram Icon","Instagram Icon","Instagram Icon"]
        imagesArray = [UIColor.clearColor(),UIColor.clearColor(),UIColor.clearColor(),UIColor.clearColor()]
        BackgroundImagesArray = ["WhiteBackground Image","WhiteBackground Image","WhiteBackground Image","WhiteBackground Image"]
        SliderImagesArray = ["Slider 1","Slider 2","Slider 3","Slider 4"]
        
        frontImageArray = ["Camera Icon","Camera Icon","Camera Icon","Camera Icon"]
        ScrollView.delegate = self
        ScrollView.scrollEnabled = true
        
        
        for index in 0..<imagesArray.count
        {
            
            frame.origin.x = self.ScrollView.frame.size.width * CGFloat(index)
            frame.size = self.ScrollView.frame.size
            self.ScrollView.pagingEnabled = true
            
            var subView = UIView(frame: frame)
            subView.backgroundColor = imagesArray[index]
            self.ScrollView .addSubview(subView)
            
            
            var imageView = UIImageView(frame: frame)
        
            var image = UIImage(named: BackgroundImagesArray[index]);
            imageView.image = image;
       
            self.ScrollView.addSubview(imageView);
            
            
            var imageViewFront = UIImageView()//(frame: CGRectMake(30, 50, 260,180))
            
            if sizeHeight == 736
            {
                imageViewFront = UIImageView(frame: CGRectMake(70, 50, 260,180))
            }
            else if sizeHeight == 667
            {
                imageViewFront = UIImageView(frame: CGRectMake(60, 50, 260,180))
            }
            else if sizeHeight == 568
            {
                imageViewFront = UIImageView(frame: CGRectMake(30, 50, 260,180))
            }

            var imageFront = UIImage(named: frontImageArray[index]);
            imageViewFront.image = imageFront;
            imageView.addSubview(imageViewFront);

            
            
            var upperLbl_yPosition = CGFloat()
            var upperLbl_width = self.view.frame.size.width
            upperLbl_yPosition = imageViewFront.frame.origin.y + imageViewFront.frame.size.height - 25
           var upperTextLbl = UILabel(frame: CGRectMake(0, upperLbl_yPosition, upperLbl_width,200))
          //  var upperTextLbl = UILabel(frame: CGRectMake(100, upperLbl_yPosition, 120,250))
           // upperTextLbl.backgroundColor = UIColor.blueColor()
            upperTextLbl.textColor = UIColor.blackColor()
            upperTextLbl.textAlignment = NSTextAlignment.Center
            
            
            
            //upperTextLbl.text = "Fotografini Cek\n VE\n Paylas"
            upperTextLbl.text = NSLocalizedString("INFO_TITLE", comment: "The information title")
            upperTextLbl.numberOfLines = 3;
            upperTextLbl.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
            imageView.addSubview(upperTextLbl);
            

            var ypos_Slider = CGFloat()
            ypos_Slider = upperTextLbl.frame.origin.y + upperTextLbl.frame.size.height
            var imageViewSlider = UIImageView()
            
            
             if sizeHeight == 736
            {
                imageViewSlider = UIImageView(frame: CGRectMake(130, ypos_Slider + 70, 140,20))
             }
            else if sizeHeight == 667
            {
                imageViewSlider = UIImageView(frame: CGRectMake(120, ypos_Slider + 50, 140,20))
            }
            else if sizeHeight == 568
            {
                 imageViewSlider = UIImageView(frame: CGRectMake(90, ypos_Slider, 140,20))
            }
            
            
            var imageSlider = UIImage(named: SliderImagesArray[index]);
            imageViewSlider.image = imageSlider;
            imageView.addSubview(imageViewSlider);
            
            
//            var lowerLbl_yPosition = CGFloat()
//            var lowerLbl_width = self.view.frame.size.width
//            lowerLbl_yPosition = upperTextLbl.frame.origin.y + upperTextLbl.frame.size.height + 50
//            var lowerTextLbl = UILabel(frame: CGRectMake(0, lowerLbl_yPosition, lowerLbl_width,150))
//            lowerTextLbl.textColor = UIColor.blackColor()
//            lowerTextLbl.textAlignment = NSTextAlignment.Center
//            lowerTextLbl.text = "Introyu Gec"
//            lowerTextLbl.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
//            imageView.addSubview(lowerTextLbl);
//            
//            
//            let btn: UIButton = UIButton(frame: CGRectMake(0, lowerLbl_yPosition, lowerLbl_width,150))
//            btn.backgroundColor = UIColor.clearColor()
//            btn.setTitle("", forState: UIControlState.Normal)
//            btn.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
//            imageView.addSubview(btn)
//            
//            imageView.userInteractionEnabled = true
//            imageView.bringSubviewToFront(btn)
//            
            
            //self.ScrollView.delaysContentTouches = NO;
            self.ScrollView.userInteractionEnabled = true;
            self.ScrollView.exclusiveTouch = true;
            self.ScrollView.delaysContentTouches = false
          
        }
        
        self.ScrollView.contentSize = CGSizeMake(self.ScrollView.frame.size.width * CGFloat(imagesArray.count), self.ScrollView.frame.size.height)
        
        
//////////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////////////////
        
    }

    
    //MARK:- GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "TutorialsScreen")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    
    
    //MARK:- VIEW-WILL-APPEAR
    override func viewWillAppear(animated: Bool)
    {
        
        
        
////////////////////////////////////////////////////// NAVBAR AND STATUS BAR /////////////////////////////////////////////////
        
        UIApplication.sharedApplication().statusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
 
        
        
        
        
        
        
 ////////////////////////////////////////////////////// SETTING BOOL VALUES METHODS /////////////////////////////////////////////////
        
        if imageCaptureBool == true
        {
//            let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
//            cameraScreen.newImage = imageCaptured
//            self.navigationController?.pushViewController(cameraScreen, animated: false)
            imageCaptureBool = false
        }
        
        
/////////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////////////// 
        

    }
 
 
    
    //MARK:- NEXT BUTTON ACTION
    func buttonAction(sender: UIButton!)
    {
//        var btnsendtag: UIButton = sender
       // print("btn clicked")
 
        let cameraScreen = storyboard?.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
        cameraScreen.DescriptionBtnCheck = "tutorialsscreen"
        self.navigationController?.pushViewController(cameraScreen, animated: true)
       // self.navigationController?.presentViewController(cameraScreen, animated: true, completion: nil)
    }
    
    
    //MARK:- NEXT BUTTON ACTION
    @IBAction func NextBtnAction(sender: AnyObject)
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
        let middle = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
        let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
        let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
        let bottom = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
        
        let snapContainer = SnapContainerViewController.containerViewWith(left,
                                                                          middleVC: middle,
                                                                          rightVC: right,
                                                                          topVC: top,
                                                                          bottomVC: nil)
        
        snapContainer.BestPostStr = "BestPostStr"
        
        middle.didCancel = { () in
          //  print("didCancel")
            
            if myProfileBool == true
            {
//                self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
//                dismissMyProfile = true
                
                let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                //cameraScreen.newImage = image
                self.navigationController?.pushViewController(cameraScreen, animated: true)
                
                
                
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
//                let middle = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
//                let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
//                let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
//                let bottom = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
//                
//                let snapContainer = SnapContainerViewController.containerViewWith(left,
//                                                                                  middleVC: middle,
//                                                                                  rightVC: right,
//                                                                                  topVC: nil,
//                                                                                  bottomVC: bottom)
//                
//              //  snapContainer.BestPostStr = "UsingMyProfile"
//                
//                self.navigationController?.pushViewController(snapContainer, animated: true)
                
                myProfileBool = false
            }
            
            if myPostsBool == true
            {
                
                //self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                //dismissMyPost = true
                
                
                let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                //votingScreen.newImage = newImage
                
                let transition = CATransition()
                transition.duration = 0.5  // kCAMediaTimingFunctionEaseInEaseOut
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionMoveIn
                transition.subtype = kCATransitionFromLeft
                self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)

                
                self.navigationController?.pushViewController(profileScreen, animated: true)
                
                
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
//                let middle = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
//                let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
//                let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
//                let bottom = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
//                
//                let snapContainer = SnapContainerViewController.containerViewWith(left,
//                                                                                  middleVC: middle,
//                                                                                  rightVC: right,
//                                                                                  topVC: top,
//                                                                                  bottomVC: nil)
//                
//                snapContainer.BestPostStr = "UsingMyPosts"
//
//                self.navigationController?.pushViewController(snapContainer, animated: true)
                
                
                 myPostsBool = false
            }
            
            
            if myBestPostsBool == true
            {
                //self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                //votingScreen.newImage = newImage
                profileScreen.BestPostStr = "BestPostStr"
                self.navigationController?.pushViewController(profileScreen, animated: true)
                
                
                
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
//                let middle = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
//                let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
//                let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
//                let bottom = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
//                
//                let snapContainer = SnapContainerViewController.containerViewWith(left,
//                                                                                  middleVC: middle,
//                                                                                  rightVC: right,
//                                                                                  topVC: top,
//                                                                                  bottomVC: nil)
//                
//                snapContainer.BestPostStr = "UsingMyBestPosts"
//                
//                self.navigationController?.pushViewController(snapContainer, animated: true)

                
                //dismissBestPost = true
                myBestPostsBool = false
            }
        
        }
        
        middle.didFinishCapturingImage = {(image: UIImage) in
      //   print("didFinishCapturingImage")
         
        self.newImage = image
        imageCaptured = image
        //self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
            
            let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
            cameraScreen.newImage = imageCaptured
          //  self.navigationController?.presentViewController(cameraScreen, animated: false, completion: nil)
             self.navigationController?.pushViewController(cameraScreen, animated: true)
             imageCaptureBool = true
         
         }

          self.navigationController?.pushViewController(snapContainer, animated: true)
      //   self.navigationController?.presentViewController(snapContainer, animated: true, completion: nil)
 
    }
    
    
    
     //MARK:- DIDRECEIVEMEMORYWARNING METHOD
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
