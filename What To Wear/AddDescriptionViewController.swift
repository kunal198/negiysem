//
//  AddDescriptionViewController.swift
//  What To Wear
//
//  Created by brst on 8/22/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class AddDescriptionViewController: UIViewController,UITextViewDelegate,UIAlertViewDelegate
{

    //MARK:- VARIABLES DECLARATION
     var newImage = UIImage(named: "")
     var DescriptionAddedStr = String()
    var DescriptionBtnCheck = String()
    var postDetailDict = NSMutableDictionary()
    var createPostDict = NSMutableDictionary()
    var dataModel = NSData()
    var savedUserID = String()
    
    //MARK:- OUTLETS DECLARATION
    @IBOutlet var ImageAdded: UIImageView!
    @IBOutlet var DescriptionAdded: UITextView!
    @IBOutlet var ShareView: UIView!
    @IBOutlet var DownloadView: UIView!
    @IBOutlet var CrossView: UIView!
    @IBOutlet var PlaceHolderTextView: UILabel!
    
    
    //MARK:- VIEW-DID-LOAD
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       GoogleAnalyticsRegisterView()
        
        var userSaved = String()
        userSaved = NSUserDefaults.standardUserDefaults().valueForKey("InstagramUser") as! String
        print("userSaved = \(userSaved)")
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        if userSaved != ""
        {
            if userSaved == "InstagramUser"
            {
                savedUserID = userDefaults.valueForKey("userID") as! String
                print("savedUserID=\(savedUserID)")
            }
            else
            {
                savedUserID = userDefaults.valueForKey("userID") as! String
                print("savedUserID=\(savedUserID)")
            }
            
        }

        
        self.PlaceHolderTextView.text =  NSLocalizedString("PLACEHOLDER_TEXT", comment: "The placeholder title")

        UIApplication.sharedApplication().statusBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
        self.DescriptionAdded.becomeFirstResponder()
        
        if newImage != nil
        {
            ImageAdded.image = newImage
        }
        else
        {
            ImageAdded.image = UIImage(named: "TableViewImage3")
        }

    }

    
    //MARK:- GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "AddDescriptionScreen")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    
    //MARK:- UITEXT-VIEW DELEGATES
    func textViewDidChange(textView: UITextView)
    {
       // print("text view count = \(textView.text)")
       NSUserDefaults.standardUserDefaults().setObject(textView.text, forKey: "TextDescrption")
        DescriptionAddedStr = textView.text
        
        if (textView.text.characters.count > 0)
        {
            PlaceHolderTextView.hidden = true
           // [_LblPlaceHolderTextView setHidden:YES];
        }
        else
        {
            PlaceHolderTextView.hidden = false
            self.PlaceHolderTextView.text =  NSLocalizedString("PLACEHOLDER_TEXT", comment: "The placeholder title")
            //[_LblPlaceHolderTextView setHidden:NO];
        }

        
        
        if textView.text.characters.count > 140
        {
            var alert = UIAlertView(title: "", message:  NSLocalizedString("ALERT_DESCRIPTION", comment: "The description restriction title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
            alert.show()
        }
        
        
    }
    
    
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool
    {
        if (text == "\n")
        {
             textView.resignFirstResponder()
            DescriptionBtnCheck = "DescriptionBtnCheck"
            DescriptionTxtViewBool = true
             self.dismissViewControllerAnimated(false, completion: nil)
        }
        
        
        return true
    }
    
    
    
    //MARK:- CANCEL-VIEW BTN ACTION
    @IBAction func CancelViewBtnAction(sender: AnyObject)
    {
        //print("cancel btn")
        DescriptionBtnCheck = "DescriptionBtnCheck"
        //DescriptionTxtViewBool = true
        camerafromDescription = true
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    
    //MARK:- IMAGE DOWNLOAD BTN ACTION
    @IBAction func ImageDownloadBtnAction(sender: AnyObject)
    {
        //print("image dwnload btn")
        
        UIImageWriteToSavedPhotosAlbum(self.ImageAdded.image!, self, #selector(self.imageSaved(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    
    //MARK:- IMAGE SAVED IN GALLERY METHOD
    func imageSaved(image: UIImage!, didFinishSavingWithError error: NSError?, contextInfo: AnyObject?)
    {
        if (error != nil)
        {
            //  print("error")
            
            var ErrorImage = UIAlertView(title: "", message: NSLocalizedString("ALERT_IMAGE_NOT_SAVED", comment: "The not saved image title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
            ErrorImage.show()
        }
        else
        {
            // print("image saved")
            
            var saveImage = UIAlertView(title: "", message: NSLocalizedString("ALERT_IMAGE_SAVED", comment: "The saved image title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
            saveImage.show()
        }
    }

    
    
    //MARK:- SHARE BTN ACTION
    @IBAction func ShareBtnAction(sender: AnyObject)
    {
       //print("share btn")
        
        CreateNewPostAPI()
        
    }

    
    
    //MARK:- Rotate ImageView w.r.t Orignal Image
    func fixOrientation(img:UIImage) -> UIImage
    {
        
        if (img.imageOrientation == UIImageOrientation.Up)
        {
            return img;
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale);
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.drawInRect(rect)
        
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return normalizedImage;
        
    }
    
    
    //MARK:- CreateNewPostAPI
    func CreateNewPostAPI()
    {
        
        let spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        
        
        
        var image:UIImage =  fixOrientation(ImageAdded.image!)
        
        // PhotoView.transform = CGAffineTransformMakeRotation(3.14/2);
        
        let imageData2 = UIImageJPEGRepresentation(image, 1.0)
        let base64String = imageData2!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)) // encode the image
        let strImage64 = base64String.stringByReplacingOccurrencesOfString("+", withString: "%2B") as String
        //  print("strImage64 = \(strImage64)")
        
        
        let post = NSString(format:"user_id=%@&img_para=%@&photo_description=%@",savedUserID,strImage64,self.DescriptionAdded.text)
        
        //   print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        let postLength = String(dataModel.length)
        let url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/post")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
                
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                    
                    self.createPostDict = jsonResult as! NSMutableDictionary
                    print("self.updateResultDict = \(self.createPostDict)")
                    
                    let statusStr = self.createPostDict.valueForKey("status") as! String
                    self.postDetailDict = self.createPostDict.valueForKey("data") as! NSMutableDictionary
                    
                    if statusStr == "success"
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            ShareImageFromCamera = true
                            self.dismissViewControllerAnimated(false, completion: nil)
                            
                            var array = NSMutableArray()
                            
                            array.addObject(self.postDetailDict)

                            let defaultsArray = NSUserDefaults.standardUserDefaults()
                            defaultsArray.setObject(array, forKey: "postDetailDict")

                            
                            
                        //    GetpostIDBool = true
                            
//                            NSUserDefaults .standardUserDefaults().setObject(self.postDetailDict.valueForKey("post_id") as! String, forKey: "post_id")
//                            
//                            //  votingScreen.newImage = imageCaptured
//                            votingScreen.postDetailDict = self.createPostDict.valueForKey("data") as! NSMutableDictionary
//                            self.navigationController?.pushViewController(votingScreen, animated: true)
                            
                            spinningIndicator.hide(true)
                        })
                        
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        })
                        
                    }
                    
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }

    
    //MARK:- DID-RECEIVE-MEMORY-WARNING
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
