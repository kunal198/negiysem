//
//  MyProfileViewController.swift
//  What To Wear
//
//  Created by brst on 8/22/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController,UITextFieldDelegate,UIAlertViewDelegate
{
    
    //MARK:- VARIABLES DECLARATION
    var DisConnectResultDict = NSMutableDictionary()
    var alertValue = UIAlertView()
    var dataModel = NSData()
    var savedFirstname = String()
    var savedLastName = String()
    var savedUserID = String()
    var savedUserSex = String()
    var savedEmail = String()
    var savedName = String()
    var savedImage = String()
    var logOutAlert = UIAlertView()
    var user_interested_in_male = Bool()
    var user_interested_in_female = Bool()
    var user_interested_in_maleStr = String()
    var user_interested_in_femaleStr = String()
    var user_sexStr = String()
    var fullNameBool = Bool()
    var updateResultDict = NSMutableDictionary()
    
    
    //MARK:- OUTLETS DECLARATION
    @IBOutlet weak var LogOutLbl: UILabel!
    @IBOutlet var DescriptionView: UIView!
    @IBOutlet var ShowNameSwitch: UISwitch!
    @IBOutlet var ShowOnlyNameImage: UIImageView!
    @IBOutlet var ShowOnlyNaleLbl: UILabel!
    @IBOutlet var ShowOnlyNameView: UIView!
    @IBOutlet var Addresslbl: UILabel!
    @IBOutlet var NameLbl: UILabel!
    @IBOutlet var ProfileImage: UIImageView!
    @IBOutlet var MaleSwitch: UISwitch!
    @IBOutlet var FemaleSwitch: UISwitch!
    @IBOutlet var GenderLbl: UILabel!
    @IBOutlet var FemaleLbl: UILabel!
    @IBOutlet var InterestedInLbl: UILabel!
    @IBOutlet var InterestedInView: UIView!
    @IBOutlet var WebUrlLbl: UILabel!
    @IBOutlet var WebUrlImage: UIImageView!
    @IBOutlet var AddWebUrlView: UIView!
    @IBOutlet var ConnectInstaImage: UIImageView!
    @IBOutlet var ConnectInstaSwitch: UISwitch!
    @IBOutlet var ConnectInstaLbl: UILabel!
    @IBOutlet var ConnectInstaView: UIView!
    @IBOutlet var WebTextField: UITextField!
    @IBOutlet var ProfileViewBtn: UIButton!
    @IBOutlet var logoutBtn: UIButton!
    @IBOutlet var ArrowBtnDown: UIButton!
    
    
    
    //MARK:- VIEW-DID-LOAD
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       GoogleAnalyticsRegisterView()
        
       self.alertValue.delegate = self
        
       LocalizedStrings()
        
    }

    
    //MARK:- LOCALIZATION STRINGS
    func LocalizedStrings()
    {
        self.ShowOnlyNaleLbl.text =  NSLocalizedString("SHOW_NAME", comment: "The show name title")
        self.ConnectInstaLbl.text =  NSLocalizedString("CONNECT_INSTAGRAM", comment: "The connect instagram title")
        self.InterestedInLbl.text =  NSLocalizedString("INSTERESTED_IN", comment: "The interested in title")
        self.GenderLbl.text =  NSLocalizedString("GENDER_TYPE_MALE", comment: "The gender male title")
        self.FemaleLbl.text =  NSLocalizedString("GENDER_TYPE_FEMALE", comment: "The gender female title")
        self.LogOutLbl.text =  NSLocalizedString("LOGOUT_TITLE", comment: "The logout title")
        self.Addresslbl.text =  NSLocalizedString("POSTS_TITLE", comment: "The posts title")
    }
    
    
    //MARK:- GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "MyProfileScreen")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    
    //MARK:- VIEW-WILL-APPEAR
    override func viewWillAppear(animated: Bool)
    {
        
        var userSaved = String()
        userSaved = NSUserDefaults.standardUserDefaults().valueForKey("InstagramUser") as! String
        
//        print("userSaved = \(userSaved)")
//        print("instaAccessToken = \(instaAccessToken)")
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        if userSaved != ""
        {
            if userSaved == "InstagramUser"
            {
                
                 ConnectInstaSwitch.enabled=false
                
                savedUserID = userDefaults.valueForKey("userID") as! String
                savedName = userDefaults.valueForKey("userName") as! String
                savedEmail = userDefaults.valueForKey("userMail") as! String
                savedImage = userDefaults.valueForKey("userImage") as! String
//                savedFirstname =  userDefaults.valueForKey("user_Firstname") as! String
//                savedLastName =  userDefaults.valueForKey("user_Surname") as! String
//                user_interested_in_maleStr =  userDefaults.valueForKey("user_interested_in_male") as! String
//                user_interested_in_femaleStr =  userDefaults.valueForKey("user_interested_in_female") as! String
//                user_sexStr =  userDefaults.valueForKey("user_sex") as! String
                
//                print("savedUserID = \(savedUserID) ")
//                print("savedName = \(savedName) ")
//                print("savedEmail = \(savedEmail) ")
//                print("savedImage = \(savedImage) ")
                
                
                
//                print("user_interested_in_maleStr = \(user_interested_in_maleStr) ")
//                print("user_interested_in_femaleStr = \(user_interested_in_femaleStr) ")
//                print("user_sexStr = \(user_sexStr) ")
                
                savedFirstname = savedName
                
                var userSaved = String()
                if let userwebsite = NSUserDefaults.standardUserDefaults().valueForKey("user_website")
                {
                    self.WebTextField.text = userwebsite as! String
                }


            }
            else if userSaved == "FacebookUser"
            {
                savedUserID = userDefaults.valueForKey("userID") as! String
                savedName = userDefaults.valueForKey("userName") as! String
                savedEmail = userDefaults.valueForKey("userMail") as! String
                savedImage = userDefaults.valueForKey("userImage") as! String
                savedFirstname = userDefaults.valueForKey("first_name") as! String
                savedLastName = userDefaults.valueForKey("last_name") as! String
                savedUserSex = userDefaults.valueForKey("user_sex") as! String
                
                if  let savedMale = NSUserDefaults.standardUserDefaults().valueForKey("user_interested_in_male")
                {
                    user_interested_in_maleStr = savedMale as! String
                }
                else
                {
                    MaleSwitch.setOn(false, animated: false)
                    MaleSwitch.thumbTintColor = UIColor.whiteColor()
                    MaleSwitch.onTintColor = UIColor.darkGrayColor()
                    
                    user_interested_in_male = false
                }
                
                if let savedFemale = NSUserDefaults.standardUserDefaults().valueForKey("user_interested_in_female")
                {
                    user_interested_in_femaleStr = savedFemale as! String
                }
                else
                {
                    FemaleSwitch.setOn(false, animated: false)
                    FemaleSwitch.thumbTintColor = UIColor.whiteColor()
                    FemaleSwitch.onTintColor = UIColor.darkGrayColor()
                    
                    user_interested_in_female = false
                }
                

                
                print("savedLastName = \(savedLastName) ")
                print("savedFirstname = \(savedFirstname) ")
                print("savedUserID = \(savedUserID) ")
                print("savedName = \(savedName) ")
                print("savedEmail = \(savedEmail) ")
                print("savedImage = \(savedImage) ")
                print("savedUserSex = \(savedUserSex) ")
                 print("user_interested_in_maleStr = \(user_interested_in_maleStr) ")
                 print("user_interested_in_femaleStr = \(user_interested_in_femaleStr) ")
                
                
               //if NSUserDefaults.standardUserDefaults().valueForKey("enabled_string") as! Bool != true
                if user_interested_in_maleStr == "1"
                {
                    MaleSwitch.setOn(true, animated: false)
                    MaleSwitch.thumbTintColor = UIColor(red: 236.0/255, green: 42.0/255, blue: 76.0/255, alpha: 1.0)
                    MaleSwitch.onTintColor = UIColor(red: 105.0/255, green: 27.0/255, blue: 41.0/255, alpha: 1.0)
                }
                else
                {
                    MaleSwitch.setOn(false, animated: false)
                    MaleSwitch.thumbTintColor = UIColor.whiteColor()
                    MaleSwitch.onTintColor = UIColor.darkGrayColor()
                }
                
                
                
                if user_interested_in_femaleStr == "1"
                {
                    FemaleSwitch.setOn(true, animated: false)
                    FemaleSwitch.thumbTintColor = UIColor(red: 236.0/255, green: 42.0/255, blue: 76.0/255, alpha: 1.0)
                    FemaleSwitch.onTintColor = UIColor(red: 105.0/255, green: 27.0/255, blue: 41.0/255, alpha: 1.0)
                }
                else
                {
                    FemaleSwitch.setOn(false, animated: false)
                    FemaleSwitch.thumbTintColor = UIColor.whiteColor()
                    FemaleSwitch.onTintColor = UIColor.darkGrayColor()
                }

                
                if let instaStatus = NSUserDefaults.standardUserDefaults().valueForKey("InstaConnected")
                {
                    
                    if instaStatus as! String == "InstaConnected"
                    {
                        ConnectInstaSwitch.setOn(true, animated: false)
                        ConnectInstaSwitch.thumbTintColor = UIColor(red: 236.0/255, green: 42.0/255, blue: 76.0/255, alpha: 1.0)
                        ConnectInstaSwitch.onTintColor = UIColor(red: 105.0/255, green: 27.0/255, blue: 41.0/255, alpha: 1.0)

                    }
                    else
                    {
                        ConnectInstaSwitch.setOn(false, animated: false)
                        ConnectInstaSwitch.thumbTintColor = UIColor.whiteColor()
                        ConnectInstaSwitch.onTintColor = UIColor.darkGrayColor()
                    }
                    
                }
                else
                {
                    ConnectInstaSwitch.setOn(false, animated: false)
                    ConnectInstaSwitch.thumbTintColor = UIColor.whiteColor()
                    ConnectInstaSwitch.onTintColor = UIColor.darkGrayColor()
                }
                
                
            }
            
        }
        else
        {
            print("user saved is nil")
        }

        
        

            self.NameLbl.text = savedName
            var imageURL: NSURL = NSURL(string: savedImage)!
            if let data : NSData = NSData(contentsOfURL: imageURL)
            {
                self.ProfileImage.image = UIImage(data: data)!
            }
        
        
        UIApplication.sharedApplication().statusBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
        logOutAlert.delegate = self
        
        ProfileImage.layer.borderWidth = 4.0
        ProfileImage.layer.masksToBounds = false
        ProfileImage.layer.borderColor = UIColor.whiteColor().CGColor
        ProfileImage.layer.cornerRadius = 13
        ProfileImage.layer.cornerRadius = ProfileImage.frame.size.height/2
        ProfileImage.clipsToBounds = true
        
      WebTextField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("WEB_URL", comment: "The logout title"), attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 13)!])

    }
    
    
    //MARK:- UITEXTFIELD DELEGATES
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        self.WebTextField.resignFirstResponder()
        ProfileUpdateAPI()
       // UIApplication.sharedApplication().openURL(NSURL(string:"http://www.facebook.com")!)
        //UIApplication.sharedApplication().openURL(NSURL(string: "http://" + self.WebTextField.text!)!)
        return true
    }
    
    
    //MARK:- PROFILE VIEW BTN ACTION
    @IBAction func ProfileViewBtnAction(sender: AnyObject)
    {
        //print("profile view btn")
        self.WebTextField.resignFirstResponder()
        let myPostsScreen = storyboard?.instantiateViewControllerWithIdentifier("MyPostsViewController") as! MyPostsViewController
        self.navigationController?.pushViewController(myPostsScreen, animated: true)
    }
    
    
    //MARK:- SHOWS NAME/FULLNAME SWITCH ACTION
    @IBAction func ShowsNameSwitchAction(sender: AnyObject)
    {
            if ShowNameSwitch.on
            {
               // print("Switch is on")
                self.NameLbl.text = savedName
                self.fullNameBool = true
                ShowNameSwitch.thumbTintColor = UIColor(red: 236.0/255, green: 42.0/255, blue: 76.0/255, alpha: 1.0)
                ShowNameSwitch.onTintColor = UIColor(red: 105.0/255, green: 27.0/255, blue: 41.0/255, alpha: 1.0)
                //  ShowNameSwitch.setOn(false, animated:true)
            }
            else
            {
                //print("Switch is off")
                self.NameLbl.text = savedFirstname
                self.fullNameBool = false
                //ShowNameSwitch.setOn(true, animated:true)
//                ShowNameSwitch.thumbTintColor = UIColor.whiteColor()
//                ShowNameSwitch.onTintColor = UIColor(red: 166.0/255, green: 168.0/255, blue: 171.0/255, alpha: 1.0)
                
                ShowNameSwitch.thumbTintColor = UIColor.whiteColor()
                ShowNameSwitch.onTintColor = UIColor.darkGrayColor()
            }
        
       UpdateFullNameAPI()
        
    }
    
    
    //MARK:- CONNECT WITH INSTAGRAM BTN ACTION
    @IBAction func ConnectInstaSwitchACtion(sender: AnyObject)
    {
        
        var userSaved = String()
        userSaved = NSUserDefaults.standardUserDefaults().valueForKey("InstagramUser") as! String
        
        print("userSaved = \(userSaved)")
    
        
        if userSaved != ""
        {
            if userSaved == "InstagramUser"
            {
                
                ConnectInstaSwitch.enabled=false
                
//                var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_ALREADY_CONNECT_INSTA", comment: "The already connet insta title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
//                alert.show()
            }
            else
            {
                if ConnectInstaSwitch.on
                {
                    print("connect insta switch is on")
                    
                    ConnectInstaSwitch.thumbTintColor = UIColor(red: 236.0/255, green: 42.0/255, blue: 76.0/255, alpha: 1.0)
                    ConnectInstaSwitch.onTintColor = UIColor(red: 105.0/255, green: 27.0/255, blue: 41.0/255, alpha: 1.0)
                    
                    let instagramScreen = storyboard?.instantiateViewControllerWithIdentifier("ConnectWithInstagramViewController") as! ConnectWithInstagramViewController
                    self.navigationController?.pushViewController(instagramScreen, animated: true)
                }
                else
                {
                    print("connect insta switch is offf")
                    ConnectInstaSwitch.thumbTintColor = UIColor.whiteColor()
                    ConnectInstaSwitch.onTintColor = UIColor.darkGrayColor()
                    
                    DisConnectWithInstagramAPI()
                }

            }
        }
    
    }
    
    
    
    //MARK:- DisConnectWithInstagramAPI
    func DisConnectWithInstagramAPI()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        //let savedUserID = NSUserDefaults.standardUserDefaults().valueForKey("userID") as! String
        
        var post = NSString(format:"user_id=%@",savedUserID)
        
        print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        var postLength = String(dataModel.length)
        var url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/unlinkinsta")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
                
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()
                        
                        
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                    
                    self.DisConnectResultDict = jsonResult as! NSMutableDictionary
                    print("self.DisConnectResultDict = \(self.DisConnectResultDict)")
                    
                    let statusStr = self.DisConnectResultDict.valueForKey("status") as! String
                    print("statusStr=\(statusStr)")
                    
                    if statusStr == "success"
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert  =  UIAlertView(title: "", message: NSLocalizedString("ALERT_DISCONNECT_TRUE", comment: "The disConnect true insta title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            //self.alertConnectTrue.tag = 22
                            alert.show()
                            
                            
                            NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "InstaConnected")
                            
                            let cookieJar : NSHTTPCookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
                            for cookie in cookieJar.cookies! as [NSHTTPCookie]
                            {
                                NSLog("cookie.domain = %@", cookie.domain)
                                
                                if cookie.domain == "www.instagram.com" ||
                                    cookie.domain == "api.instagram.com"
                                {
                                    
                                    cookieJar.deleteCookie(cookie)
                                }
                            }

                            
                            spinningIndicator.hide(true)
                        })
                        
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_DISCONNECT_FALSE", comment: "The disconnect false title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        })
                    }
                    
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }

       

    
    
    
    //MARK:- MALE SWITCH ACTION
    @IBAction func MaleSwitchAction(sender: AnyObject)
    {
            if MaleSwitch.on
            {
               // print("MaleSwitch is on")
                //self.NameLbl.text = savedName
                
                user_interested_in_male = true
                //print("user_interested_in_male = \(user_interested_in_male)")
                
                MaleSwitch.thumbTintColor = UIColor(red: 236.0/255, green: 42.0/255, blue: 76.0/255, alpha: 1.0)
                MaleSwitch.onTintColor = UIColor(red: 105.0/255, green: 27.0/255, blue: 41.0/255, alpha: 1.0)
            }
            else
            {
              //  print("MaleSwitch is off")
                
                user_interested_in_male = false
                //print("user_interested_in_male = \(user_interested_in_male)")
                
                MaleSwitch.thumbTintColor = UIColor.whiteColor()
                MaleSwitch.onTintColor = UIColor.darkGrayColor()
            }
            
            
            ProfileUpdateAPI()
       
    }
    
    
    //MARK:- FEMALE SWITCH ACTION
    @IBAction func FemaleSwitchAction(sender: AnyObject)
    {
            if FemaleSwitch.on
            {
                //print("FemaleSwitch is on")
                user_interested_in_female = true
            //   print("user_interested_in_female = \(user_interested_in_female)")
                FemaleSwitch.thumbTintColor = UIColor(red: 236.0/255, green: 42.0/255, blue: 76.0/255, alpha: 1.0)
                FemaleSwitch.onTintColor = UIColor(red: 105.0/255, green: 27.0/255, blue: 41.0/255, alpha: 1.0)
            }
            else
            {
               // print("FemaleSwitch is off")
                user_interested_in_female = false
                
                //print("user_interested_in_female = \(user_interested_in_female)")
                FemaleSwitch.thumbTintColor = UIColor.whiteColor()
                FemaleSwitch.onTintColor = UIColor.darkGrayColor()

            }
            
            
            ProfileUpdateAPI()
            FacebookLoginBool = false
    }
    
    
    
    //MARK:- ProfileUpdateAPI
    func ProfileUpdateAPI()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        var post = NSString(format:"user_id=%@&user_name=%@&user_surname=%@&user_sex=%@&user_interested_in_male=%@&user_interested_in_female=%@&user_website=%@",savedUserID,self.savedName,self.savedLastName,"",user_interested_in_male,user_interested_in_female,self.WebTextField.text!)
        
        print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        var postLength = String(dataModel.length)
        var url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/updateuser")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
             //
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()
                    
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {

                    self.updateResultDict = jsonResult as! NSMutableDictionary
                  //  print("self.updateResultDict = \(self.updateResultDict)")
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var status = String()
                        status = self.updateResultDict.valueForKey("status") as! String
                        
                        let defaults = NSUserDefaults.standardUserDefaults()
                        defaults.setObject(self.updateResultDict.valueForKey("user_name")!, forKey: "userName")
                        defaults.setObject(self.updateResultDict.valueForKey("user_sex")!, forKey: "user_sex")
                        defaults.setObject(self.updateResultDict.valueForKey("user_interested_in_female")!, forKey: "user_interested_in_female")
                        defaults.setObject(self.updateResultDict.valueForKey("user_interested_in_male")!, forKey: "user_interested_in_male")
                       NSUserDefaults.standardUserDefaults().setValue(self.updateResultDict.valueForKey("user_website") as! String, forKeyPath: "user_website")
                        
                        if status == "success"
                        {
                            self.alertValue =  UIAlertView(title: "", message: NSLocalizedString("ALERT_SUCCESS", comment: "The success title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            self.alertValue.tag = 2
                            self.alertValue.show()
                        }
                        else
                        {
                            var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_NOT_SUCCESS", comment: "The not success title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            alert.show()
                        }

                        
                        spinningIndicator.hide(true)
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }

    
    
    
    //MARK:- UpdateFullNameAPI
    func UpdateFullNameAPI()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        var post = NSString(format:"user_id=%@&full_name=%@",savedUserID,self.fullNameBool)
        
        print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        var postLength = String(dataModel.length)
        var url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/fullname")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
              
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()

                        
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                    
                    self.updateResultDict = jsonResult as! NSMutableDictionary
                    print("self.updateResultDict = \(self.updateResultDict)")
                    
                    
                    var dataDict = NSMutableDictionary()
                    dataDict = self.updateResultDict.valueForKey("data") as! NSMutableDictionary
                    
                    print("dataDict = \(dataDict)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var fullname_boolValue = String()
                        
                        fullname_boolValue = dataDict.valueForKey("full_name") as! String
                        
                        print("fullname_boolValue = \(fullname_boolValue)")
                        
                        if fullname_boolValue == "0"
                        {
                            print("first name only")
                            
//                            let defaults = NSUserDefaults.standardUserDefaults()
//                            defaults.setObject(self.savedFirstname, forKey: "userName")
                        }
                        else
                        {
                            print("full name")
//                            let defaults = NSUserDefaults.standardUserDefaults()
//                            defaults.setObject(self.savedName, forKey: "userName")
                        }
                        
                        spinningIndicator.hide(true)
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    

    
    
    
    //MARK:- LOGOUT BTN ACTION
    @IBAction func LogoutBtnAction(sender: AnyObject)
    {
       // print("logout btn")
        logOutAlert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_LOGOUT", comment: "The logout title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_DELETE_NO", comment: "The delete no title"), otherButtonTitles: NSLocalizedString("ALERT_DELETE_YES", comment: "The delete yes title"))
        logOutAlert.tag = 1
        logOutAlert.show()
    }
    
    
    //MARK:- UIALERTVIEW DELEGATE
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if logOutAlert.tag == 1
        {
            if buttonIndex == 1
            {

                instaAccessToken = ""
                
                let cookieJar : NSHTTPCookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
                for cookie in cookieJar.cookies! as [NSHTTPCookie]
                {
                    NSLog("cookie.domain = %@", cookie.domain)
                    
                    if cookie.domain == "www.instagram.com" ||
                        cookie.domain == "api.instagram.com"
                    {
                        
                        cookieJar.deleteCookie(cookie)
                    }
                }
                
               NSUserDefaults.standardUserDefaults().setValue(nil, forKeyPath: "InstagramUser")
             //  NSUserDefaults.standardUserDefaults().setValue(nil, forKeyPath: "access_token")
//                let objFBSDKLoginManager: FBSDKLoginManager = FBSDKLoginManager()
//                objFBSDKLoginManager.logOut()
                NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "userID")
                NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "userName")
                NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "userMail")
                NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "userImage")
                
                let homeScreen = self.storyboard?.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(homeScreen, animated: true)
            }
        }
    }
    
    
    //MARK:- ARROW DOWN BTN ACTION
    @IBAction func ArrowBtnDownAction(sender: AnyObject)
    {
       // print("Arrow btn down")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
        let middle = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
        let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
        let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
        let bottom = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
        
        let snapContainer = SnapContainerViewController.containerViewWith(left,
                                                                          middleVC: middle,
                                                                          rightVC: right,
                                                                          topVC: top,
                                                                          bottomVC: nil)
        
        snapContainer.BestPostStr = "BestPostStr"
        
        middle.didCancel = { () in
            //print("didCancel")
            
            if myProfileBool == true
            {
                let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                //cameraScreen.newImage = image
                self.navigationController?.pushViewController(cameraScreen, animated: true)
                
                myProfileBool = false
            }
            
            if myPostsBool == true
            {
                let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                //votingScreen.newImage = newImage
                let transition = CATransition()
                transition.duration = 0.5  // kCAMediaTimingFunctionEaseInEaseOut
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionMoveIn
                transition.subtype = kCATransitionFromLeft
                self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)
                self.navigationController?.pushViewController(profileScreen, animated: true)
                
                myPostsBool = false
            }
            
            
            if myBestPostsBool == true
            {
                let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                //votingScreen.newImage = newImage
                profileScreen.BestPostStr = "BestPostStr"
                self.navigationController?.pushViewController(profileScreen, animated: true)
                //dismissBestPost = true
                myBestPostsBool = false
            }
            
        }
        
        middle.didFinishCapturingImage = {(image: UIImage) in
           // print("didFinishCapturingImage")
            
           // self.newImage = image
            imageCaptured = image
            let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
            cameraScreen.newImage = imageCaptured
            //self.navigationController?.presentViewController(cameraScreen, animated: false, completion: nil)
            self.navigationController?.pushViewController(cameraScreen, animated: true)
            imageCaptureBool = true
            
        }
        
        self.navigationController?.pushViewController(snapContainer, animated: true)
     
    }
    
    
    //MARK:- UITOUCH EVENTS METHOD
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        if let touch = touches.first
        {
           //print("touchesBegan | This is a view")
            self.WebTextField.resignFirstResponder()
        }
        else
        {
            //print("touchesBegan | This is not a view")
        }
        super.touchesBegan(touches, withEvent:event)
    }
    
    
    //MARK:- DID-RECEIVE-MEMORY-WARNING
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
