//
//  VotingViewController.swift
//  What To Wear
//
//  Created by brst on 8/22/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import GoogleMobileAds

class VotingViewController: UIViewController,UIAlertViewDelegate,UIActionSheetDelegate,GADInterstitialDelegate
{

    //MARK:- VARIABLES DECLARATION
    var ImageCheck = String()
    var interstitialAd : GADInterstitial!
    var longPressView: UIView!
    var newImage = UIImage(named: "")
    var savedUserID = String()
    var savedEmail = String()
    var savedName = String()
    var savedImage = String()
    var dataModel = NSData()
    var postDetailDict = NSMutableDictionary()
    var post_id = String()
    var savedArrayValues = NSMutableDictionary()
    var likeCountInt = 0
    var dislikeCountInt = 0
    

    

    //MARK:- OUTLETS DECLARATION
    @IBOutlet var UserNameLbl: UILabel!
    @IBOutlet var WrapperBtnImage: UIButton!
    @IBOutlet var ImageAdded: UIImageView!
    @IBOutlet var HeadingView: UIView!
    @IBOutlet var WrapperViewNameImage: UIView!
    @IBOutlet var LikeBnt: UIButton!
    @IBOutlet var DislikeBtn: UIButton!
    @IBOutlet var LikeImage: UIImageView!
    @IBOutlet var DislikeImage: UIImageView!
    @IBOutlet var NextArrowImageView: UIImageView!
    @IBOutlet var RoundImageView: UIButton!
    
    
    //MARK:- VIEW-DID-LOAD
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        if ImageCheck == "ImageCheck"
        {
           
            let total = savedArrayValues.allKeys.count
            //if savedArrayValues != []
            if total > 0
            {
                
                print("saved array is viewDidLoad = \(savedArrayValues)")
                
                post_id = self.savedArrayValues.valueForKey("post_id") as! String
                print("self.post_id=\(post_id)")
                

                var profileUrl: String = self.savedArrayValues.valueForKey("img_para") as! String
                var url: NSURL = NSURL(string: profileUrl)!
                
                if let imageURL = NSURL(string: profileUrl)
                {
                    if let data = NSData(contentsOfURL: imageURL)
                    {
                        self.ImageAdded.image = UIImage(data: data)
                    }
                }
                
            }

        }
        
        
        if GetpostIDBool == true
        {
            print("self.postDetailDict=\(self.postDetailDict)")
            
            var post_id_str = self.postDetailDict.valueForKey("post_id") as! String
            print("self.post_id=\(post_id_str)")
            
            
            if post_id_str != ""
            {
                post_id = post_id_str
            }
            else
            {
               post_id = NSUserDefaults.standardUserDefaults().valueForKey("post_id") as! String
            }
            
            
            var profileUrl: String = self.postDetailDict.valueForKey("img_para") as! String
            var url: NSURL = NSURL(string: profileUrl)!
            
            if let imageURL = NSURL(string: profileUrl)
            {
                if let data = NSData(contentsOfURL: imageURL)
                {
                    self.ImageAdded.image = UIImage(data: data)
                }
            }

            
            
           // GetpostIDBool = false
        }
        
    
        startNewGame()
        interstitialAd.delegate = self;
        
        GoogleAnalyticsRegisterView()

//        if imageCaptured != nil
//        {
//            
//            //print("new image clicked = \(newImage)")
//            ImageAdded.image = imageCaptured
//        }
        


       
    }

    
    
    //MARK:-  GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "VotingScreen")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    
    
    //MARK:- START NEW GAME METHOD
    private func startNewGame()
    {
        createAndLoadInterstitial()
        
        // Set up a new game.
    }
    
    //MARK:- CREATE INTERSTITIAL  ADS METHOD
    private func createAndLoadInterstitial()
    {
        interstitialAd = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        let request = GADRequest()
        // Request test ads on devices you specify. Your test device ID is printed to the console when
        // an ad request is made.
        request.testDevices = [ kGADSimulatorID, "2077ef9a63d2b398840261c8221a0c9b" ]
        interstitialAd.loadRequest(request)
    }
    
    
    //MARK:- END GAME METHOD
    private func endGame()
    {
//        UIAlertView(title: "Game Over",
//                    message: "Your time ran out!",
//                    delegate: self,
//                    cancelButtonTitle: "Ok").show()
    }
    
    
    //MARK:- UIALERTVIEW DELEGATE
    func alertView(alertView: UIAlertView, willDismissWithButtonIndex buttonIndex: Int)
    {
        if interstitialAd.isReady
        {
            interstitialAd.presentFromRootViewController(self)
        }
        else
        {
            print("Ad wasn't ready")
        }
        // Give user the option to start the next game.
    }
    //    func createAndLoadInterstitial() -> GADInterstitial
    //    {
    //        var interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
    //        interstitial.delegate = self
    //        interstitial.loadRequest(GADRequest())
    //        print("Google Mobile Ads SDK version: " + GADRequest.sdkVersion())
    //        return interstitial
    //    }
    //
    //    func interstitialDidDismissScreen(ad: GADInterstitial!)
    //    {
    //        interstitialAd = createAndLoadInterstitial()
    //        print("interstitialDidDismissScreen")
    //    }
    
    
    //MARK:- INSTERSTITIAL ADS DELEGATES
    
    /// Called when an interstitial ad request succeeded.
    func interstitialDidReceiveAd(ad: GADInterstitial!)
    {
        print("interstitialDidReceiveAd")
        interstitialAd.presentFromRootViewController(self)
    }
    
    /// Called when an interstitial ad request failed.
    func interstitial(ad: GADInterstitial!, didFailToReceiveAdWithError error: GADRequestError!)
    {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Called just before presenting an interstitial.
    func interstitialWillPresentScreen(ad: GADInterstitial!)
    {
        print("interstitialWillPresentScreen")
    }
    
    /// Called before the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(ad: GADInterstitial!)
    {
        print("interstitialWillDismissScreen")
    }
    
    //    /// Called just after dismissing an interstitial and it has animated off the screen.
    //    func interstitialDidDismissScreen(ad: GADInterstitial!)
    //    {
    //        print("interstitialDidDismissScreen")
    //    }
    
    /// Called just before the application will background or terminate because the user clicked on an
    /// ad that will launch another app (such as the App Store).
    func interstitialWillLeaveApplication(ad: GADInterstitial!)
    {
        print("interstitialWillLeaveApplication")
    }


    //MARK:- VIEW-WILL-APPEAR
    override func viewWillAppear(animated: Bool)
    {
        UIApplication.sharedApplication().statusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
        WrapperViewNameImage.layer.cornerRadius = 10
        
        WrapperBtnImage.layer.masksToBounds = true
        WrapperBtnImage.layer.cornerRadius = 10
        
        ImageAdded.layer.masksToBounds = true
        ImageAdded.layer.cornerRadius = 10
       // self.ImageAdded.roundCorner(UIRectCorner.TopRight, radius: 10)
        
        
//        if FacebookLoginBool == true
//        {
            var userDefaults = NSUserDefaults.standardUserDefaults()
            savedUserID = userDefaults.valueForKey("userID") as! String
            savedName = userDefaults.valueForKey("userName") as! String
            savedEmail = userDefaults.valueForKey("userMail") as! String
            savedImage = userDefaults.valueForKey("userImage") as! String
            
//            print("savedUserID = \(savedUserID) ")
//            print("savedName = \(savedName) ")
//            print("savedEmail = \(savedEmail) ")
//            print("savedImage = \(savedImage) ")
        
            
            self.UserNameLbl.text = savedName
            
  //          FacebookLoginBool = false
  //      }
        
     
        
        
       addingGestures()
        
        
//        let rectShape = CAShapeLayer()
//        rectShape.bounds = self.ImageAdded.frame
//        rectShape.position = self.ImageAdded.center
//       // rectShape.path = UIBezierPath(roundedRect: self.ImageAdded.bounds, byRoundingCorners: .BottomLeft , .BottomRight , cornerRadii: CGSize(width: 20, height: 20)).CGPath
//        
//        
//        
//        let path = UIBezierPath(roundedRect:ImageAdded.bounds, byRoundingCorners:[.topRight, .bottomLeft], cornerRadii: CGSize(width: 20, height:  20))
//        let maskLayer = CAShapeLayer()
//        maskLayer.path = path.cgPath
//        ImageAdded.layer.mask = maskLayer
//        
//        
//        self.ImageAdded.layer.backgroundColor = UIColor.greenColor().CGColor
//        //Here I'm masking the textView's layer with rectShape layer
//        self.ImageAdded.layer.mask = rectShape
        
        
        
        DislikeBtn.layer.borderWidth=1.0
        DislikeBtn.layer.masksToBounds = false
        DislikeBtn.layer.borderColor = UIColor.whiteColor().CGColor
        DislikeBtn.layer.cornerRadius = 13
        DislikeBtn.layer.cornerRadius = DislikeBtn.frame.size.height/2
        DislikeBtn.clipsToBounds = true
        
        LikeBnt.layer.borderWidth=1.0
        LikeBnt.layer.masksToBounds = false
        LikeBnt.layer.borderColor = UIColor.whiteColor().CGColor
        LikeBnt.layer.cornerRadius = 13
        LikeBnt.layer.cornerRadius = LikeBnt.frame.size.height/2
        LikeBnt.clipsToBounds = true
        
       
//        if dismissBestPost == true
//        {
//            //  let camera = DKCamera()
//            var subview = UIView()
//            subview = UIView(frame: (CGRectMake(0, 0, 320, 568)))
//            subview.backgroundColor = UIColor.blackColor()
//            self.view.addSubview(subview)
//
//            
//            let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
//            //votingScreen.newImage = newImage
//            self.navigationController?.pushViewController(profileScreen, animated: true)
//            dismissBestPost = false
//        }
//        
//        
//        if dismissMyPost == true
//        {
//            //  let camera = DKCamera()
//            var subview = UIView()
//            subview = UIView(frame: (CGRectMake(0, 0, 320, 568)))
//            subview.backgroundColor = UIColor.blackColor()
//            self.view.addSubview(subview)
//
//            
//            let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
//            //votingScreen.newImage = newImage
//            self.navigationController?.pushViewController(profileScreen, animated: true)
//            dismissMyPost = false
//        }
//
//        
//        
//        if imageCaptureBool == true
//        {
//            //self.newImage = image
//            
//            let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
//            //cameraScreen.newImage = image
//            self.navigationController?.pushViewController(cameraScreen, animated: false)
//             imageCaptureBool = false
//        }
//
//        if dismissMyProfile == true
//        {
//            //self.newImage = image
//            var subview = UIView()
//            subview = UIView(frame: (CGRectMake(0, 0, 320, 568)))
//            subview.backgroundColor = UIColor.blackColor()
//            self.view.addSubview(subview)
//
//            
//            let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
//            //cameraScreen.newImage = image
//            self.navigationController?.pushViewController(cameraScreen, animated: true)
//            
//            dismissMyProfile = false
//        }
//

    }
    
    
    // MARK: - GESTURE METHOD
    func addingGestures()
    {
        let tapGesture = UITapGestureRecognizer(target: self, action: "normalTap")
        let longGesture = UILongPressGestureRecognizer(target: self, action: "longTap:")
        tapGesture.numberOfTapsRequired = 1
        WrapperBtnImage.addGestureRecognizer(tapGesture)
        WrapperBtnImage.addGestureRecognizer(longGesture)
    }
    

    //MARK:- DISLIKE BTN ACTION
    @IBAction func DislikeBtnAction(sender: AnyObject)
    {
         print("dislike btn")
        
        print("dislikeCountInt=\(dislikeCountInt)")
        
        if post_id == ""
        {
            print("post_id is blank")
            var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_NO_POSTS_AVAIL", comment: "The  no post available title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
            alert.show()
            
        }
        else
        {
            print("post_id is not blank")
            if dislikeCountInt == 0
            {
                print("dislikeCountInt is 0")
                UnLikePostAPI()
            }
            else
            {
                print("dislikeCountInt is greater than 0")
                var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_DISLIKE_ONCE", comment: "The  like/dislike once title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                alert.show()
            }

        }

        
    }
    
    
    
    
    //MARK:- UnLikePostAPI
    func UnLikePostAPI()
    {
        
//        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
//        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        savedUserID = NSUserDefaults.standardUserDefaults().valueForKey("userID") as! String
        
        var post = NSString(format:"post_id=%@",post_id)
        
        print("post details = \(post)")
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        let postLength = String(dataModel.length)
        let url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/unlikesave")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    //spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()
                      //  spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                    
                    var statusStr = String()
                    self.postDetailDict = jsonResult as! NSMutableDictionary
                    print("self.postDetailDict = \(self.postDetailDict)")
                    
                    statusStr = self.postDetailDict.valueForKey("status") as! String
                    print("statusStr = \(statusStr)")
                    
                    if statusStr == "Success"
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            print("Unlike saved successfully")
                            
                            self.dislikeCountInt = self.dislikeCountInt + 1
                            print("self.dislikeCountInt=\(self.dislikeCountInt)")
                            
                            var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_POST_UNLIKE_SAVE", comment: "The like saved title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            alert.show()
                            
                            
                          //  spinningIndicator.hide(true)
                            
                        })
                        
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_POST_UNLIKE_UNSAVE", comment: "The dislike not saved title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            alert.show()
                            
                           // spinningIndicator.hide(true)
                        })
                        
                    }
                    
                    
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }

    

    
    
    
    //MARK:- LIKE BTN ACTION
    @IBAction func LikeBtnAction(sender: AnyObject)
    {
        print("like btn")
    
        print("likeCountInt=\(likeCountInt)")
        
        print("post_id = \(post_id)")
        
        if post_id == ""
        {
             print("post_id is blank")
            var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_NO_POSTS_AVAIL", comment: "The  no post available title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
            alert.show()
        }
        else
        {
            print("post_id is not blank")
            
            if likeCountInt == 0
            {
                print("likeCountInt is 0")
                LikePostAPI()
            }
            else
            {
                print("likeCountInt is greater than 0")
                
                var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_LIKE_ONCE", comment: "The  like/dislike once title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                alert.show()
            }
           
        }
        
       
    }
    
    
    
    
    //MARK:- LikePostAPI
    func LikePostAPI()
    {
        
//        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
//        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        savedUserID = NSUserDefaults.standardUserDefaults().valueForKey("userID") as! String
        
        var post = NSString(format:"post_id=%@",post_id)
        
        print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        let postLength = String(dataModel.length)
        let url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/likesave")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                  //  spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()
                    //    spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                    
                    var statusStr = String()
                    self.postDetailDict = jsonResult as! NSMutableDictionary
                    print("self.postDetailDict = \(self.postDetailDict)")
                    
                    statusStr = self.postDetailDict.valueForKey("data") as! String
                     print("statusStr = \(statusStr)")
                    
                  if statusStr == "Success"
                  {
                          dispatch_async(dispatch_get_main_queue(), {
                
                                print("Like saved successfully")
                            
                            self.likeCountInt = self.likeCountInt + 1
                            
                            print("self.likeCountInt=\(self.likeCountInt)")
                            
                            var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_POST_LIKE_SAVE", comment: "The like saved title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            alert.show()

                            
                               //  spinningIndicator.hide(true)
                        
                             })
                    
                   }
                  else
                  {
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_POST_LIKE_UNSAVE", comment: "The like not saved title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()
                        
                       // spinningIndicator.hide(true)
                    })

                   }
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }

    
    
    
    
    //MARK:- REPORT BTN ACTION
    @IBAction func ReportBtnAction(sender: AnyObject)
    {
      //  print("report btn")
     
       /* let alert = UIAlertController(title: "Notice", message: "Lauching this missile will destroy the entire universe. Is this what you intended to do?", preferredStyle: UIAlertControllerStyle.Alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Remind Me Tomorrow", style: UIAlertActionStyle.Default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Launch the Missile", style: UIAlertActionStyle.Destructive, handler: nil))
        
        // show the alert
        self.presentViewController(alert, animated: true, completion: nil)*/
        
  
        
        // title --> "Choose Option","Cancel" ,"Report the picture"
        let actionSheet = UIActionSheet(title: NSLocalizedString("TITLE_ACTION_SHEET", comment: "The title"), delegate: self, cancelButtonTitle: NSLocalizedString("CANCEL_OPTION", comment: "The cancel title"), destructiveButtonTitle: nil, otherButtonTitles:NSLocalizedString("REPORT_OPTION", comment: "The report title"))
        
        actionSheet.showInView(self.view)
  
    }
    
    
    //MARK:- UIACTIONSHEET DELEGATE
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        print("\(buttonIndex)")
        
        switch (buttonIndex)
        {
               case 0:
                        print("Cancel")
                case 1:
                         print("Save")
               //        case 2:
               //            print("Delete")
                 default:
                          print("Default")
            
            
         }
    }
    
    
    //MARK:- IMAGE DESCRIPTION BTN ACTION
    @IBAction func ImageDescriptonBtnAction(sender: AnyObject)
    {
         print("image description btn")
    }
    
    
    //MARK:- UI-GESTURES METHOD
    func normalTap()
    {
       //  print("TapPressed")
        let imageScreen = storyboard?.instantiateViewControllerWithIdentifier("ImageDescriptionViewController") as! ImageDescriptionViewController
        //imageScreen.newImage = imageCaptured
        imageScreen.ImageCheck = "ImageCheck"
        imageScreen.savedArrayValues = self.savedArrayValues 
        self.navigationController?.pushViewController(imageScreen, animated: false)
    }
    
    func longTap(sender: UILongPressGestureRecognizer)
    {
       // print("longpressed")
        
        if(sender.state == UIGestureRecognizerState.Ended)
        {
            //print("ended long press")
            
//            let imageScreen = storyboard?.instantiateViewControllerWithIdentifier("ImageDescriptionViewController") as! ImageDescriptionViewController
//            imageScreen.newImage = imageCaptured
//            self.navigationController?.pushViewController(imageScreen, animated: false)
            
        }
        else if(sender.state == UIGestureRecognizerState.Began)
        {
            //print("began long preess")
        }
    }
    
    
    //MARK:- NEXT ARROW BTN ACTION
    @IBAction func NextArrowBtnAction(sender: AnyObject)
    {
       // print("next arrow btn")
        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let left = self.storyboard!.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
        let middle = self.storyboard!.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
        let right = self.storyboard!.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
        let top = self.storyboard!.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
        let bottom = self.storyboard!.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
        
        dispatch_async(dispatch_get_main_queue(), {
            let snapContainer = SnapContainerViewController.containerViewWith(left,
                middleVC: middle,
                rightVC: right,
                topVC: top,
                bottomVC: nil)
            
            snapContainer.BestPostStr = "BestPostStr"
            
            middle.didCancel = { () in
              //  print("didCancel")
                
                if myProfileBool == true
                {
                    let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                    //cameraScreen.newImage = image
                    self.navigationController?.pushViewController(cameraScreen, animated: true)
                    
                    myProfileBool = false
                }
                
                if myPostsBool == true
                {
                    let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                    //votingScreen.newImage = newImage
                    let transition = CATransition()
                    transition.duration = 0.5  // kCAMediaTimingFunctionEaseInEaseOut
                    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    transition.type = kCATransitionMoveIn
                    transition.subtype = kCATransitionFromLeft
                    self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)
                    self.navigationController?.pushViewController(profileScreen, animated: true)
                    
                    myPostsBool = false
                }
                
                
                if myBestPostsBool == true
                {
                    let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                    //votingScreen.newImage = newImage
                    profileScreen.BestPostStr = "BestPostStr"
                    self.navigationController?.pushViewController(profileScreen, animated: true)
                    //dismissBestPost = true
                    myBestPostsBool = false
                }
                
            }
            
            middle.didFinishCapturingImage = {(image: UIImage) in
              //  print("didFinishCapturingImage")
                
                self.newImage = image
                imageCaptured = image
                let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
                cameraScreen.newImage = imageCaptured
                self.navigationController?.pushViewController(cameraScreen, animated: true)
                imageCaptureBool = true
                
            }
            
            self.navigationController?.pushViewController(snapContainer, animated: true)
            
        })

    }
    
    
    //MARK:- DID-RECEIVE-MEMORY-WARNING
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
