//
//  MyBestPostsViewController.swift
//  What To Wear
//
//  Created by brst on 8/23/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SDWebImage

class MyBestPostsViewController: UIViewController,UITableViewDelegate
{

    //MARK:- VARIABLES DECLARATION
    var savedUserID = String()
    var dataModel = NSData()
    var BestPostListingistingResultDict = NSMutableDictionary()
    var BestPostStr = String()
    var sizeHeight = UIScreen.mainScreen().bounds.height
    var ProfileImageArray = [String]()
    var LikeCountArray = [String]()
    var DislikeCountArray = [String]()
    var BestPostsArray = NSMutableArray()
    var particularBestPostDetailsDict = NSMutableDictionary()
    
    
    //MARK:- OUTLETS DECLARATION
    @IBOutlet var BackBtnView: UIView!
    @IBOutlet var BackBtn: UIButton!
    @IBOutlet var myBestPostsTabelView: UITableView!
    @IBOutlet weak var BackLbl: UILabel!
    @IBOutlet weak var bannerView: GADBannerView!
    
    //MARK:- VIEW-DID-LOAD
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        ////////////////////////////////////////// GoogleAnalyticsRegisterView STARTS ////////////////////////////////////////////////
        
        GoogleAnalyticsRegisterView()
        
        ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
         //////////////////////////////////////////// GOOGLE BANNNER ADS CODE ////////////////////////////////////////////////////
        
        let request = GADRequest()
        request.testDevices = ["9ada2ece428c0b1e0e44701ad8d8a8c296857ea1"]
        //request.testDevices = ["2077ef9a63d2b398840261c8221a0c9b"]
        
        print("Google Mobile Ads SDK version: " + GADRequest.sdkVersion())
        // bannerView.adUnitID = "ca-app-pub-7356930517533133/205998480"
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        
        bannerView.rootViewController = self
        bannerView.loadRequest(GADRequest())
        
          ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
       ////////////////////////////////////////////////////// LOALIZED STRINGS ////////////////////////////////////////////////////////////////
        
        self.BackLbl.text = NSLocalizedString("BACK_TITLE", comment: "The back button title")
        
      ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
         ///////////////////////////////////////////////// HIT BEST POST LISTING API ///////////////////////////////////////////////////////////
        
        
        //        if BestPostStr == "BestPostsAPI"
        //        {
        BestPostListingAPI()
        //        }
        
        
         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
    }

    
    //MARK:- GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "BestPostsListingView")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    
    //MARK:- VIEW-WILL-APPEAR
    override func viewWillAppear(animated: Bool)
    {
        
        ///////////////////////////////////////////////////// NAVBAR INITIALIZATION /////////////////////////////////////////////////////
        
        UIApplication.sharedApplication().statusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
        ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        

        
        
        
          ///////////////////////////////////////////////////// ARRAY'S INITIALIZATION /////////////////////////////////////////////////////
        
        ProfileImageArray = ["GirlImage","ManWithWatchImage","TableViewImage2","TableViewImage3","GirlImage","ManWithWatchImage","TableViewImage2","TableViewImage3"]
        LikeCountArray = ["79","39","11","10","79","39","11","10"]
        DislikeCountArray = ["2","14","7","2","2","14","7","2"]
        
        ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
    }
    
    
    
    
    //MARK:- BestPostListingAPI
    func BestPostListingAPI()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        savedUserID = NSUserDefaults.standardUserDefaults().valueForKey("userID") as! String
        
        var post = NSString(format:"user_id=%@",savedUserID)
        
        print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        let postLength = String(dataModel.length)
        let url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/bestposts")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
                
                
                
                
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()
                        
                        
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                    
                    self.BestPostListingistingResultDict = jsonResult as! NSMutableDictionary
                    print("self.BestPostListingistingResultDict = \(self.BestPostListingistingResultDict.count)")
    
                    let statusStr = self.BestPostListingistingResultDict.valueForKey("status") as! String
                    
//                    if self.BestPostStr == "BestPostsAPI"
//                    {
                        if statusStr == "No post Found"
                        {
                            dispatch_async(dispatch_get_main_queue(), {
                                
                                let alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_NO_BEST_POSTS", comment: "The no best posts title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                                alert.show()
                                
                                self.BestPostsArray = []
                                
                                self.myBestPostsTabelView.reloadData()
                                
                                spinningIndicator.hide(true)
                            })

//                    }
                    }
                    else if statusStr == "success"
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            self.BestPostsArray = self.BestPostListingistingResultDict.valueForKey("data")  as! NSMutableArray
                            print("self.BestPostsArray = \(self.BestPostsArray)")
                            //   var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_DISCONNECT_FALSE", comment: "The disconnect false title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            // alert.show()
                            self.myBestPostsTabelView.reloadData()
                            spinningIndicator.hide(true)
                        })
                    }
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }

    
    
    
    

    //MARK:- BACK BTN ACTION
    @IBAction func BackBtnaction(sender: AnyObject)
    {
      //  print("back btn")
        
        //self.navigationController?.popViewControllerAnimated(true)
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
        let middle = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
        let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
        let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
        let bottom = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
        
        let snapContainer = SnapContainerViewController.containerViewWith(left,
                                                                          middleVC: middle,
                                                                          rightVC: right,
                                                                          topVC: top,
                                                                          bottomVC: nil)
        
        snapContainer.BestPostStr = "BestPostStr"
        
        middle.didCancel = { () in
          //  print("didCancel")
            
            if myProfileBool == true
            {
                //                self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                //                dismissMyProfile = true
                
                let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                //cameraScreen.newImage = image
                self.navigationController?.pushViewController(cameraScreen, animated: true)
                
                
                
                //                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                //                let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                //                let middle = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                //                let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                //                let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                //                let bottom = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
                //
                //                let snapContainer = SnapContainerViewController.containerViewWith(left,
                //                                                                                  middleVC: middle,
                //                                                                                  rightVC: right,
                //                                                                                  topVC: nil,
                //                                                                                  bottomVC: bottom)
                //
                //              //  snapContainer.BestPostStr = "UsingMyProfile"
                //
                //                self.navigationController?.pushViewController(snapContainer, animated: true)
                
                myProfileBool = false
            }
            
            if myPostsBool == true
            {
                
                //self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                //dismissMyPost = true
                
                
                let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                //votingScreen.newImage = newImage
                let transition = CATransition()
                transition.duration = 0.5  // kCAMediaTimingFunctionEaseInEaseOut
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionMoveIn
                transition.subtype = kCATransitionFromLeft
                self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)
                self.navigationController?.pushViewController(profileScreen, animated: true)
                
                
                //                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                //                let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                //                let middle = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
                //                let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                //                let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                //                let bottom = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
                //
                //                let snapContainer = SnapContainerViewController.containerViewWith(left,
                //                                                                                  middleVC: middle,
                //                                                                                  rightVC: right,
                //                                                                                  topVC: top,
                //                                                                                  bottomVC: nil)
                //
                //                snapContainer.BestPostStr = "UsingMyPosts"
                //
                //                self.navigationController?.pushViewController(snapContainer, animated: true)
                
                
                myPostsBool = false
            }
            
            
            if myBestPostsBool == true
            {
                //self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                //votingScreen.newImage = newImage
                profileScreen.BestPostStr = "BestPostStr"
                self.navigationController?.pushViewController(profileScreen, animated: true)
                
                
                
                //                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                //                let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                //                let middle = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
                //                let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                //                let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                //                let bottom = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
                //
                //                let snapContainer = SnapContainerViewController.containerViewWith(left,
                //                                                                                  middleVC: middle,
                //                                                                                  rightVC: right,
                //                                                                                  topVC: top,
                //                                                                                  bottomVC: nil)
                //
                //                snapContainer.BestPostStr = "UsingMyBestPosts"
                //
                //                self.navigationController?.pushViewController(snapContainer, animated: true)
                
                
                //dismissBestPost = true
                myBestPostsBool = false
            }
            
        }
        
        middle.didFinishCapturingImage = {(image: UIImage) in
           // print("didFinishCapturingImage")
            
           // self.newImage = image
            imageCaptured = image
            //self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
            
            let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
            cameraScreen.newImage = imageCaptured
            //  self.navigationController?.presentViewController(cameraScreen, animated: false, completion: nil)
            self.navigationController?.pushViewController(cameraScreen, animated: true)
            imageCaptureBool = true
            
        }
        
        let transition = CATransition()
        transition.duration = 0.5  // kCAMediaTimingFunctionEaseInEaseOut
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromLeft
        self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)
        
        self.navigationController?.pushViewController(snapContainer, animated: true)
        //   self.navigationController?.presentViewController(snapContainer, animated: true, completion: nil)
    }
    
    
    
    
    //MARK:- UITABLEVIEW DELEGATES
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //return ProfileImageArray.count
        return self.BestPostsArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
         ////////////////////////////////////////////////////////// REUSABLE CELL ////////////////////////////////////////////////////////////////
        
        var cell = tableView.dequeueReusableCellWithIdentifier("MyPostsCell", forIndexPath: indexPath)as! MyPostsTableViewCell
        
         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
        
         //////////////////////////////////////////////////////////// CELL'S PROPERTIES ////////////////////////////////////////////////////////////////
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        myBestPostsTabelView.separatorColor = UIColor.clearColor()
        
        cell.BackgroundTableView.layer.cornerRadius = 10
        cell.BackgroundTableView.layer.masksToBounds = true

         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
        
        
        
         ///////////////////////////////////////////////////// BUTTON TAG & ACTION ///////////////////////////////////////////////////////////
        
        cell.WrapperImageBtn.tag = indexPath.row
        cell.WrapperImageBtn.addTarget(self, action: "WrapperImageAction:", forControlEvents: UIControlEvents.TouchUpInside)
        
         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
        
        
        
         ////////////////////////////////////////////////// UIGESTURES ON BUTTON //////////////////////////////////////////////////////
        
//        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: "longTap:")
//         cell.WrapperImageBtn.addGestureRecognizer(longPressRecognizer)
//        
//        let tapGesture = UITapGestureRecognizer(target: self, action: "normalTap")
//        tapGesture.numberOfTapsRequired = 1
//        cell.WrapperImageBtn.addGestureRecognizer(tapGesture)
  
         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
        
        
        
        
        
     //////////////////////////////////////////////// GET LIKES & UNLIKES FOR A POST //////////////////////////////////////////////////
        
        if  self.BestPostsArray[indexPath.row].valueForKey("likes") is NSNull
        {
            //print("self.VotesDict is null")
            cell.LikeCountLbl.text = "0"
        }
        else
        {
            //print("\(self.VotesDict.valueForKey("likes") as! String)")
            cell.LikeCountLbl.text = self.BestPostsArray[indexPath.row].valueForKey("likes")  as? String
        }
        
        // let dislikeStr:AnyObject = self.BestPostsArray[indexPath.row].valueForKey("unlike") as! String
        
        if  self.BestPostsArray[indexPath.row].valueForKey("unlike") is NSNull
        {
            //print("self.VotesDict is null")
            cell.DislikeCountLbl.text = "0"
        }
        else
        {
            //print("\(self.VotesDict.valueForKey("likes") as! String)")
            cell.DislikeCountLbl.text =  self.BestPostsArray[indexPath.row].valueForKey("unlike") as? String
        }
        
        
         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
        
        
         ////////////////////////////////////////// GET POST TIMING AND DISPLAYING ///////////////////////////////////////////////
        
        var postDict = self.BestPostsArray[indexPath.row].valueForKey("post")  as! NSDictionary
        print("postDict=\(postDict)")
        
        var post_time = String()
        post_time = postDict.valueForKey("post_time") as! String
        
        if post_time != ""
        {
            
            let strtemp = post_time
            // print("strtemp is \(strtemp)")
            
            let str = timedifference(strtemp)
            
            cell.DaysAgoLbl.text = "  \(str) "
            
        }

       ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
        
        
         /////////////////////////////////////////////////////////// SET IMAGE BOUNDS /////////////////////////////////////////////////////////////
        
        cell.MyPostImage.layer.cornerRadius = 5
        cell.MyPostImage.layer.masksToBounds = true
        
         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
        
        
        
         //////////////////////////////////////////////////// DISPLAY IMAGE FROM URL ////////////////////////////////////////////////////////
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            var profileUrl: String = postDict.valueForKey("img_para") as! String
            var url: NSURL = NSURL(string: profileUrl)!
            dispatch_async(dispatch_get_main_queue(), {
                
                if let cellToUpdate = self.myBestPostsTabelView.cellForRowAtIndexPath(indexPath) as? MyPostsTableViewCell {
                    if let imageURL = NSURL(string: profileUrl)
                    {
                        if let data = NSData(contentsOfURL: imageURL)
                        {
                            //cell.MyPostImage.image = UIImage(data: data)
                            let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                            }
                            // cell.MyPostImage.sd_setImageWithURL(url, completed: block)
                            cell.MyPostImage.sd_setImageWithURL(url, placeholderImage:  UIImage(named: "NoImageDefault"))
                            
                            //self.MyPostsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
                            
                        }
                    }
                }
                
            })
            
            
            /*dispatch_async(dispatch_get_main_queue(), {
             
             cell.MyPostImage.image =  self.ImagesArray[indexPath.row]
             
             });*/
            
            
            
            
            //                    //cell.MyPostImage.sd_setImageWithURL(imageURL, placeholderImage: UIImage(named: "NoImageDefault"), options: SDWebImageOptions.ContinueInBackground)
            //
            //
            //                         dispatch_async(dispatch_get_main_queue(), {
            //
            //                                     cell.MyPostImage.sd_setImageWithURL(imageURL)
            //
            //                          });
            //
            //                    //cell.MyPostImage.contentMode = UIViewContentMode.ScaleAspectFit
            //
            //           }
            //    }
            
            
        });

         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        

        
        
         /////////////////////////////////////////// COMPATIBILITY iPHONE DEVICES //////////////////////////////////////////////////////
        
        if sizeHeight == 736
        {
            cell.LikeImage.frame.size.width = 22
            cell.LikeImage.frame.size.height = 22
            
            cell.DislikeImage.frame.size.width = 20
            cell.DislikeImage.frame.size.height = 20
            
            cell.MyPostImage.frame.size.width = 110
            
            cell.LikeCountLbl.frame.origin.x = cell.MyPostImage.frame.origin.x + cell.MyPostImage.frame.size.width + 10
            cell.LikeImage.frame.origin.x = cell.LikeCountLbl.frame.origin.x + cell.LikeCountLbl.frame.size.width + 10
            cell.UnderLineLbl.frame.origin.x = cell.MyPostImage.frame.origin.x + cell.MyPostImage.frame.size.width + 45
            
        }
        else  if sizeHeight == 667
        {
            cell.LikeImage.frame.size.width = 22
            cell.LikeImage.frame.size.height = 22
            
            cell.DislikeImage.frame.size.width = 20
            cell.DislikeImage.frame.size.height = 20
            
            cell.MyPostImage.frame.size.width = 110
            
            cell.LikeCountLbl.frame.origin.x = cell.MyPostImage.frame.origin.x + cell.MyPostImage.frame.size.width + 10
            cell.LikeImage.frame.origin.x = cell.LikeCountLbl.frame.origin.x + cell.LikeCountLbl.frame.size.width + 10
            cell.UnderLineLbl.frame.origin.x = cell.MyPostImage.frame.origin.x + cell.MyPostImage.frame.size.width + 45
            
        }
        
         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
//        let myPostDetailScreen = storyboard?.instantiateViewControllerWithIdentifier("MyBestPostDetailsViewController") as! MyBestPostDetailsViewController
//        self.navigationController?.pushViewController(myPostDetailScreen, animated: true)
    }

    
    
    //MARK:- TIME DIFFERENCE FROM TIMESTAMP AND CURRENT TIME
    func timedifference(dateString: String)->String
    {
        let date = NSDate()
        
        //let messageTimezoneOffset = resultSet.intForColumn("timezoneOffset")
        
        
        /// GET TIMESTAMP IN UNIX
        let currentTimeStamp = Int64(date.timeIntervalSince1970*1000)
        
        //        print("current date = \(NSDate().timeIntervalSince1970 * 1000)")
        //
        //        print("currentTimeStamp=\(currentTimeStamp)")
        // let timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
        
        let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
        //   print("timeDiff=\(timeDiff)")
        
        let nYears = timeDiff / (1000*60*60*24*365)
        
        
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        
        
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        
        
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        //println(nMinutes)
        
        var timeMsg = ""
        
        if nYears > 0
        {
            var yearWord = "years"
            if nYears == 1
            {
                yearWord = "year"
            }
            
            timeMsg = " \(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
            var monthWord = "months"
            if nMonths == 1
            {
                monthWord = "month"
            }
            
            timeMsg = " \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            timeMsg = " \(nDays) \(dayWord) ago"
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = " \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            
            timeMsg = " \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        return timeMsg
    }

    
    
    
    //MARK:- WRAPPER IMAGE BTN ACTION
    func WrapperImageAction(sender: UIButton!)
    {
        
         /////////////////////////////////////////////// MOVE TO BEST POST DETAILS //////////////////////////////////////////////////////////
        
        let imageScreen = storyboard?.instantiateViewControllerWithIdentifier("MyBestPostDetailsViewController") as! MyBestPostDetailsViewController
        //  imageScreen.newImage = imageCaptured
         imageScreen.particularBestPostDetailsDict = self.BestPostsArray[sender.tag] as! NSMutableDictionary
        self.navigationController?.pushViewController(imageScreen, animated: false)
        
         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
    }
    
    
    //MARK:- UI-GESTURES METHOD
    func normalTap()
    {
       // print("TapPressed")
        
    }
    
    func longTap(sender: UILongPressGestureRecognizer)
    {
       // print("longpressed")
        
        if(sender.state == UIGestureRecognizerState.Ended)
        {
          //  print("ended long press")
            
        }
        else if(sender.state == UIGestureRecognizerState.Began)
        {
            //print("began long preess")
        }
    }

    
    //MARK:- DID-RECEIVE-MEMORY-WARNING
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 

}
