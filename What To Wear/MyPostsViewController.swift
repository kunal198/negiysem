//
//  MyPostsViewController.swift
//  What To Wear
//
//  Created by brst on 8/22/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import SDWebImage


class MyPostsViewController: UIViewController,UITableViewDelegate
{
    
    //MARK:- VARIABLES DECLARATION
    var dataModel = NSData()
    var savedUserID = String()
    var listingResultDict = NSMutableDictionary()
    var sizeHeight = UIScreen.mainScreen().bounds.height
//    var ProfileImageArray = [String]()
//    var LikeCountArray = [String]()
//    var DislikeCountArray = [String]()
    var PostsArray = NSMutableArray()
    var VotesDict = NSMutableDictionary()
    var particularPostDetailsArray = NSMutableDictionary()
    var ImagesArray = [UIImage]()
    
    
    //MARK:- OUTLETS DECLARATION
    @IBOutlet var BackBtnView: UIView!
    @IBOutlet var BackBtn: UIButton!
    @IBOutlet var MyPostsTableView: UITableView!
    @IBOutlet weak var PostsTitleLbl: UILabel!
    
    
    
    //MARK:- VIEW-DID-LOAD
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        
        self.MyPostsTableView.delegate = self
        
        GoogleAnalyticsRegisterView()
        
        self.PostsTitleLbl.text = NSLocalizedString("BACK_POSTS", comment: "The back button title")
        
//        ProfileImageArray = ["TableViewImage1","ManWithWatchImage","TableViewImage2","TableViewImage3","TableViewImage1","ManWithWatchImage","TableViewImage2","TableViewImage3"]
//        LikeCountArray = ["12","39","11","10","79","39","11","10"]
//        DislikeCountArray = ["3","14","7","2","2","14","7","2"]
    }

    
    //MARK:- VIEW-WILL-APPEAR
    override func viewWillAppear(animated: Bool)
    {
        UIApplication.sharedApplication().statusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
        self.ImagesArray = []
        
       PostListingAPI()
    }
    
    
    func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
        let calendar = NSCalendar.currentCalendar()
        let now = NSDate()
        let earliest = now.earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:NSDateComponents = calendar.components([NSCalendarUnit.Minute , NSCalendarUnit.Hour , NSCalendarUnit.Day , NSCalendarUnit.WeekOfYear , NSCalendarUnit.Month , NSCalendarUnit.Year , NSCalendarUnit.Second], fromDate: earliest, toDate: latest, options: NSCalendarOptions())
        
        if (components.year >= 2)
        {
            return "\(components.year) years ago"
        }
        else if (components.year >= 1)
        {
            if (numericDates)
            {
                return "1 year ago"
            }
            else
            {
                return "Last year"
            }
        }
        else if (components.month >= 2)
        {
            return "\(components.month) months ago"
        }
        else if (components.month >= 1)
        {
            if (numericDates)
            {
                return "1 month ago"
            }
            else
            {
                return "Last month"
            }
        }
        else if (components.weekOfYear >= 2)
        {
            return "\(components.weekOfYear) weeks ago"
        }
        else if (components.weekOfYear >= 1)
        {
            if (numericDates)
            {
                return "1 week ago"
            }
            else
            {
                return "Last week"
            }
        }
        else if (components.day >= 2)
        {
            return "\(components.day) days ago"
        }
        else if (components.day >= 1)
        {
            if (numericDates)
            {
                return "1 day ago"
            }
            else
            {
                return "Yesterday"
            }
        }
        else if (components.hour >= 2)
        {
            return "\(components.hour) hours ago"
        }
        else if (components.hour >= 1)
        {
            if (numericDates)
            {
                return "1 hour ago"
            }
            else
            {
                return "An hour ago"
            }
        }
        else if (components.minute >= 2)
        {
            return "\(components.minute) minutes ago"
        }
        else if (components.minute >= 1)
        {
            if (numericDates)
            {
                return "1 minute ago"
            }
            else
            {
                return "A minute ago"
            }
        }
        else if (components.second >= 3)
        {
            return "\(components.second) seconds ago"
        }
        else
        {
            return "Just now"
        }
        
    }
    
    
    //MARK:- GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "MyPostsListingView")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    
    
    
    
    //MARK:- PostListingAPI
    func PostListingAPI()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        savedUserID = NSUserDefaults.standardUserDefaults().valueForKey("userID") as! String
        
        var post = NSString(format:"user_id=%@",savedUserID)
        //print("post details = \(post)")
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        let postLength = String(dataModel.length)
        let url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/listing")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
             print("jsonresult === \(jsonResult)")
                
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()
                        
                        
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                    
                    self.listingResultDict = jsonResult as! NSMutableDictionary
                  //  print("self.listingResultDict = \(self.listingResultDict.count)")
                    
                    
                    let statusStr = self.listingResultDict.valueForKey("status") as! String
                    
                    if statusStr == "No post Found"
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            let alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_NO_POSTS", comment: "The no posts title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            alert.show()
                            
                            self.PostsArray = []
                            
                            self.MyPostsTableView.reloadData()
                            
                            spinningIndicator.hide(true)
                        })
                        
                    }
                    else if statusStr == "success"
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            self.PostsArray = self.listingResultDict.valueForKey("data")  as! NSMutableArray
                           // print("self.PostsArray = \(self.PostsArray.count)")
                           

//                           for var i = 0; i < self.PostsArray.count; i++
//                            {
//                                var profileUrl: String = self.PostsArray[i].valueForKey("img_para") as! String
//                                var url: NSURL = NSURL(string: profileUrl)!
//                                
//                                
//                                if let imageURL = NSURL(string: profileUrl)
//                                {
//                                    if let data = NSData(contentsOfURL: imageURL)
//                                    {
//                                        let imageof = UIImage(data: data)
//                                        self.ImagesArray.append(imageof!)
//                                        
//                                         print("self.ImagesArray = \(self.ImagesArray.count)")
//                                    }
//                                    
//                                }
//
//                            }
                            
                            
                        
//                            var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_DISCONNECT_FALSE", comment: "The disconnect false title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
//                            alert.show()
                            
                            self.MyPostsTableView.reloadData()
                            
                            spinningIndicator.hide(true)
                        })
                    }
                    
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }

    
    
    
    
    //MARK:- BACK BTN ACTION
    @IBAction func BackbtnAction(sender: AnyObject)
    {
       // print("back btn method")
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    //MARK:- UITABLEVIEW DELEGATES
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //print("self.PostsArray.count=\(self.PostsArray.count)")
        return self.PostsArray.count
    }
    
  
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCellWithIdentifier("MyPostsCell", forIndexPath: indexPath)as! MyPostsTableViewCell

        cell.selectionStyle = UITableViewCellSelectionStyle.None
        MyPostsTableView.separatorColor = UIColor.clearColor()
        cell.BackgroundTableView.layer.cornerRadius = 10
        
        cell.WrapperImageBtn.tag = indexPath.row
        cell.WrapperImageBtn.addTarget(self, action: "WrapperImageAction:", forControlEvents: UIControlEvents.TouchUpInside)
        
        
        let array = self.PostsArray[indexPath.row].valueForKey("votes")
        //let likeStr:AnyObject = array!.valueForKey("likes")!
        //let dislikeStr:AnyObject = array!.valueForKey("unlike")!
        
        //var like:AnyObject = self.VotesDict.valueForKey("likes")!
        
        if  array!.valueForKey("likes")! is NSNull
        {
            //print("self.VotesDict is null")
            cell.LikeCountLbl.text = "0"
        }
        else
        {
            //print("\(self.VotesDict.valueForKey("likes") as! String)")
            cell.LikeCountLbl.text = array!.valueForKey("likes")! as? String
        }

        
        if  array!.valueForKey("unlike")! is NSNull
        {
            //print("self.VotesDict is null")
            cell.DislikeCountLbl.text = "0"
        }
        else
        {
            //print("\(self.VotesDict.valueForKey("likes") as! String)")
            cell.DislikeCountLbl.text = array!.valueForKey("unlike")! as? String
        }

        
        cell.MyPostImage.layer.cornerRadius = 5
        cell.MyPostImage.layer.masksToBounds = true
        
        

        
   dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
    
           var profileUrl: String = self.PostsArray[indexPath.row].valueForKey("img_para") as! String
            var url: NSURL = NSURL(string: profileUrl)!
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        
                        let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                            
                                    }
                        // cell.MyPostImage.sd_setImageWithURL(url, completed: block)
                        
                        
//                        cell.MyPostImage.sd_setImageWithURL(url, placeholderImage:  UIImage(named: "NoImageDefault"))

                        
                    cell.MyPostImage.sd_setImageWithURL(url, placeholderImage: UIImage(named: "NoImageDefault"), options: SDWebImageOptions.ProgressiveDownload, completed: block)
                        
                        
//                 if let cellToUpdate = self.MyPostsTableView.cellForRowAtIndexPath(indexPath) as? MyPostsTableViewCell {
//                            if let imageURL = NSURL(string: profileUrl)
//                            {
//                                if let data = NSData(contentsOfURL: imageURL)
//                                {
//                                    cell.MyPostImage.image = UIImage(data: data)
//                                    
//                                    
//        
//                                    //self.MyPostsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
//                                    cell.MyPostImage.contentMode = .ScaleAspectFill
//                                }
//                            }
//                        }
                
                    })
    
    
                /*dispatch_async(dispatch_get_main_queue(), {
        
                       cell.MyPostImage.image =  self.ImagesArray[indexPath.row]
        
                });*/
    
   });
        
        
        if (self.PostsArray[indexPath.row].valueForKey("post_time") != nil)
        {
            
            let strtemp = self.PostsArray[indexPath.row].valueForKey("post_time") as! String
           // print("strtemp is \(strtemp)")
            
            let str = timedifference(strtemp)
            
            cell.DaysAgoLbl.text = "  \(str) "
            
        }
        
        if sizeHeight == 736
        {
            cell.LikeImage.frame.size.width = 22
            cell.LikeImage.frame.size.height = 22
            
            cell.DislikeImage.frame.size.width = 20
            cell.DislikeImage.frame.size.height = 20
            
            cell.MyPostImage.frame.size.width = 110
            cell.LikeCountLbl.frame.origin.x = cell.MyPostImage.frame.origin.x + cell.MyPostImage.frame.size.width + 10
            cell.LikeImage.frame.origin.x = cell.LikeCountLbl.frame.origin.x + cell.LikeCountLbl.frame.size.width + 10
            cell.UnderLineLbl.frame.origin.x = cell.MyPostImage.frame.origin.x + cell.MyPostImage.frame.size.width + 45
        }
        else  if sizeHeight == 667
        {
            cell.LikeImage.frame.size.width = 22
            cell.LikeImage.frame.size.height = 22
            
            cell.DislikeImage.frame.size.width = 20
            cell.DislikeImage.frame.size.height = 20
            
            cell.MyPostImage.frame.size.width = 110
            
            cell.LikeCountLbl.frame.origin.x = cell.MyPostImage.frame.origin.x + cell.MyPostImage.frame.size.width + 10
            cell.LikeImage.frame.origin.x = cell.LikeCountLbl.frame.origin.x + cell.LikeCountLbl.frame.size.width + 10
            cell.UnderLineLbl.frame.origin.x = cell.MyPostImage.frame.origin.x + cell.MyPostImage.frame.size.width + 45
            
        }

        
        return cell
    }
    
    
    
    
    func timedifference(dateString: String)->String
    {
        let date = NSDate()
        
        //let messageTimezoneOffset = resultSet.intForColumn("timezoneOffset")
        
        
        /// GET TIMESTAMP IN UNIX
        let currentTimeStamp = Int64(date.timeIntervalSince1970*1000)
       
//        print("current date = \(NSDate().timeIntervalSince1970 * 1000)")
//        
//        print("currentTimeStamp=\(currentTimeStamp)")
        // let timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
        
        let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
     //   print("timeDiff=\(timeDiff)")
        
        let nYears = timeDiff / (1000*60*60*24*365)
        
        
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        
        
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        
        
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        //println(nMinutes)
        
        var timeMsg = ""
        
        if nYears > 0
        {
            var yearWord = "years"
            if nYears == 1
            {
                yearWord = "year"
            }
            
            timeMsg = " \(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
            var monthWord = "months"
            if nMonths == 1
            {
                monthWord = "month"
            }
            
            timeMsg = " \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            timeMsg = " \(nDays) \(dayWord) ago"
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = " \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            
            timeMsg = " \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        return timeMsg
    }
    
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
//        let myPostDetailScreen = storyboard?.instantiateViewControllerWithIdentifier("MyPostsDetailViewController") as! MyPostsDetailViewController
//        self.navigationController?.pushViewController(myPostDetailScreen, animated: true)
    }
    
    
    
    //MARK:- WRAPPER IMAGE BTN ACTION
    func WrapperImageAction(sender: UIButton!)
    {
        
        //print("sender tag = \(sender.tag)")
        let myPostDetailScreen = storyboard?.instantiateViewControllerWithIdentifier("MyPostsDetailViewController") as! MyPostsDetailViewController
        myPostDetailScreen.particularPostDetailsArray = self.PostsArray[sender.tag] as! NSMutableDictionary
        self.navigationController?.pushViewController(myPostDetailScreen, animated: false)

    }
    
  
    //MARK:- DID-RECEIVE-MEMORY-WARNING
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
