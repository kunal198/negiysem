//
//  AppDelegate.swift
//  What To Wear
//
//  Created by brst on 8/20/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAnalytics
import FirebaseMessaging
import FirebaseInstanceID






var GetpostIDBool = Bool()
var FacebookLoginBool = Bool()
 var cameraBool = Bool()
var DescriptionTxtViewBool = Bool()
var camerafromDescription = Bool()
var myProfileBool = Bool()
var myPostsBool = Bool()
var myBestPostsBool = Bool()
var TutorialsBtnBool = Bool()
var ShareImageFromCamera = Bool()
var dismissBestPost = Bool()
var dismissMyPost = Bool()
var imageCaptureBool = Bool()
var dismissMyProfile = Bool()
var imageCaptured = UIImage(named: "")
var BestPostLongClickBool = Bool()
var instaAccessToken = String()


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    var window: UIWindow?
    
    var tracker: GAITracker?
    
    class func setupGoogleAnalytics()
    {
        GAI.sharedInstance().trackUncaughtExceptions = true;
        GAI.sharedInstance().dispatchInterval = 20
        GAI.sharedInstance().logger.logLevel = .Verbose
        if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
        {
            //            appDelegate.tracker = GAI.sharedInstance().trackerWithTrackingId("UA-XXXXXXXX-X")
            appDelegate.tracker = GAI.sharedInstance().trackerWithTrackingId("UA-83276254-1")
        }
    }


    
   //  http://instagram.com/accounts/logout/
    
    let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    let INSTAGRAM_APIURl = "https://api.instagram.com/v1/users/"
    let INSTAGRAM_CLIENT_ID = "bf7c3d28453a411e85e1458413f2d80f"
    let INSTAGRAM_CLIENTSERCRET = "f521ea4fccac4ed381c70102eafe9c70"
    let INSTAGRAM_REDIRECT_URI = "http://www.brihaspatiinfotech.com"
    let INSTAGRAM_ACCESS_TOKEN = "access_token"
    let INSTAGRAM_SCOPE = "likes+comments+relationships"

    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        cameraBool = false
        camerafromDescription = false
        DescriptionTxtViewBool = false
        myProfileBool = false
        myPostsBool = false
        myBestPostsBool = false
        TutorialsBtnBool = false
        ShareImageFromCamera = false
        dismissBestPost = false
        dismissMyPost = false
        imageCaptureBool = false
        dismissMyProfile = false
        BestPostLongClickBool = false
        FacebookLoginBool = false
        GetpostIDBool = false
        

        
       AppDelegate.setupGoogleAnalytics()
        
        // [START register_for_notifications]
        //        if #available(iOS 10.0, *)
        //        {
        //            let authOptions : UNAuthorizationOptions = [.Alert, .Badge, .Sound]
        //            UNUserNotificationCenter.currentNotificationCenter().requestAuthorizationWithOptions(
        //                authOptions,
        //                completionHandler: {_,_ in })
        //
        //            // For iOS 10 display notification (sent via APNS)
        //            UNUserNotificationCenter.currentNotificationCenter().delegate = self
        //            // For iOS 10 data message (sent via FCM)
        //            FIRMessaging.messaging().remoteMessageDelegate = self
        //
        //        }
        //        else
        //        {
        if #available(iOS 8.0, *)
        {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        else
        {
            // Fallback on earlier versions
        }
        
        //      }
        
        
        // [END register_for_notifications]
        // Use Firebase library to configure APIs
        FIRApp.configure()
        
        
        // Add observer for InstanceID token refresh callback.
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(self.tokenRefreshNotification),
                                                         name: kFIRInstanceIDTokenRefreshNotification,
                                                         object: nil)

        
//        let settings: UIUserNotificationSettings =
//            UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
//        application.registerUserNotificationSettings(settings)
//        application.registerForRemoteNotifications()
//        
//        let token = FIRInstanceID.instanceID().token()!
        
        

        
//   FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
//                    kFIRParameterContentType:"cont",
//                    kFIRParameterItemID:"1"
//                    ])
        
        //    FIRAnalytics.logEventWithName("share_image", parameters: nil)
        
        
//  GADMobileAds.configureWithApplicationID("ca-app-pub-7356930517533133~6537543600");
        
    GADMobileAds.configureWithApplicationID("ca-app-pub-3940256099942544~1458002511");
        
        
        //////////////////////////////////////////// Set Status bar text color ///////////////////////////////////////////////////////////
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
        
         FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
        var userSaved = String()
        if let userLoggedIn = NSUserDefaults.standardUserDefaults().valueForKey("InstagramUser")
        {
            userSaved = userLoggedIn as! String
        }
      
        print("userSaved = \(userSaved)")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        
        if userSaved != ""
        {
            if userSaved == "InstagramUser"
            {
                let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                let middle = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
                let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                let bottom = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
                
                let snapContainer = SnapContainerViewController.containerViewWith(left,
                                                                                  middleVC: middle,
                                                                                  rightVC: right,
                                                                                  topVC: top,
                                                                                  bottomVC: nil)
                
                snapContainer.BestPostStr = "BestPostStr"
                
                middle.didCancel = { () in
                   // print("didCancel")
                    
                    if myProfileBool == true
                    {
                        let cameraScreen = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                        //cameraScreen.newImage = image
                        navigationController.pushViewController(cameraScreen, animated: true)
                        myProfileBool = false
                    }
                    
                    if myPostsBool == true
                    {
                        let profileScreen = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                        //votingScreen.newImage = newImage
                        
                        // GetpostIDBool = true
                        
                        let transition = CATransition()
                        transition.duration = 0.5
                        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                        transition.type = kCATransitionPush
                        transition.subtype = kCATransitionFromTop
                        navigationController.view.layer.addAnimation(transition, forKey: nil)
                        
//                        let writeView : WriteViewController = self.storyboard?.instantiateViewControllerWithIdentifier("WriteView") as! WriteViewController
//                        self.navigationController?.pushViewController(writeView, animated: false)
                        
                        navigationController.pushViewController(profileScreen, animated: true)
                        myPostsBool = false
                    }
                    
                    
                    if myBestPostsBool == true
                    {
                        //self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                        let profileScreen = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                        //votingScreen.newImage = newImage
                        profileScreen.BestPostStr = "BestPostsAPI"
                       // profileScreen.BestPostStr = "BestPostStr"
                        navigationController.pushViewController(profileScreen, animated: true)
                        //dismissBestPost = true
                        myBestPostsBool = false
                    }
                    
                }
                
                middle.didFinishCapturingImage = {(image: UIImage) in
                   // print("didFinishCapturingImage")
                    
//self.newImage = image
                    imageCaptured = image
                    //self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                    
                    let cameraScreen = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
                    cameraScreen.newImage = imageCaptured
                    //  self.navigationController?.presentViewController(cameraScreen, animated: false, completion: nil)
                    navigationController.pushViewController(cameraScreen, animated: true)
                    imageCaptureBool = true
                    
                }
                navigationController.viewControllers = [snapContainer]
               
            }
            else
            {
                let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                let middle = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
                let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                let bottom = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
                
                let snapContainer = SnapContainerViewController.containerViewWith(left,
                                                                                  middleVC: middle,
                                                                                  rightVC: right,
                                                                                  topVC: top,
                                                                                  bottomVC: nil)
                
                snapContainer.BestPostStr = "BestPostStr"
                
                middle.didCancel = { () in
                  //  print("didCancel")
                    
                    if myProfileBool == true
                    {
                        let cameraScreen = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                        //cameraScreen.newImage = image
                        navigationController.pushViewController(cameraScreen, animated: true)
                        myProfileBool = false
                    }
                    
                    if myPostsBool == true
                    {
                        let profileScreen = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                        //votingScreen.newImage = newImage
                        
                        let transition = CATransition()
                        transition.duration = 0.5  // kCAMediaTimingFunctionEaseInEaseOut
                        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                        transition.type = kCATransitionMoveIn
                        transition.subtype = kCATransitionFromLeft
                        navigationController.view.layer.addAnimation(transition, forKey: kCATransition)
                        
                        navigationController.pushViewController(profileScreen, animated: true)
                        myPostsBool = false
                    }
                    
                    
                    if myBestPostsBool == true
                    {
                        //self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                        let profileScreen = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                        //votingScreen.newImage = newImage
                        profileScreen.BestPostStr = "BestPostStr"
                        navigationController.pushViewController(profileScreen, animated: true)
                        myBestPostsBool = false
                    }
                    
                }
                
                middle.didFinishCapturingImage = {(image: UIImage) in
                   // print("didFinishCapturingImage")
                    
                    imageCaptured = image
                    
                    let cameraScreen = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
                    cameraScreen.newImage = imageCaptured
            
                    navigationController.pushViewController(cameraScreen, animated: true)
                    imageCaptureBool = true
                    
                }
                
                navigationController.viewControllers = [snapContainer]
            }
            
         
        }
        else
        {
            
            let homeScreen = storyboard.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
            navigationController.viewControllers = [homeScreen]
            
        }

        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
        return true
    }

    
    // [START receive_message]
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
                     fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void)
    {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        
        // Print full message.
        print("%@", userInfo)
    }
    // [END receive_message]
    
    
    
    // [START refresh_token]
    func tokenRefreshNotification(notification: NSNotification)
    {
        if let refreshedToken = FIRInstanceID.instanceID().token()
        {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    // [END refresh_token]
    
    // [START connect_to_fcm]
    func connectToFcm()
    {
        FIRMessaging.messaging().connectWithCompletion { (error) in
            if (error != nil)
            {
                print("Unable to connect with FCM. \(error)")
            }
            else
            {
                print("Connected to FCM.")
            }
        }
    }
    // [END connect_to_fcm]
    
    
    
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
   
    
    
    func applicationWillResignActive(application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication)
    {
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }

    
    func applicationWillEnterForeground(application: UIApplication)
    {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication)
    {
        FBSDKAppEvents.activateApp()
        
        connectToFcm()
    }

    func applicationWillTerminate(application: UIApplication)
    {
//        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
//        loginManager.logOut()
        
        FBSDKAccessToken.setCurrentAccessToken(nil)
        FBSDKProfile.setCurrentProfile(nil)
        
    }
    
    
    
    //    // [START ios_10_message_handling]
    //    @available(iOS 10, *)
    //    extension AppDelegate : UNUserNotificationCenterDelegate
    //    {
    //
    //        // Receive displayed notifications for iOS 10 devices.
    //        func userNotificationCenter(center: UNUserNotificationCenter,
    //                                    willPresentNotification notification: UNNotification,
    //                                                            withCompletionHandler completionHandler: (UNNotificationPresentationOptions) -> Void)
    //        {
    //            let userInfo = notification.request.content.userInfo
    //            // Print message ID.
    //            print("Message ID: \(userInfo["gcm.message_id"]!)")
    //
    //            // Print full message.
    //            print("%@", userInfo)
    //        }
    //    }
    //
    //    extension AppDelegate : FIRMessagingDelegate
    //    {
    //        // Receive data message on iOS 10 devices.
    //        func applicationReceivedRemoteMessage(remoteMessage: FIRMessagingRemoteMessage)
    //        {
    //            print("%@", remoteMessage.appData)
    //        }
    //    }
    //    
    //    // [END ios_10_message_handling]



}

