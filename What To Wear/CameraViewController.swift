//
//  CameraViewController.swift
//  What To Wear
//
//  Created by brst on 8/22/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import Social

class CameraViewController: UIViewController,UIImagePickerControllerDelegate,UIPickerViewDelegate,UINavigationControllerDelegate,FBSDKSharingDelegate
{

    //MARK:- VARIABLES DECLARTION
    var createPostDict = NSMutableDictionary()
    var savedUserID = String()
    var DescriptionBtnCheck = String()
    var dataModel = NSData()
    var DescriptionAddedStr = String()
    var newImage = UIImage(named: "")
    let imagePicker: UIImagePickerController! = UIImagePickerController()
    var postDetailDict = NSMutableDictionary()
    var savedImage = String()
    var savedArrayValues = NSMutableDictionary()
    var ImageCheck = String()
    
    
    
    //MARK:- OUTLETS DECLARATION
    @IBOutlet var PhotoView: UIImageView!
    @IBOutlet var DescriptionTextView: UITextView!
    @IBOutlet var DescriptionBtn: UIButton!
    @IBOutlet var BtnsWrapperView: UIView!
    @IBOutlet var PlaceHolderTextViewLbl: UILabel!
    @IBOutlet var ShareBtn: UIButton!
    @IBOutlet var ImageBtn: UIButton!
    @IBOutlet var BestPhotosBtn: UIButton!
    @IBOutlet var VotingBtn: UIButton!
    @IBOutlet var ProfileBtn: UIButton!
    
  
    
    //MARK:- VIEW-DID-LOAD
    override func viewDidLoad()
    {
        super.viewDidLoad()

        
////////////////////////////////////////////////// GoogleAnalyticsRegisterView STARTS //////////////////////////////////////////////////////////
        
        GoogleAnalyticsRegisterView()
        
  //////////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
 ///////////////////////////////////////////////////////////// NAVBAR AND STATUS BAR ///////////////////////////////////////////////////////////       
        
        UIApplication.sharedApplication().statusBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
  //////////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
/////////////////////////////////////////////////////////// SET IMAGE ON IMAGEVIEW //////////////////////////////////////////////////////////
    
        self.DescriptionTextView.hidden = true
        
        //print("captured image = \(imageCaptured)")
        
        if imageCaptured != nil
        {
            
           // print("new image clicked = \(newImage)")
            PhotoView.image = imageCaptured
            
            rotateImage(PhotoView.image!)
            
            print("image.Orientation = \(PhotoView.image?.imageOrientation)")
           // convertImageToBase64(PhotoView.image!)
            
        }
        
 //////////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////////////
      

    }
    
    
    
    
    //MARK:- Rotate Image on ImageView
    func rotateImage(image:UIImage)->UIImage
    {
        var rotatedImage = UIImage();
        switch image.imageOrientation
        {
        case UIImageOrientation.Right:
            
            print(" UIImageOrientation.Right = \(UIImageOrientation.Right)")
            rotatedImage = UIImage(CGImage:image.CGImage!, scale: 1, orientation:UIImageOrientation.Up);
            
        case UIImageOrientation.Down:
            
            print(" UIImageOrientation.Down = \(UIImageOrientation.Down)")
            rotatedImage = UIImage(CGImage:image.CGImage!, scale: 1, orientation:UIImageOrientation.Left);
            
        case UIImageOrientation.Left:
            print(" UIImageOrientation.Left = \(UIImageOrientation.Left)")
            rotatedImage = UIImage(CGImage:image.CGImage!, scale: 1, orientation:UIImageOrientation.Up);
            
        default:
            print(" UIImageOrientation.Right = \(UIImageOrientation.Right)")
            rotatedImage = UIImage(CGImage:image.CGImage!, scale: 1, orientation:UIImageOrientation.Right);
        }
        return rotatedImage;
    }
    
    
    
    
    //MARK:- GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "CameraView")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }


    //MARK:- VIEW-WILL-APPEAR
    override func viewWillAppear(animated: Bool)
    {
        
        
 /////////////////////////////////////////////////////////// SET LOCALIZED STRINGS //////////////////////////////////////////////////////////
        
        self.PlaceHolderTextViewLbl.text =  NSLocalizedString("PLACEHOLDER_TEXT", comment: "The placeholder title")
        
  //////////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
 
        
        
        
  /////////////////////////////////////////////////////////// SET NSUSER DEFAULTS //////////////////////////////////////////////////////////
        
        var userSaved = String()
        userSaved = NSUserDefaults.standardUserDefaults().valueForKey("InstagramUser") as! String
        print("userSaved = \(userSaved)")
        var userDefaults = NSUserDefaults.standardUserDefaults()
        
        if userSaved != ""
        {
            if userSaved == "InstagramUser"
            {
                savedUserID = userDefaults.valueForKey("userID") as! String
                print("savedUserID=\(savedUserID)")
            }
            else
            {
                savedUserID = userDefaults.valueForKey("userID") as! String
                print("savedUserID=\(savedUserID)")
            }
            
        }
 
        
        
 //////////////////////////////////////////////////////////////////////////// ENDS ///////////////////////////////////////////////////////////////////////////// 
        
        
        
        
        
        
        
//////////////////////////////////////////////// SET BOOL CONDITIONS FOR TEXTVIEW ///////////////////////////////////////////////
        
        if DescriptionTxtViewBool == true
        {
            
            self.PlaceHolderTextViewLbl.hidden = true
             self.PlaceHolderTextViewLbl.text = ""
            
            if let savedValue = NSUserDefaults.standardUserDefaults().stringForKey("TextDescrption")
            {
                //print("description added = \( savedValue )")
                DescriptionTextView.hidden = false
                DescriptionTextView.text = savedValue
            }

            DescriptionTxtViewBool = false
         }
        
        
//////////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////////////
        
        
        
        
    
        
        
 ///////////////////////////////////////////////// SET BOOL CONDITIONS FOR CAMERA /////////////////////////////////////////////////
        
          if camerafromDescription == true
         {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let left = storyboard.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
            let middle = storyboard.instantiateViewControllerWithIdentifier("DKCamera") as! DKCamera
            let right = storyboard.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
            let top = storyboard.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
            let bottom = storyboard.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
            
            let snapContainer = SnapContainerViewController.containerViewWith(left,
                                                                              middleVC: middle,
                                                                              rightVC: right,
                                                                              topVC: top,
                                                                              bottomVC: nil)
            
            snapContainer.BestPostStr = "BestPostStr"
            
            middle.didCancel = { () in
               // print("didCancel")
                
                if myProfileBool == true
                {
                    //                self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                    //                dismissMyProfile = true
                    
                    let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
                    //cameraScreen.newImage = image
                    self.navigationController?.pushViewController(cameraScreen, animated: true)
                    
                    myProfileBool = false
                }
                
                if myPostsBool == true
                {
                    
                    //self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                    //dismissMyPost = true
                    
                    
                    let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                    //votingScreen.newImage = newImage
                    let transition = CATransition()
                    transition.duration = 0.5  // kCAMediaTimingFunctionEaseInEaseOut
                    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    transition.type = kCATransitionMoveIn
                    transition.subtype = kCATransitionFromLeft
                    self.navigationController?.view.layer.addAnimation(transition, forKey: kCATransition)
                    self.navigationController?.pushViewController(profileScreen, animated: true)
                    
                    myPostsBool = false
                }
                
                
                if myBestPostsBool == true
                {
                    //self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                    let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
                    //votingScreen.newImage = newImage
                    profileScreen.BestPostStr = "BestPostStr"
                    self.navigationController?.pushViewController(profileScreen, animated: true)
                    //dismissBestPost = true
                    myBestPostsBool = false
                }
                
            }
            
            middle.didFinishCapturingImage = {(image: UIImage) in
              //  print("didFinishCapturingImage")
                
                self.newImage = image
                imageCaptured = image
               // self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
                
                let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
                cameraScreen.newImage = imageCaptured
                //self.navigationController?.presentViewController(cameraScreen, animated: false, completion: nil)
                  self.navigationController?.pushViewController(cameraScreen, animated: true)
                
                imageCaptureBool = true
                
            }
            
            self.navigationController?.pushViewController(snapContainer, animated: true)
            //   self.navigationController?.presentViewController(snapContainer, animated: true, completion: nil)
            
            camerafromDescription = false
            
        }
        
        if dismissBestPost == true
        {
            
            var subview = UIView()
            subview = UIView(frame: (CGRectMake(0, 0, 320, 568)))
            subview.backgroundColor = UIColor.blackColor()
            self.view.addSubview(subview)

            
            //  let camera = DKCamera()
            let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
            //votingScreen.newImage = newImage
            self.navigationController?.pushViewController(profileScreen, animated: true)
            dismissBestPost = false
        }
        
        
        if dismissMyPost == true
        {
            
            var subview = UIView()
            subview = UIView(frame: (CGRectMake(0, 0, 320, 568)))
            subview.backgroundColor = UIColor.blackColor()
            self.view.addSubview(subview)

            
            //  let camera = DKCamera()
            let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
            profileScreen.newImage = imageCaptured
            self.navigationController?.pushViewController(profileScreen, animated: true)
            dismissMyPost = false
        }
        
        
//        if imageCaptureBool == true
//        {
//            //self.newImage = image
//            
//            let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("CameraViewController") as! CameraViewController
//            //cameraScreen.newImage = image
//            self.navigationController?.pushViewController(cameraScreen, animated: false)
//            
//             imageCaptureBool = false
//        }
        
        if dismissMyProfile == true
        {
            //self.newImage = image
            var subview = UIView()
            subview = UIView(frame: (CGRectMake(0, 0, 320, 568)))
            subview.backgroundColor = UIColor.blackColor()
            self.view.addSubview(subview)

            
            let cameraScreen = self.storyboard?.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
            //cameraScreen.newImage = image
            self.navigationController?.pushViewController(cameraScreen, animated: true)
            
            dismissMyProfile = false
        }

//////////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
        
        
 /////////////////////////////////////////////// SET BOOL CONDITION FOR SHARE IMAGE //////////////////////////////////////////////
        
        if ShareImageFromCamera == true
        {
            var subview = UIView()
            subview = UIView(frame: (CGRectMake(0, 0, 320, 568)))
            subview.backgroundColor = UIColor.blackColor()
            self.view.addSubview(subview)

            let defaultsArray = NSUserDefaults.standardUserDefaults()
            // let defaultsArray = NSUserDefaults.standardUserDefaults()
            let savedValueArray = defaultsArray.objectForKey("postDetailDict") as? [AnyObject] ?? [AnyObject]()
            print("saved array is = \(savedValueArray[0])")

            let votingScreen = storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
           // votingScreen.newImage = imageCaptured
            //votingScreen.savedImage = savedValueArray[0].valueForKey("img_para") as! String
            votingScreen.ImageCheck = "ImageCheck"
            votingScreen.savedArrayValues = savedValueArray[0] as! NSMutableDictionary
            self.navigationController?.pushViewController(votingScreen, animated: true)
            ShareImageFromCamera = false
        }
        
        
//////////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////////////
        

}
  
    
    
    //MARK:- IMAGE PICKER DELEGATE
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        
        let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        PhotoView.contentMode = .ScaleAspectFit
        PhotoView.image = pickedImage
        newImage = pickedImage
        self.dismissViewControllerAnimated(true, completion: nil)
        
         //self.BtnsWrapperView.hidden = false
      
        self.DescriptionTextView.hidden = false
        self.PlaceHolderTextViewLbl.hidden = false
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        
        cameraBool = true
        self.dismissViewControllerAnimated(true, completion: nil)
      //  self.navigationController?.popViewControllerAnimated(false)
    }
    
    
    //MARK:- CANCEL BUTTON ACTION
    @IBAction func CancelViewBtnAction(sender: AnyObject)
    {
      //  print("cancel btn")
        
 /////////////////////////////////////////////////// SET BOOL FOR TEXTVIEW TO NIL /////////////////////////////////////////////////////
        
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "TextDescrption")
        
 //////////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////////////
        
        
        
        
/////////////////////////////////////////////////////// POP TO VIEW CONTROLLER //////////////////////////////////////////////////////////////
        
      //  self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        self.navigationController?.popViewControllerAnimated(true)
        imageCaptureBool = true
        
 //////////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////////////
        
}
    
    
    
    
    //MARK:- PROFILE BUTTON ACTION
    @IBAction func ProfileBtnAction(sender: AnyObject)
    {
        
        /////////////////////////////////////// MOVE TO MY PROFILE VIEW CONTROLLER ////////////////////////////////////////////
        
        let profileScreen = storyboard?.instantiateViewControllerWithIdentifier("MyProfileViewController") as! MyProfileViewController
        //votingScreen.newImage = newImage
        self.navigationController?.pushViewController(profileScreen, animated: true)
        
      //////////////////////////////////////////////////////////////////////////// ENDS ////////////////////////////////////////////////////////////////////////
        
    }
    
    
    
    
    //MARK:- VOTING BUTTON ACTION
    @IBAction func VotingBtnAction(sender: AnyObject)
    {
        
         /////////////////////////////////////// MOVE TO VOTING VIEW CONTROLLER ////////////////////////////////////////////
        
            let votingScreen = storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
            votingScreen.newImage = imageCaptured
           self.navigationController?.pushViewController(votingScreen, animated: true)
        
         //////////////////////////////////////////////////////////////////////////// ENDS ////////////////////////////////////////////////////////////////////////
    }
    
    
    //MARK:- BEST PHOTOS BTN ACTION
    @IBAction func BestPhotosBtnAction(sender: AnyObject)
    {
        
        ////////////////////////////////////// MOVE TO MY BEST POSTS VIEW CONTROLLER ////////////////////////////////////////
        
        let myBestScreen = storyboard?.instantiateViewControllerWithIdentifier("MyBestPostsViewController") as! MyBestPostsViewController
        //votingScreen.newImage = newImage
        self.navigationController?.pushViewController(myBestScreen, animated: true)
        
        //////////////////////////////////////////////////////////////////////////// ENDS ////////////////////////////////////////////////////////////////////////
        
    }
    
    
    //MARK:- IMAGE DOWNLOAD BTN ACTION
    @IBAction func ImageDownloadBtnAction(sender: AnyObject)
    {
        //print("image dwnload btn")
        
          ////////////////////////////////////// IMAGE SAVE TO iPHONE's PHOTOS ////////////////////////////////////////
        
        UIImageWriteToSavedPhotosAlbum(self.PhotoView.image!, self, #selector(self.imageSaved(_:didFinishSavingWithError:contextInfo:)), nil)
        
        //////////////////////////////////////////////////////////// ENDS ///////////////////////////////////////////////////////////////////
        
    }
    
    
    
    //MARK:- IMAGE SAVED IN GALLERY METHOD
    func imageSaved(image: UIImage!, didFinishSavingWithError error: NSError?, contextInfo: AnyObject?)
    {
        
        ///////////////////////////////////////// If Error Exists during Saving Image ////////////////////////////////////////////////
        
        if (error != nil)
        {
          //  print("error")
            
            var ErrorImage = UIAlertView(title: "", message: NSLocalizedString("ALERT_IMAGE_NOT_SAVED", comment: "The not saved image title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
            ErrorImage.show()
        }
            
         //////////////////////////////////////////////////////////// ENDS //////////////////////////////////////////////////////////////////
            
            
            
      ////////////////////////////////////////////// Image saved sucessfully in photo's ////////////////////////////////////////////////////////
            
        else
        {
          // print("image saved")
            
            var saveImage = UIAlertView(title: "", message: NSLocalizedString("ALERT_IMAGE_SAVED", comment: "The saved image title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
            saveImage.show()
        }
        
    //////////////////////////////////////////////////////////////////// ENDS ///////////////////////////////////////////////////////////////////////////
        
    }
    
    
    //MARK:- SHARE BTN ACTION
    @IBAction func ShareBtnAction(sender: AnyObject)
    {
       // print("share btn")
        
//        var userSaved = String()
//        if let userLoggedIn = NSUserDefaults.standardUserDefaults().valueForKey("InstagramUser")
//        {
//            userSaved = userLoggedIn as! String
//        }
//        
//        print("userSaved = \(userSaved)")
//        
//        if userSaved != ""
//        {
//            if userSaved == "InstagramUser"
//            {
//                // share to instagram
//                InstagramManager.sharedManager.postImageToInstagramWithCaption(self.ResizeImage(PhotoView.image!, targetSize: CGSizeMake(1080.0, 1350.0)), instagramCaption: "My Photo", view: self.view)
//            }
//            else if userSaved == "FacebookUser"
//            {
//                if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook)
//                {
//                    let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//                    facebookSheet.setInitialText(self.DescriptionTextView.text!)
//                    facebookSheet.addImage(self.ResizeImage(PhotoView.image!, targetSize: CGSizeMake(1080.0, 1350.0)))
//                    
//                    self.presentViewController(facebookSheet, animated: true, completion: nil)
//                    
//                    facebookSheet.completionHandler = {(result: SLComposeViewControllerResult) -> Void in
//                        switch result
//                        {
//                            case .Cancelled:
//                                    print("Post Canceled")
//                            case .Done:
//                                    print("Post Sucessful")
//                             default:
//                                      break
//                        }
//                        
//                       // self.dismiss(animated: true, completion: { _ in })
//                        //self.dismissViewControllerAnimated(true, completion: nil)
//                    }
//                    
//                    
//                }
//                else
//                {
//                    let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
//                    self.presentViewController(alert, animated: true, completion: nil)
//                }
        
        
        
        
        let photoContent = FBSDKSharePhotoContent()
        let photo : FBSDKSharePhoto = FBSDKSharePhoto()
        photo.image = (self.ResizeImage(PhotoView.image!, targetSize: CGSizeMake(1080.0, 1350.0)))
        photo.userGenerated = true
        photoContent.photos = [photo]
        FBSDKShareDialog.showFromViewController(self, withContent: photoContent, delegate:self)
//            }
//            else
//            {
//                print("user is neither from instagram nor from facebook")
//        
//                CreateNewPostAPI()
//            }
        
//        }

    
    }
    

    func sharer(sharer: FBSDKSharing!, didCompleteWithResults results: [NSObject: AnyObject])
    {
        print("sharer didCompleteWithResults, results.count\(results.count)")
        print("facebook photo sharing result=\(results)")
        // still cannot get post id from photo upload
    }
    
    func sharer(sharer: FBSDKSharing!, didFailWithError error: NSError!)
    {
        print("sharer NSError")
        print(error.description)
    }
    
    func sharerDidCancel(sharer: FBSDKSharing!)
    {
        print("sharerDidCancel")
    }
    
    //MARK:- Rotate ImageView w.r.t Orignal Image
    func fixOrientation(img:UIImage) -> UIImage
    {
        if (img.imageOrientation == UIImageOrientation.Up)
        {
            return img;
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale);
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.drawInRect(rect)
        
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return normalizedImage;
        
    }

    
    
    //MARK:- Resize Image clicked to given pixels for cropping purpose
    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage
    {
        
        
        if (image.imageOrientation == UIImageOrientation.Up)
        {
            return image;
        }

        
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSizeMake(size.width * heightRatio, size.height * heightRatio)
        } else {
            newSize = CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRectMake(0, 0, newSize.width, newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.drawInRect(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    
    
    
    
    //MARK:- CreateNewPostAPI
    func CreateNewPostAPI()
    {
        
        let spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")

        let widthInPixelsPhotoView = PhotoView.frame.width * UIScreen.mainScreen().scale
        let heightInPixelsPhotoView = PhotoView.frame.height * UIScreen.mainScreen().scale
        print("widthInPixelsPhotoView=\(widthInPixelsPhotoView)")
        print("heightInPixelsPhotoView=\(heightInPixelsPhotoView)")
        
       // var image:UIImage =  fixOrientation(PhotoView.image!)
        let image:UIImage = self.ResizeImage(PhotoView.image!, targetSize: CGSizeMake(1080.0, 1350.0))
        
        
        
        let widthInPixels = image.size.width * image.scale
        let heightInPixels = image.size.height * image.scale
        
        print("widthInPixels=\(widthInPixels)")
        print("heightInPixels=\(heightInPixels)")
        
       // PhotoView.transform = CGAffineTransformMakeRotation(3.14/2);
        
        let imageData2 = UIImageJPEGRepresentation(image, 0.9)
        let base64String = imageData2!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)) // encode the image
        let strImage64 = base64String.stringByReplacingOccurrencesOfString("+", withString: "%2B") as String
    //  print("strImage64 = \(strImage64)")
        
    
        let post = NSString(format:"user_id=%@&img_para=%@&photo_description=%@",savedUserID,strImage64,self.DescriptionTextView.text)
        
     //   print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        let postLength = String(dataModel.length)
        let url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/post")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
                
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
            
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()
                        
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                    
                    self.createPostDict = jsonResult as! NSMutableDictionary
                    print("self.updateResultDict = \(self.createPostDict)")
                    
                    let statusStr = self.createPostDict.valueForKey("status") as! String
                    self.postDetailDict = self.createPostDict.valueForKey("data") as! NSMutableDictionary
                    
                    if statusStr == "success"
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            let votingScreen = self.storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
                            
                            //GetpostIDBool = true
                            
                            NSUserDefaults .standardUserDefaults().setObject(self.postDetailDict.valueForKey("post_id") as! String, forKey: "post_id")
                            
                            
                            var array = NSMutableArray()
                            array.addObject(self.createPostDict.valueForKey("data") as! NSMutableDictionary)
                            let defaultsArray = NSUserDefaults.standardUserDefaults()
                            defaultsArray.setObject(array, forKey: "postDetailDict")
                            
                            
                          
                            let savedValueArrayDetaild = defaultsArray.objectForKey("postDetailDict") as? [AnyObject] ?? [AnyObject]()
                            print("saved array is = \(savedValueArrayDetaild[0])")

                            
            
                            votingScreen.ImageCheck = "ImageCheck"
                            votingScreen.savedArrayValues = savedValueArrayDetaild[0] as! NSMutableDictionary
//                            votingScreen.newImage = imageCaptured
//                            votingScreen.postDetailDict = self.createPostDict.valueForKey("data") as! NSMutableDictionary
                            self.navigationController?.pushViewController(votingScreen, animated: true)
                            
                            spinningIndicator.hide(true)
                        })

                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        })

                    }
           
                    
                }
                
                
            })
            
            
            }
            
            
        })
        task.resume()
        
        
    }

    
    
    //MARK:- DESCRIPTION BTN ACTION
    @IBAction func DescriptionBtnAction(sender: AnyObject)
    {
       // print("description btn")
        
        let addDescriptionScreen =  storyboard?.instantiateViewControllerWithIdentifier("AddDescriptionViewController") as! AddDescriptionViewController
        addDescriptionScreen.newImage = imageCaptured
        self.navigationController?.presentViewController(addDescriptionScreen, animated: false, completion: nil)
    }
    
    
    //MARK:- DID-RECIEVE-MEMORY-WARNING 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
