//
//  ImageDescriptionViewController.swift
//  What To Wear
//
//  Created by brst on 8/22/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class ImageDescriptionViewController: UIViewController
{

    //MARK:- VARIABLES DECLARATION
    var newImage = UIImage(named: "")
    var ImageCheck = String()
    var post_id = String()
    var savedArrayValues = NSMutableDictionary()
    

    
    //MARK:- OUTLETS DECLARTION
    @IBOutlet weak var DislikeLbl: UILabel!
    @IBOutlet weak var LikeLbl: UILabel!
    @IBOutlet weak var DaysAgoLbl: UILabel!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var instaImageView: UIImageView!
    @IBOutlet var user_websiteLbl: UILabel!
    @IBOutlet var DescriptionTextView: UITextView!
    @IBOutlet var BlackBlurView: UIView!
    @IBOutlet var PersonMainImage: UIImageView!
    @IBOutlet var PersonEmailLbl: UILabel!
    @IBOutlet var PersonNameLbl: UILabel!
    @IBOutlet var profileImagePerson: UIImageView!
    @IBOutlet var WrapperViewInfo: UIView!
    @IBOutlet var InstagramBtn: UIButton!
    @IBOutlet var BtnWebsiteLink: UIButton!
    
    
    //MARK:- VIEW-DID-LOAD
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
      GoogleAnalyticsRegisterView()

        if backgroundImage != nil
        {
            backgroundImage.image = newImage
        }
        

        if PersonMainImage != nil
        {
            PersonMainImage.image = newImage
        }
        
        
        if ImageCheck == "ImageCheck"
        {
            
            let total = savedArrayValues.allKeys.count
            //if savedArrayValues != []
            if total > 0
            {
                
                print("saved array is viewDidLoad = \(savedArrayValues)")
                
                post_id = self.savedArrayValues.valueForKey("post_id") as! String
                print("self.post_id=\(post_id)")
                
                
                self.DescriptionTextView.text = self.savedArrayValues.valueForKey("photo_description") as! String

                
                
                var profileUrl: String = self.savedArrayValues.valueForKey("img_para") as! String
                var url: NSURL = NSURL(string: profileUrl)!
                
                if let imageURL = NSURL(string: profileUrl)
                {
                    if let data = NSData(contentsOfURL: imageURL)
                    {
                        self.PersonMainImage.image = UIImage(data: data)
                        self.backgroundImage.image = UIImage(data: data)
                    }
                }
                
            }
            
        }
        
        
        

    }
    
    
    //MARK:- GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "VotingImageDescriptionScreen")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }


    //MARK:- VIEW-WILL-APPEAR
    override func viewWillAppear(animated: Bool)
    {
        UIApplication.sharedApplication().statusBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
        profileImagePerson.layer.borderWidth=1.0
        profileImagePerson.layer.masksToBounds = false
        profileImagePerson.layer.borderColor = UIColor.whiteColor().CGColor
        profileImagePerson.layer.cornerRadius = 13
        profileImagePerson.layer.cornerRadius = profileImagePerson.frame.size.height/2
        profileImagePerson.clipsToBounds = true
        
        
        WrapperViewInfo.layer.cornerRadius = 10
        
        var userSaved = String()
        if let userwebsite = NSUserDefaults.standardUserDefaults().valueForKey("user_website")
        {
            
            if userwebsite as! String == ""
            {
                self.BtnWebsiteLink.userInteractionEnabled = false
                self.user_websiteLbl.hidden = true
            }
            else
            {
                self.user_websiteLbl.text = userwebsite as! String
            }
            
            
        }

        
        var savedName = String()
        if let username = NSUserDefaults.standardUserDefaults().valueForKey("userName")
        {
            savedName = username as! String
            self.PersonNameLbl.text = savedName
        }
        //  print("savedName = \(savedName) ")
        var savedImage = NSUserDefaults.standardUserDefaults().valueForKey("userImage") as! String
        var imageURL: NSURL = NSURL(string: savedImage)!
        if let data : NSData = NSData(contentsOfURL: imageURL)
        {
            self.profileImagePerson.image = UIImage(data: data)!
        }

        
        var userSavedtype = String()
        if let userLoggedIn = NSUserDefaults.standardUserDefaults().valueForKey("InstagramUser")
        {
            userSavedtype = userLoggedIn as! String
        }
        
       // print("userSaved = \(userSavedtype)")
        
        if userSavedtype != ""
        {
            if userSavedtype == "InstagramUser"
            {
                self.InstagramBtn.hidden = false
                self.instaImageView.hidden = false
            }
            else
            {
                self.instaImageView.hidden = true
                self.InstagramBtn.hidden = true
            }
            
        }

   

        
    }
    
    
    //MARK:- INSTAGRAM BTN ACTION
    @IBAction func InstagramBtnAction(sender: AnyObject)
    {
        //print("InstagramBtnAction")
        openInstagram()
    }
    
    
    //MARK:- OPEN INSTAGRAM METHOD
    func openInstagram()
    {
        var instURL: NSURL = NSURL (string: "instagram://user?username=Instagram")! // Replace = Instagram by the your instagram user name
        var instWB: NSURL = NSURL (string: "https://instagram.com/instagram/")! // Replace the link by your instagram weblink self.PersonEmailLbl.text
        
        if (UIApplication.sharedApplication().canOpenURL(instURL))
        {
            // Open Instagram application
            UIApplication.sharedApplication().openURL(instURL)
        }
        else
        {
            // Open in Safari
            UIApplication.sharedApplication().openURL(instWB)
        }
    }

    
    //MARK:- WEBSITE BTN ACTION
    @IBAction func WebsiteBtnAction(sender: AnyObject)
    {
        //print("WebsiteBtnAction")
        
       // UIApplication.sharedApplication().openURL(NSURL(string:"http://www.facebook.com")!)
        UIApplication.sharedApplication().openURL(NSURL(string: "http://" + self.PersonEmailLbl.text!)!)
    }
    
    
    //MARK:- CANCEL BTN ACTION
    @IBAction func CancelBtnAction(sender: AnyObject)
    {
       // print("Cancel btn")
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    //MARK:- DID-RECEIVE-MEMORY-WARNING
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
