//
//  MyPostsTableViewCell.swift
//  What To Wear
//
//  Created by brst on 8/22/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class MyPostsTableViewCell: UITableViewCell
{

    
    
    @IBOutlet var UnderLineLbl: UILabel!
    @IBOutlet var WrapperImageBtn: UIButton!
    @IBOutlet var DislikeImage: UIImageView!
    @IBOutlet var LikeImage: UIImageView!
    @IBOutlet var DaysAgoLbl: UILabel!
    @IBOutlet var DislikeCountLbl: UILabel!
    @IBOutlet var LikeCountLbl: UILabel!
    @IBOutlet var MyPostImage: UIImageView!
    @IBOutlet var BackgroundTableView: UIView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
      
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
