//
//  MyPostsDetailViewController.swift
//  What To Wear
//
//  Created by brst on 8/23/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class MyPostsDetailViewController: UIViewController,UIAlertViewDelegate
{

    //MARK:- VARIABLES DECLARATION
    var dataModel = NSData()
    var savedUserID = String()
    var alertDelete = UIAlertView()
    var ImagePushed = UIImage(named: "")
    var LikeStr = String()
    var UnlikeStr = String()
    var particularPostDetailsArray = NSMutableDictionary()
    var PostID = String()
    var alertPostDelete = UIAlertView()
    
    
    //MARK:- OUTLETS DECLARATION
    @IBOutlet var HeaderView: UIView!
    @IBOutlet var CancelBtn: UIButton!
    @IBOutlet var CancelImage: UIImageView!
    @IBOutlet var BackgroundBlurImage: UIImageView!
    @IBOutlet var backgroundBlurView: UIView!
    @IBOutlet var WrapperInfoView: UIView!
    @IBOutlet var WrapperLikeDisView: UIView!
    @IBOutlet var PostImage: UIImageView!
    @IBOutlet var DaysAgoLbl: UILabel!
    @IBOutlet var DislikeCountLbl: UILabel!
    @IBOutlet var LikeCountLbl: UILabel!
    @IBOutlet var DeleteBtn: UIButton!
    @IBOutlet var DeleteImage: UIImageView!
    @IBOutlet var DeleteLbl: UILabel!
    
    
    //MARK:- VIEW-DID-LOAD
    override func viewDidLoad()
    {
        super.viewDidLoad()

        GoogleAnalyticsRegisterView()
        alertDelete.delegate = self
        alertPostDelete.delegate = self
       self.DaysAgoLbl.text =  "6 " + NSLocalizedString("DAYS_AGO", comment: "The days ago title")
       self.DeleteLbl.text = NSLocalizedString("DELETE_POST", comment: "The delete title")
    }

    
    //MARK:- GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "MyPostsDetails")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    
    //MARK:- VIEW-WILL-APPEAR
    override func viewWillAppear(animated: Bool)
    {
        print("self.particularPostDetailsArray=\(self.particularPostDetailsArray)")
        
        
        if Reachability.isConnectedToNetwork() == false
        {
            var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
            alert.show()

            self.LikeCountLbl.text = ""
            self.DislikeCountLbl.text = ""
            self.DaysAgoLbl.text = ""
            
        }
        else
        {
//            var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
//            spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
//            spinningIndicator.hide(false, afterDelay: 1)
            
            UIApplication.sharedApplication().statusBarHidden = false
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            self.navigationController?.navigationBarHidden = true
            
            
            self.WrapperInfoView.layer.cornerRadius = 10
            self.PostImage.layer.cornerRadius = 10
            
            
            let array = self.particularPostDetailsArray.valueForKey("votes")
            let likeStr:AnyObject = array!.valueForKey("likes")!
            let dislikeStr:AnyObject = array!.valueForKey("unlike")!
            
            //var like:AnyObject = self.VotesDict.valueForKey("likes")!
            
            if  likeStr is NSNull
            {
                print("self.VotesDict is null")
                self.LikeCountLbl.text = "0"
            }
            else
            {
                //print("\(self.VotesDict.valueForKey("likes") as! String)")
                self.LikeCountLbl.text = likeStr as? String
            }
            
            
            if  dislikeStr is NSNull
            {
                print("self.VotesDict is null")
                self.DislikeCountLbl.text = "0"
            }
            else
            {
                //print("\(self.VotesDict.valueForKey("likes") as! String)")
                self.DislikeCountLbl.text = dislikeStr as? String
            }
            
            
            var profileUrl: String = self.particularPostDetailsArray.valueForKey("img_para") as! String
            var url: NSURL = NSURL(string: profileUrl)!
            
            if let imageURL = NSURL(string: profileUrl)
            {
                if let data = NSData(contentsOfURL: imageURL)
                {
                    self.PostImage.image = UIImage(data: data)
                    self.BackgroundBlurImage.image = UIImage(data: data)
                    //cell.MyPostImage.contentMode = UIViewContentMode.ScaleAspectFill
                }
            }
            
            
            self.PostID = self.particularPostDetailsArray.valueForKey("post_id") as! String
            print("self.PostID=\(self.PostID)")
            
            if (self.particularPostDetailsArray.valueForKey("post_time") != nil)
            {
                
                let strtemp = self.particularPostDetailsArray.valueForKey("post_time") as! String
                print("strtemp is \(strtemp)")
                
                let str = timedifference(strtemp)
                
                self.DaysAgoLbl.text = "  \(str) "
                
            }
            
            
            //         if LikeStr != ""
            //        {
            //            self.LikeCountLbl.text = LikeStr
            //        }
            //        
            //        if UnlikeStr != ""
            //        {
            //            self.DislikeCountLbl.text = UnlikeStr
            //        }
            //        
            //        if ImagePushed != ""
            //        {
            //            self.PostImage.image = ImagePushed
            //        }

        }

        
    }
    
    
    func timedifference(dateString: String)->String
    {
        let date = NSDate()
        
        //let messageTimezoneOffset = resultSet.intForColumn("timezoneOffset")
        
        
        /// GET TIMESTAMP IN UNIX
        let currentTimeStamp = Int64(date.timeIntervalSince1970*1000)
        
        print("current date = \(NSDate().timeIntervalSince1970 * 1000)")
        
        print("currentTimeStamp=\(currentTimeStamp)")
        // let timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
        
        let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
        print("timeDiff=\(timeDiff)")
        
        let nYears = timeDiff / (1000*60*60*24*365)
        
        
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        
        
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        
        
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        //println(nMinutes)
        
        var timeMsg = ""
        
        if nYears > 0
        {
            var yearWord = "years"
            if nYears == 1
            {
                yearWord = "year"
            }
            
            timeMsg = " \(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
            var monthWord = "months"
            if nMonths == 1
            {
                monthWord = "month"
            }
            
            timeMsg = " \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            timeMsg = " \(nDays) \(dayWord) ago"
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = " \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            
            timeMsg = " \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        return timeMsg
    }

    
    //MARK:- DELETE BTN ACTION
    @IBAction func DeleteBtnAction(sender: AnyObject)
    {
        // print("delete btn")
        alertDelete = UIAlertView(title: "", message: NSLocalizedString("ALERT_DELETE_POST", comment: "The delete title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_DELETE_NO", comment: "The delete no title"), otherButtonTitles: NSLocalizedString("ALERT_DELETE_YES", comment: "The delete yes title"))
        alertDelete.tag = 12
        alertDelete.show()
    }
    
    
    //MARK:- CANCEL BTN ACTION
    @IBAction func CancelBtnAction(sender: AnyObject)
    {
        //print("canccel btn")
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    
    
    //MARK:- ALERTVIEW DELEGATE
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alertDelete.tag == 12
        {
            if buttonIndex == 1
            {
                DeleteParticularPostAPI()
            }
        }
        
        if self.alertPostDelete.tag == 50
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    
    
    
    //MARK:- DeleteParticularPostAPI
    func DeleteParticularPostAPI()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        savedUserID = NSUserDefaults.standardUserDefaults().valueForKey("userID") as! String
        
        var post = NSString(format:"post_id=%@",self.PostID)
        
        print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        let postLength = String(dataModel.length)
        let url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/deletepost")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
                
                
                
                
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()
                        
                        
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {

                
                var statusStr = jsonResult.valueForKey("status") as! String
                    
                if statusStr == "Post Deleted"
                {
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        
                        self.alertPostDelete =  UIAlertView(title:"", message: NSLocalizedString("ALERT_POST_DELETE_TRUE", comment: "The delete post title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        self.alertPostDelete.tag = 50
                        self.alertPostDelete.show()
                        
                        spinningIndicator.hide(true)
                    })
                    
               }
               else
               {
                
                dispatch_async(dispatch_get_main_queue(), {
                    
                               var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_POST_DELETE_FALSE", comment: "The post not deleted title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                               alert.show()
                    
                            spinningIndicator.hide(true)
                      })
               }
                    
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }

    
    
    //MARK:- DID-RECEIVE-MEMORY-WARNING
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
