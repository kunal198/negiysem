//
//  ViewController.swift
//  What To Wear
//
//  Created by brst on 8/20/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController//,FBSDKLoginButtonDelegate
{

//    @IBOutlet var btnFacebook: FBSDKLoginButton!
//    @IBOutlet var ivUserProfileImage: UIImageView!
//    @IBOutlet var lblName: UILabel!
    
       //MARK:- VARIABLES DECLARATION
      var dataModel = NSData()
      var JsonResponseArray = NSMutableDictionary()
      var facebookDetailsArray = NSMutableDictionary()
      var emailFromFacebook = String()
      var FirstNameFacebook = String()
      var LastNameFacebook = String()
      var ImageURLFaceBook = String()
      var FullNameFacebook = String()
      var FaceBookID = String()
   
    
     //MARK:- OUTLETS DECLARATION
    @IBOutlet var FacebookLbl: UILabel!
    @IBOutlet var OrLbl: UILabel!
    @IBOutlet var LoginInstaLbl: UILabel!
    @IBOutlet var TitleLbl: UILabel!
    @IBOutlet var InstagramView: UIView!
    @IBOutlet var FacebookView: UIView!
    @IBOutlet var DifferenceView: UIView!
    @IBOutlet var FacebookBtn: UIButton!
    
    
    //MARK:- VIEWDIDLOAD METHOD
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
////////////////////////////////////////// GoogleAnalyticsRegisterView STARTS ////////////////////////////////////////////////
        
        GoogleAnalyticsRegisterView()
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
    }

    
    
    
     //MARK:- GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "LoginView")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    
    
    
     //MARK:- VIEWWILLAPPEAR
    override func viewWillAppear(animated: Bool)
    {
        
////////////////////////////////////////////////////// NAVBAR AND STATUS BAR /////////////////////////////////////////////////
        
        UIApplication.sharedApplication().statusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
///////////////////////////////////////////////////// CORNER RADIUS CODE ////////////////////////////////////////////////////////
        
        // self.configureFacebook()
        InstagramView.layer.cornerRadius = 10
        FacebookView.layer.cornerRadius = 10
  
/////////////////////////////////////////////////////////////////////// ENDS ////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
/////////////////////////////////////////////// LOCALIZATION STRINGS STARTS /////////////////////////////////////////////
        
        self.LoginInstaLbl.text =  NSLocalizedString("LOGIN_INSTA", comment: "The insta title")
        self.FacebookLbl.text =  NSLocalizedString("LOGIN_FACEBOOK", comment: "The fb title")
        self.OrLbl.text =  NSLocalizedString("OTHER_OPTION", comment: "The or title")
        
/////////////////////////////////////////////////////////////////// ENDS ////////////////////////////////////////////////////////////////////////
        
    }
    
    
    

    
    
    //MARK:- INSTAGRAM LOGIN METHOD
    @IBAction func InstagramBtnAction(sender: AnyObject)
    {
       // print("instagram login btn")
        let instagramScreen = storyboard?.instantiateViewControllerWithIdentifier("InstagramVC") as! InstagramVC
        self.navigationController?.pushViewController(instagramScreen, animated: true)
    }
    
    
    
    //MARK:- FACEBOOK LOGIN METHOD
    @IBAction func FacebookBtnAction(sender: AnyObject)
    {
       // print("facebook login btn")
        
///////////////////////////////////////////// INITIALIZE FBSDKLOGIN MANAGER //////////////////////////////////////////
        
      //  self.configureFacebook()
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
////////////////////////////////////////////// READ PERMISSIONS FROM FB LOGIN ////////////////////////////////////////

        loginManager.logInWithReadPermissions(["email"]) { (result, error) -> Void in

            if (error != nil)
            {
                print("error")
            }
            else if (result.isCancelled)
            {
                NSLog("Cancelled");
            }
            else
            {
                NSLog("Logged in");
                FacebookLoginBool = true
                self.getFaceBookDetails()
            }
            
        }
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////

        
    }
    
    
    
     //MARK:- GET FACEBOOK DETAILS
    func getFaceBookDetails()
    {
        
/////////////////////////////////////// GETTING FB CURRENT ACCESS TOKEN ///////////////////////////////////////////
                    if ((FBSDKAccessToken.currentAccessToken()) != nil)
                    {
                        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                            
                            if (error == nil)
                            {
                                print("result = \(result)")
                                
                                self.facebookDetailsArray = result as! NSMutableDictionary
                             //   print("facebookDetailsArray = \(self.facebookDetailsArray)")
                                
                                self.emailFromFacebook = self.facebookDetailsArray.valueForKey("email") as! String
                               //  print(" self.emailFromFacebook = \( self.emailFromFacebook)")
                                
                                  self.FirstNameFacebook = self.facebookDetailsArray.valueForKey("first_name") as! String
                               //  print(" self.FirstNameFacebook = \( self.FirstNameFacebook)")
                                
                                 self.LastNameFacebook = self.facebookDetailsArray.valueForKey("last_name") as! String
                                // print(" self.LastNameFacebook = \( self.LastNameFacebook)")
                               
                                 self.FullNameFacebook = self.facebookDetailsArray.valueForKey("name") as! String
                                // print(" self.FullNameFacebook = \( self.FullNameFacebook)")
                            
                                var pictureDict = NSMutableDictionary()
                                pictureDict = self.facebookDetailsArray.valueForKey("picture") as! NSMutableDictionary
                                  //print(" pictureDict = \(pictureDict)")
                                
                                var DataDict = NSMutableDictionary()
                                DataDict = pictureDict.valueForKey("data") as! NSMutableDictionary
                             //   print(" DataDict = \(DataDict)")
                                
                                self.ImageURLFaceBook = DataDict.valueForKey("url") as! String
                              //  print(" self.ImageURLFaceBook = \(self.ImageURLFaceBook)")
                                
                                self.FaceBookID = self.facebookDetailsArray.valueForKey("id") as! String
                               // print(" self.FaceBookID = \(self.FaceBookID)")
                                
                                
//////////////////////////////////////////////////////////////////// ENDS ///////////////////////////////////////////////////////////////////////
                                
                                
                                
                                
                                
                                
                                
/////////////////////////////////////////////// USING NSUSERDEFAULTS //////////////////////////////////////////////////////////
                                
                                let defaults = NSUserDefaults.standardUserDefaults()
                                defaults.setObject(self.FirstNameFacebook, forKey: "user_Firstname")
                                defaults.setObject(self.ImageURLFaceBook, forKey: "userImage")
                                defaults.synchronize()
                                
                                 NSUserDefaults.standardUserDefaults().setValue("FacebookUser", forKeyPath: "InstagramUser")
                                
                                
/////////////////////////////////////////////////////////////////// ENDS ////////////////////////////////////////////////////////////////////////
                                
                                
                                
                                
                                
                                
/////////////////////////////////////////////////////////// HIT LOGIN API /////////////////////////////////////////////////////////////////
                                
                               self.LoginAPI()
                                
/////////////////////////////////////////////////////////////////// ENDS ////////////////////////////////////////////////////////////////////////
                                
                            }
                            else
                            {
                                print("error = \(error)")
                                var alert = UIAlertView(title: "", message: "Login failed", delegate: self, cancelButtonTitle: "Ok")
                                alert.show()
                            }
                            
                        })
                    }

    }
    
    
    
    
         //MARK:- FACEBOOK LOGIN DELEGATES
        func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!)
        {
            FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields":"first_name, last_name, picture.type(large)"]).startWithCompletionHandler { (connection, result, error) -> Void in
               // print("values are = \(result)")
    
                if (FBSDKAccessToken.currentAccessToken() != nil)
                {
                    print("fb login");
                }
                else
                {
                    print("fb logout");
                }
        }
            
    }

    
    
    
    
    
    //MARK:- LoginAPI
    func LoginAPI()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        var post = NSString(format:"facebook_id=%@&user_name=%@&user_surname=%@&user_mail=%@&profile_image_url=%@",self.FaceBookID,self.FullNameFacebook,self.LastNameFacebook,self.emailFromFacebook,self.ImageURLFaceBook)
        
      print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        var postLength = String(dataModel.length)
        var url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/facebooklogin")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
                
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()

                        
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                        self.JsonResponseArray = jsonResult as! NSMutableDictionary
                      //  print("self.JsonResponseArray = \(self.JsonResponseArray)")
                   
                    var status = String()
                    status = self.JsonResponseArray.valueForKey("status")! as! String
                    
                    if status == "success" ||  status == "Already Exists"
                    {
                        var dataDict = NSMutableDictionary()
                        dataDict = self.JsonResponseArray.valueForKey("data")! as! NSMutableDictionary
                        
//                        print("userID = \(dataDict.valueForKey("user_id")!)")
//                        print("userName = \(dataDict.valueForKey("user_name")!)")
//                        print("userEmail = \(dataDict.valueForKey("user_mail")!)")
//                        print("userImage = \(dataDict.valueForKey("profile_image_url")!)")
                        
                        let defaults = NSUserDefaults.standardUserDefaults()
                        defaults.setObject(dataDict.valueForKey("user_id")!, forKey: "userID")
                        defaults.setObject(dataDict.valueForKey("user_name")!, forKey: "userName")
                        defaults.setObject(dataDict.valueForKey("user_mail")!, forKey: "userMail")
                        //    defaults.setObject(self.JsonResponseArray.valueForKey("profile_image_url")!, forKey: "userImage")
                        defaults.setObject(dataDict.valueForKey("user_surname")!, forKey: "user_Surname")
                        
                         defaults.setObject(dataDict.valueForKey("first_name")!, forKey: "first_name")
                         defaults.setObject(dataDict.valueForKey("last_name")!, forKey: "last_name")
                        
                        defaults.synchronize()
                        
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            FacebookLoginBool = true
                            
                              NSUserDefaults.standardUserDefaults().setValue("FacebookUser", forKeyPath: "InstagramUser")
                            
                            let tutorialsScreen = self.storyboard?.instantiateViewControllerWithIdentifier("ProfileScreenViewController") as! ProfileScreenViewController
                            self.navigationController?.pushViewController(tutorialsScreen, animated: true)
                            
                            spinningIndicator.hide(true)
                        })

                    }
                    else
                    {
                        
                    }
                    
            
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    
//    func configureFacebook()
//    {
//        btnFacebook.readPermissions = ["public_profile", "email", "user_friends"];
//        btnFacebook.delegate = self
//    }
//      
//
//    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!)
//    {
//        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
//        loginManager.logOut()
//        
//    }
   
    
    //MARK:- DIDRECEIVEMEMORYWARNING METHOD
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

