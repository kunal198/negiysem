//
//  ProfileScreenViewController.swift
//  What To Wear
//
//  Created by brst on 8/20/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class ProfileScreenViewController: UIViewController,UITextFieldDelegate,UIAlertViewDelegate
{

    
     //MARK:- VARIABLES DECLARATION
    var alertValue = UIAlertView()
    var updateResultDict = NSMutableDictionary()
    var dataModel = NSData()
    var savedFirstname = String()
    var savedLastName = String()
    var savedUserID = String()
    var savedEmail = String()
    var savedName = String()
    var savedImage = String()
    var user_interested_in_male = Bool()
    var user_interested_in_female = Bool()
    var blankTextFieldAlert = UIAlertView()
    var femaleCheck = Bool()
    var maleCheck = Bool()
    var gender_type = String()
    
    
    
     //MARK:- OUTLETS DECLARATION
    @IBOutlet weak var NextBtnTitleLbl: UILabel!
    @IBOutlet weak var InterestedINFeamleLbl: UILabel!
    @IBOutlet weak var InterestedINMaleLbl: UILabel!
    @IBOutlet weak var InterestedInLbl: UILabel!
    @IBOutlet weak var FemaleLbl: UILabel!
    @IBOutlet weak var MaleLbl: UILabel!
    @IBOutlet weak var GenderLbl: UILabel!
    @IBOutlet var HeadingView: UIView!
    @IBOutlet var mailTextField: UITextField!
    @IBOutlet var genderView: UIView!
    @IBOutlet var MailView: UIView!
    @IBOutlet var MaleSwitch: UISwitch!
    @IBOutlet var FemaleSwitch: UISwitch!
    @IBOutlet var MaleBtn: UIButton!
    @IBOutlet var FemaleBtn: UIButton!
    @IBOutlet var NextBtn: UIButton!
    @IBOutlet var NextBtnView: UIView!
    @IBOutlet var InterestedInView: UIView!
    @IBOutlet var FemaleImageView: UIImageView!
    @IBOutlet var MaleImageView: UIImageView!
    
    
    
     //MARK:- VIEW-DID-LOAD METHOD
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
////////////////////////////////////////// GoogleAnalyticsRegisterView STARTS ////////////////////////////////////////////////        
        
        GoogleAnalyticsRegisterView()
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
////////////////////////////////////////////////// TEXTFIELD DELEGATE ///////////////////////////////////////////////////////////
        
        self.blankTextFieldAlert.delegate = self
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
////////////////////////////////////////////////// STATUS BAR & NAVBAR //////////////////////////////////////////////////////////
        
        UIApplication.sharedApplication().statusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
//////////////////////////////////////////////// CORNER RADIUS UIBUTTON ////////////////////////////////////////////////////
        
        self.NextBtn.layer.borderWidth = 2
        self.NextBtn.layer.borderColor = UIColor(red: 237.0/255, green: 37.0/255, blue: 78.0/255, alpha: 1.0).CGColor
        self.NextBtn.layer.masksToBounds = true
        self.NextBtn.layer.cornerRadius = 20
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
/////////////////////////////////////////////////// SETTING BOOL VALUE ///////////////////////////////////////////////////////////
        
        maleCheck = false
        femaleCheck = false
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
    }

    
    
     //MARK:- VIEW-WILL-APPEAR
    override func viewWillAppear(animated: Bool)
    {
        
        
/////////////////////////////////////////////// NSUSERDEFAULTS VALUES ///////////////////////////////////////////////////////        
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        savedUserID = userDefaults.valueForKey("userID") as! String
        savedName = userDefaults.valueForKey("userName") as! String
        savedEmail = userDefaults.valueForKey("userMail") as! String
        savedImage = userDefaults.valueForKey("userImage") as! String
        savedFirstname =  userDefaults.valueForKey("userName") as! String
      //  savedLastName =  userDefaults.valueForKey("user_Firstname") as! String
        
        
        print("instaAccessToken = \(instaAccessToken)")
        print("savedUserID = \(savedUserID) ")
        print("savedName = \(savedName) ")
        print("savedEmail = \(savedEmail) ")
        print("savedImage = \(savedImage) ")
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
/////////////////////////////////////////////// PLACEHOLDER TEXTFIELD ///////////////////////////////////////////////////////
        
        self.mailTextField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("EMAIL_FIELD", comment: "The email title"), attributes: [NSForegroundColorAttributeName: UIColor.lightGrayColor(),NSFontAttributeName :UIFont(name: "Helvetica Neue", size: 15)!])
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        
        
        
//////////////////////////////////////////// SETTING LOCALIZED STRINGS ////////////////////////////////////////////////////
        
        self.InterestedInLbl.text =  NSLocalizedString("INSTERESTED_IN_TYPE", comment: "The interested in title")
        self.InterestedINMaleLbl.text =  NSLocalizedString("GENDER_TYPE_MALE", comment: "The gender male title")
        self.InterestedINFeamleLbl.text =  NSLocalizedString("GENDER_TYPE_FEMALE", comment: "The gender female title")
        self.GenderLbl.text =  NSLocalizedString("YOUR_GENDER", comment: "The gender title")
        self.MaleLbl.text =  NSLocalizedString("GENDER_TYPE_MALE", comment: "The gender male title")
        self.FemaleLbl.text =  NSLocalizedString("GENDER_TYPE_FEMALE", comment: "The gender female title")
        self.NextBtnTitleLbl.text =  NSLocalizedString("NEXT_BTN_LBL", comment: "The next title")
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        

    }
    
    
     //MARK:- GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "AddEmailView")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    
    
    //MARK:- MALE SWITCH METHOD
    @IBAction func MaleSwitchAction(sender: AnyObject)
    {
        // print("male switch btn action")
        
        
        if MaleSwitch.on
        {
         //   print("MaleSwitch is on")
            //self.NameLbl.text = savedName
            
            user_interested_in_male = true
         //   print("user_interested_in_male = \(user_interested_in_male)")
            
            MaleSwitch.thumbTintColor = UIColor(red: 236.0/255, green: 42.0/255, blue: 76.0/255, alpha: 1.0)
            MaleSwitch.onTintColor = UIColor(red: 247.0/255, green: 170.0/255, blue: 183.0/255, alpha: 1.0)
            //  ShowNameSwitch.setOn(false, animated:true)
        }
        else
        {
           // print("MaleSwitch is off")
            
            user_interested_in_male = false
          //  print("user_interested_in_male = \(user_interested_in_male)")
            
            //self.NameLbl.text = savedFirstname
            //ShowNameSwitch.setOn(true, animated:true)
            MaleSwitch.thumbTintColor = UIColor(red: 230.0/255, green: 231.0/255, blue: 232.0/255, alpha: 1.0)
            MaleSwitch.onTintColor = UIColor.darkGrayColor()//(red: 166.0/255, green: 168.0/255, blue: 171.0/255, alpha: 1.0)
        }
    }
    
    
    //MARK:- FEMALE SWITCH METHOD
    @IBAction func FemaleSwitchAction(sender: AnyObject)
    {
       //  print("female switch btn action")
        
        if FemaleSwitch.on
        {
            //print("FemaleSwitch is on")
            user_interested_in_female = true
            //print("user_interested_in_female = \(user_interested_in_female)")
            //self.NameLbl.text = savedName
            FemaleSwitch.thumbTintColor = UIColor(red: 236.0/255, green: 42.0/255, blue: 76.0/255, alpha: 1.0)
            FemaleSwitch.onTintColor = UIColor(red: 247.0/255, green: 170.0/255, blue: 183.0/255, alpha: 1.0)
            //  ShowNameSwitch.setOn(false, animated:true)
        }
        else
        {
           // print("FemaleSwitch is off")
            user_interested_in_female = false
          //  print("user_interested_in_female = \(user_interested_in_female)")
            // self.NameLbl.text = savedFirstname
            //ShowNameSwitch.setOn(true, animated:true)
            FemaleSwitch.thumbTintColor = UIColor(red: 230.0/255, green: 231.0/255, blue: 232.0/255, alpha: 1.0)
            FemaleSwitch.onTintColor = UIColor.darkGrayColor()//(red: 166.0/255, green: 168.0/255, blue: 171.0/255, alpha: 1.0)
        }
        
      //  ProfileUpdateAPI()
        
    }
    
    
    //MARK:- FEMALE SELECTION METHOD
    @IBAction func FemaleBtnAction(sender: AnyObject)
    {
       //  print("female btn action")
            
            if femaleCheck == false
            {
                
                FemaleImageView.image = UIImage(named: "Radio check")
                femaleCheck = true
                
                gender_type = "female"
                
               // print("femaleCheck = \(femaleCheck)")
                
                MaleImageView.image = UIImage(named: "Radio Uncheck")
               maleCheck = false
                
              //  print("gender_type = \(gender_type)")
            }
            else
            {
                FemaleImageView.image = UIImage(named: "Radio Uncheck")
                femaleCheck = false
                
                gender_type = ""
                
              //  print("femaleCheck = \(femaleCheck)")
                
                MaleImageView.image = UIImage(named: "Radio check")
                maleCheck = true
                
               // print("gender_type = \(gender_type)")
            }
      
    }
    
    
    //MARK:- MALE SELECTION METHOD
    @IBAction func MaleBtnAction(sender: AnyObject)
    {
        // print("male btn action")
            
            if maleCheck == false
            {
                MaleImageView.image = UIImage(named: "Radio check")
                maleCheck = true
               // print("maleCheck = \(maleCheck)")
                
                gender_type = "male"
              //  print("gender_type = \(gender_type)")
                
                FemaleImageView.image = UIImage(named: "Radio Uncheck")
                femaleCheck = false
            }
            else
            {
                MaleImageView.image = UIImage(named: "Radio Uncheck")
                maleCheck = false
               // print("maleCheck = \(maleCheck)")
                
                gender_type = ""
                //print("gender_type = \(gender_type)")
                
                FemaleImageView.image = UIImage(named: "Radio check")
                femaleCheck = true
            }
     
    }
    
    
    //MARK:- NEXT BTN METHOD
    @IBAction func NextBtnAction(sender: AnyObject)
    {
        //print("next btn action")
        if self.mailTextField.text == ""
        {
            blankTextFieldAlert = UIAlertView(title: "", message: NSLocalizedString("ALERT_EMAIL", comment: "The mail field title"), delegate: self, cancelButtonTitle:  NSLocalizedString("ALERT_OK", comment: "The ok title"))
            blankTextFieldAlert.show()
        }
        if gender_type == ""
        {
            blankTextFieldAlert = UIAlertView(title: "", message: NSLocalizedString("ALERT_GENDER", comment: "The gender field title"), delegate: self, cancelButtonTitle:  NSLocalizedString("ALERT_OK", comment: "The ok title"))
            blankTextFieldAlert.show()
        }
        else if !isValidEmail(self.mailTextField.text!) //&& self.mailTextField.text != ""
        {
            let alert1 = UIAlertView()
            alert1.title = ""
            alert1.message = NSLocalizedString("ALERT_VALID_MAIL", comment: "The valid mail title")
            alert1.addButtonWithTitle( NSLocalizedString("ALERT_OK", comment: "The ok title"))
            alert1.show()
        }
        else
        {
             //print("text field not blank")
            ProfileUpdateAPI()
        }
    }
    
    
    //MARK:- EMAIL VALIDATION
    func isValidEmail(testStr:String) -> Bool
    {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    
    
    
    
    //MARK:- ProfileUpdateAPI
    func ProfileUpdateAPI()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        var post = NSString(format:"user_id=%@&user_name=%@&user_surname=%@&user_sex=%@&user_interested_in_male=%@&user_interested_in_female=%@&user_mail=%@&user_website=%@",savedUserID,savedName,savedLastName,gender_type,user_interested_in_male,user_interested_in_female,self.mailTextField.text!,"")
        
        print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        var postLength = String(dataModel.length)
        var url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/updateuser")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
                
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()

                        
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                    
                    self.updateResultDict = jsonResult as! NSMutableDictionary
                  print("self.updateResultDict = \(self.updateResultDict)")
                    
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var status = String()
                        status = self.updateResultDict.valueForKey("status") as! String
                        
                        
                        let defaults = NSUserDefaults.standardUserDefaults()
                        defaults.setObject(self.updateResultDict.valueForKey("user_id")!, forKey: "userID")
                        defaults.setObject(self.updateResultDict.valueForKey("user_name")!, forKey: "userName")
                        defaults.setObject(self.updateResultDict.valueForKey("user_mail")!, forKey: "userMail")
                        //    defaults.setObject(self.JsonResponseArray.valueForKey("profile_image_url")!, forKey: "userImage")
                        defaults.setObject(self.updateResultDict.valueForKey("user_sex")!, forKey: "user_sex")
                        defaults.setObject(self.updateResultDict.valueForKey("user_surname")!, forKey: "user_Surname")
                        defaults.setObject(self.updateResultDict.valueForKey("user_interested_in_female")!, forKey: "user_interested_in_female")
                        defaults.setObject(self.updateResultDict.valueForKey("user_interested_in_male")!, forKey: "user_interested_in_male")
                        defaults.synchronize()

                        
                        if status == "success"
                        {
                            self.alertValue =  UIAlertView(title: "", message: NSLocalizedString("ALERT_SUCCESS", comment: "The success title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            self.alertValue.tag = 2
                            self.alertValue.show()
                        }
                        else
                        {
                            var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_NOT_SUCCESS", comment: "The not success title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            alert.show()
                        }
                        
                        
                        spinningIndicator.hide(true)
                    })
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }

    
    
     //MARK:- ALERT-VIEW DELEGATE
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        
        
/////////////////////////////////////////////////////// ALERT WITH TAG ////////////////////////////////////////////////////////////////
        
        if  self.alertValue.tag == 2
        {
            let tutorialsScreen = storyboard?.instantiateViewControllerWithIdentifier("TutorialsViewController") as! TutorialsViewController
            self.navigationController?.pushViewController(tutorialsScreen, animated: true)
        }
        
////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
       
    }
    
    
    
    
    
    //MARK:- UITEXTFIELD DELEGATE
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        self.mailTextField.resignFirstResponder()
        return true
    }
    
    
    
    //MARK:- DIDRECEIVEMEMORYWARNING METHOD
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
