//
//  MyBestPostDetailsViewController.swift
//  What To Wear
//
//  Created by brst on 8/23/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit
import SDWebImage

class MyBestPostDetailsViewController: UIViewController
{

    //MARK:- VARIABLES DECLARATION
    var savedName = String()
    var newImage = UIImage(named: "")
    var particularBestPostDetailsDict = NSMutableDictionary()
    
    
    //MARK:- OUTLETS DECLARATION
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var DescriptionTextView: UITextView!
    @IBOutlet var BlackBlurView: UIView!
    @IBOutlet var PersonMainImage: UIImageView!
    @IBOutlet var PersonEmailLbl: UILabel!
    @IBOutlet var PersonNameLbl: UILabel!
    @IBOutlet var profileImagePerson: UIImageView!
    @IBOutlet var WrapperViewInfo: UIView!
    @IBOutlet weak var DaysAgoLbl: UILabel!
    @IBOutlet var WebsiteBtn: UIButton!
    @IBOutlet var InstagramBtn: UIButton!
    @IBOutlet var InstagraImageIcon: UIImageView!
    @IBOutlet weak var DislikeLbl: UILabel!
    @IBOutlet weak var LikeLbl: UILabel!
    
    
    //MARK:- VIEW-DID-LOAD
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
         ////////////////////////////////////////// GOOGLE ANALYTICS REGISTER VIEW ///////////////////////////////////////////
        
        GoogleAnalyticsRegisterView()
        
         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
     //  self.DaysAgoLbl.text =  "6 " + NSLocalizedString("DAYS_AGO", comment: "The days ago title")

    }

    
   
    
    func FetchDetailsForParticularPost()
    {
//        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
//        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
//        spinningIndicator.hide(false, afterDelay: 1)
        
        
         ///////////////////////////////////////////////// SET CORNER RADIUS OF IMAGE ///////////////////////////////////////////////
        
        self.WrapperViewInfo.layer.cornerRadius = 10
        self.PersonMainImage.layer.cornerRadius = 10
        
         ////////////////////////////////////////////////////////////////////// ENDS /////////////////////////////////////////////////////////////////////
        
        
        
        
        let likeStr:AnyObject = self.particularBestPostDetailsDict.valueForKey("likes") as! String
        let dislikeStr:AnyObject = self.particularBestPostDetailsDict.valueForKey("unlike") as! String
        
        //var like:AnyObject = self.VotesDict.valueForKey("likes")!
        
        if  likeStr is NSNull
        {
            //print("self.VotesDict is null")
            self.LikeLbl.text = "0"
        }
        else
        {
            //print("\(self.VotesDict.valueForKey("likes") as! String)")
            self.LikeLbl.text = likeStr as? String
        }
        
        
        if  dislikeStr is NSNull
        {
            //print("self.VotesDict is null")
            self.DislikeLbl.text = "0"
        }
        else
        {
            //print("\(self.VotesDict.valueForKey("likes") as! String)")
            self.DislikeLbl.text = dislikeStr as? String
        }
        
        
        var postDict = self.particularBestPostDetailsDict.valueForKey("post")  as! NSDictionary
        print("postDict=\(postDict)")
        
        var post_time = String()
        post_time = postDict.valueForKey("post_time") as! String
        
        self.DescriptionTextView.text = postDict.valueForKey("photo_description") as! String
        
        if post_time != ""
        {
            
            let strtemp = post_time
            // print("strtemp is \(strtemp)")
            
            let str = timedifference(strtemp)
            
            self.DaysAgoLbl.text = "  \(str) "
            
        }
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            var profileUrl: String = postDict.valueForKey("img_para") as! String
            var url: NSURL = NSURL(string: profileUrl)!
            dispatch_async(dispatch_get_main_queue(), {
                
                    if let imageURL = NSURL(string: profileUrl)
                    {
                        if let data = NSData(contentsOfURL: imageURL)
                        {
                            //cell.MyPostImage.image = UIImage(data: data)
                            let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                            }
                            // cell.MyPostImage.sd_setImageWithURL(url, completed: block)
                            self.PersonMainImage.sd_setImageWithURL(url, placeholderImage:  UIImage(named: "NoImageDefault"))
                            
                            self.backgroundImage.sd_setImageWithURL(url, placeholderImage:  UIImage(named: "NoImageDefault"))
                            
                        }
                    }
                
            })
            
            
        });
        


    }
    
    
    func timedifference(dateString: String)->String
    {
        let date = NSDate()
        
        //let messageTimezoneOffset = resultSet.intForColumn("timezoneOffset")
        
        
        /// GET TIMESTAMP IN UNIX
        let currentTimeStamp = Int64(date.timeIntervalSince1970*1000)
        
        print("current date = \(NSDate().timeIntervalSince1970 * 1000)")
        
        print("currentTimeStamp=\(currentTimeStamp)")
        // let timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
        
        let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
        print("timeDiff=\(timeDiff)")
        
        let nYears = timeDiff / (1000*60*60*24*365)
        
        
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        
        
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        
        
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        //println(nMinutes)
        
        var timeMsg = ""
        
        if nYears > 0
        {
            var yearWord = "years"
            if nYears == 1
            {
                yearWord = "year"
            }
            
            timeMsg = " \(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
            var monthWord = "months"
            if nMonths == 1
            {
                monthWord = "month"
            }
            
            timeMsg = " \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            timeMsg = " \(nDays) \(dayWord) ago"
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = " \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            
            timeMsg = " \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        return timeMsg
    }

    
    
    
    
    
    //MARK:- GOOGLE ANALYTICS REGISTER VIEW
    func GoogleAnalyticsRegisterView()
    {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "MyBestPostDetailsView")
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }

    
    //MARK:- VIEW-WILL-APPEAR
    override func viewWillAppear(animated: Bool)
    {
        UIApplication.sharedApplication().statusBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
        profileImagePerson.layer.borderWidth=1.0
        profileImagePerson.layer.masksToBounds = false
        profileImagePerson.layer.borderColor = UIColor.whiteColor().CGColor
        profileImagePerson.layer.cornerRadius = 13
        profileImagePerson.layer.cornerRadius = profileImagePerson.frame.size.height/2
        profileImagePerson.clipsToBounds = true
        
        
/////////////////////////////////////////////////////////// NSUserDefaults Strings /////////////////////////////////////////////////////////////////
        
        
        if Reachability.isConnectedToNetwork() == false
        {
            var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
            alert.show()
            
            self.LikeLbl.text = ""
            self.DislikeLbl.text = ""
            self.DaysAgoLbl.text = ""
            self.DescriptionTextView.text = ""
            self.PersonEmailLbl.text = ""
            self.PersonNameLbl.text = ""
            
        }
        else
        {
            var userSaved = String()
            if let userwebsite = NSUserDefaults.standardUserDefaults().valueForKey("user_website")
            {
                if userwebsite as! String == ""
                {
                    self.WebsiteBtn.userInteractionEnabled = false
                    self.PersonEmailLbl.hidden = true
                }
                else
                {
                    self.PersonEmailLbl.text = userwebsite as! String
                }
                
                
            }
            
            if let username = NSUserDefaults.standardUserDefaults().valueForKey("userName")
            {
                savedName = username as! String
                self.PersonNameLbl.text = savedName
            }
            
            //  print("savedName = \(savedName) ")
            var savedImage = NSUserDefaults.standardUserDefaults().valueForKey("userImage") as! String
            var imageURL: NSURL = NSURL(string: savedImage)!
            
            if let data : NSData = NSData(contentsOfURL: imageURL)
            {
                self.profileImagePerson.image = UIImage(data: data)!
            }
            
            
            
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            
            
            
            var userSavedtype = String()
            if let userLoggedIn = NSUserDefaults.standardUserDefaults().valueForKey("InstagramUser")
            {
                userSavedtype = userLoggedIn as! String
            }
            
            //   print("userSaved = \(userSavedtype)")
            
            if userSavedtype != ""
            {
                if userSavedtype == "InstagramUser"
                {
                    self.InstagramBtn.hidden = false
                    self.InstagraImageIcon.hidden = false
                }
                else
                {
                    self.InstagraImageIcon.hidden = true
                    self.InstagramBtn.hidden = true
                }
                
            }
            
            print("self.particularBestPostDetailsDict=\(self.particularBestPostDetailsDict)")
            FetchDetailsForParticularPost()

        }

        
    

    }
    
    
    
    //MARK:- INSTAGRAM BTN ACTION
    @IBAction func InstagramBtnAction(sender: AnyObject)
    {
        //print("instagram btn action")
        openInstagram()
    }
    
    
    //MARK:- OPEN INSTAGRAM IN WEBVIEW METHOD
    func openInstagram()
    {
        var instURL: NSURL = NSURL (string: "instagram://user?username=Instagram")! // Replace = Instagram by the your instagram user name
        var instWB: NSURL = NSURL (string: "https://instagram.com/instagram/")! // Replace the link by your instagram weblink self.PersonEmailLbl.text
        
        if (UIApplication.sharedApplication().canOpenURL(instURL))
        {
            // Open Instagram application
            UIApplication.sharedApplication().openURL(instURL)
        }
        else
        {
            // Open in Safari
            UIApplication.sharedApplication().openURL(instWB)
        }
    }
    
    
    //MARK:- WEBSITE BTN ACTION
    @IBAction func WebsiteBtnAction(sender: AnyObject)
    {
        //print("website btn action")
        // UIApplication.sharedApplication().openURL(NSURL(string:"http://www.facebook.com")!)
        
       // print("website link = \("http://" + self.PersonEmailLbl.text!)")
        
        UIApplication.sharedApplication().openURL(NSURL(string: "http://" + self.PersonEmailLbl.text!)!)
    }
    
    
    //MARK:- CANCEL BTN ACTION
    @IBAction func CancelBtnAction(sender: AnyObject)
    {
        //print("Cancel btn")
        self.navigationController?.popViewControllerAnimated(false)
//        for controller in self.navigationController!.viewControllers as Array
//        {
//            if controller.isKindOfClass(MyBestPostsViewController)
//            {
//                self.navigationController?.popToViewController(controller as! UIViewController, animated: false)
//                break
//            }
//        }
        

//        let votingScreen = storyboard?.instantiateViewControllerWithIdentifier("VotingViewController") as! VotingViewController
//        votingScreen.newImage = PersonMainImage.image
//        self.navigationController?.pushViewController(votingScreen, animated: true)
    }

    
    //MARK:- DID-RECEIVE-MEMORY-WARNING
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
