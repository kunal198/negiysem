//
//  InstagramVC.swift
//  ZumeSpot
//
//  Created by mrinal khullar on 9/1/16.
//  Copyright © 2016 mrinal khullar. All rights reserved.
//

import UIKit

class InstagramVC: UIViewController, UIWebViewDelegate{

    
    var JsonResponseDict = NSMutableDictionary()
    var dataModel = NSData()
    var instaUserBioDict = NSMutableDictionary()
    var fullNameInstagram = String()
    var instagramUserID = String()
    var instaUserProfilePic = String()
    var instaUserName = String()
    var instaUserWebSite = String()
    
    
     var namearray=NSMutableArray()
     var spinningIndicator = MBProgressHUD()
     let appdele=AppDelegate()
    
    @IBOutlet weak var web_view: UIWebView!
    
 //   @IBOutlet weak var activityindi: UIActivityIndicatorView!
    
    var kBaseURL = "https://instagram.com/"
    var kAuthenticationURL="oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=token&scope=likes+comments+basic"
    var kClientID = "bf7c3d28453a411e85e1458413f2d80f"
    var kRedirectURI="http://www.brihaspatiinfotech.com"
    var kAccessToken="access_token"
    var typeOfAuthentication = ""

    var sorteddistance=[AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
     }
    
     override func viewWillAppear(animated: Bool)
     {
        UIApplication.sharedApplication().statusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
        var authURL: String? = nil
        
        if (typeOfAuthentication == "UNSIGNED")
        {
            authURL = "\(appdele.INSTAGRAM_AUTHURL)?client_id=\(appdele.INSTAGRAM_CLIENT_ID)&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&response_type=token&scope=\(appdele.INSTAGRAM_SCOPE)&DEBUG=True"
        }
        else
        {
            authURL = "\(appdele.INSTAGRAM_AUTHURL)?client_id=\(appdele.INSTAGRAM_CLIENT_ID)&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&response_type=code&scope=\(appdele.INSTAGRAM_SCOPE)&DEBUG=True"
        }
        web_view.loadRequest(NSURLRequest(URL: NSURL(string: authURL!)!))
        web_view.delegate = self
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
//        let urlString = request.URL!.absoluteString
//        let Url = request.URL!
//        let UrlParts = Url.pathComponents!
//        if UrlParts.count == 1 {
//            let tokenParam = (urlString as NSString).rangeOfString(kAccessToken)
//            if tokenParam.location != NSNotFound {
//                var token = urlString.substringFromIndex(urlString.startIndex.advancedBy(NSMaxRange(tokenParam)))
//                // If there are more args, don't include them in the token:
//                let endRange = (token as NSString).rangeOfString("&")
//                if endRange.location != NSNotFound {
//                    token = token.substringToIndex(token.startIndex.advancedBy(endRange.location))
//                }
//                if token.characters.count > 0 {
//                    // call the method to fetch the user's Instagram info using access token
//                   // gAppData.getUserInstagramWithAccessToken(token)
//                }
//            }
//            else {
//                print("rejected case, user denied request")
//            }
//            return false
//        }
        return self.checkRequestForCallbackURL(request)

        
    }
    
    func webViewDidStartLoad(webView: UIWebView)
    {
        spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        web_view.layer.removeAllAnimations()
        web_view.userInteractionEnabled = false
        UIView.animateWithDuration(0.1, animations: {() -> Void in
            //  loginWebView.alpha = 0.2;
        })
    }
    
    func webViewDidFinishLoad(webView: UIWebView)
    {
        dispatch_async(dispatch_get_main_queue(), {
            self.spinningIndicator.hide(true)
        })
        
        web_view.layer.removeAllAnimations()
        web_view.userInteractionEnabled = true
        UIView.animateWithDuration(0.1, animations: {() -> Void in
            //loginWebView.alpha = 1.0;
        })
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?)
    {
//        print("Code : %d \nError : %@", error!.code, error!.description)
//        //Error : Error Domain=WebKitErrorDomain Code=102 "Frame load interrupted"
//        if error!.code == 102 {
//            return
//        }
//        if error!.code == -1009 || error!.code == -1005 {
//            //        _completion(kNetworkFail,kPleaseCheckYourInternetConnection);
//        }
//        else {
//            //        _completion(kError,error.description);
//        }
//      //  UIUtils.networkFailureMessage()
        self.webViewDidFinishLoad(webView)

    }
    
    
    func checkRequestForCallbackURL(request: NSURLRequest) -> Bool
    {
        let urlString = request.URL!.absoluteString
        if (typeOfAuthentication == "UNSIGNED")
        {
            // check, if auth was succesfull (check for redirect URL)
            if urlString.hasPrefix(appdele.INSTAGRAM_REDIRECT_URI)
            {
                // extract and handle access token
                let range = (urlString as NSString).rangeOfString("#access_token=")
                self.handleAuth(urlString.substringFromIndex(urlString.startIndex.advancedBy(range.location + range.length)))
                return false
            }
        }
        else
        {
            if urlString.hasPrefix(appdele.INSTAGRAM_REDIRECT_URI)
            {
                // extract and handle access token
                let range = (urlString as NSString).rangeOfString("code=")
                self.makePostRequest(urlString.substringFromIndex(urlString.startIndex.advancedBy(range.location + range.length)))
                return false
            }
        }
        return true
    }
    
    func makePostRequest(code: String)
    {
        
        let post = "client_id=\(appdele.INSTAGRAM_CLIENT_ID)&client_secret=\(appdele.INSTAGRAM_CLIENTSERCRET)&grant_type=authorization_code&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&code=\(code)"
        let postData = post.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: true)!
        let postLength = "\(UInt(postData.length))"
        let requestData = NSMutableURLRequest(URL: NSURL(string: "https://api.instagram.com/oauth/access_token")!)
        requestData.HTTPMethod = "POST"
        requestData.setValue(postLength, forHTTPHeaderField: "Content-Length")
        requestData.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        requestData.HTTPBody = postData
        
       // var response: NSURLResponse? = nil
        //var responseData = try! NSURLConnection.sendSynchronousRequest(requestData, returningResponse: response)!
        var urldata=NSData()
        var requestError: NSError? = nil
        let response : AutoreleasingUnsafeMutablePointer<NSURLResponse?> = nil
        do{
            let urlData = try NSURLConnection.sendSynchronousRequest(requestData, returningResponse: response)
            urldata=urlData
            print(urlData)
        }
        catch (let e) {
            print(e)
        }
        let dict = try! NSJSONSerialization.JSONObjectWithData(urldata, options: NSJSONReadingOptions.AllowFragments)
        
      //  print("response from instagram = \(dict)")
        
        instaUserBioDict = dict.valueForKey("user") as! NSMutableDictionary
        print("instaUserBioDict = \(instaUserBioDict)")
        
        fullNameInstagram = instaUserBioDict.valueForKey("full_name") as! String
        instagramUserID = instaUserBioDict.valueForKey("id") as! String
        instaUserProfilePic = instaUserBioDict.valueForKey("profile_picture") as! String
        instaUserName = instaUserBioDict.valueForKey("username") as! String
        instaUserWebSite = instaUserBioDict.valueForKey("website") as! String
        
//         print("fullNameInstagram = \(fullNameInstagram)")
//         print("instagramUserID = \(instagramUserID)")
//         print("instaUserProfilePic = \(instaUserProfilePic)")
//         print("instaUserName = \(instaUserName)")
//         print("instaUserWebSite = \(instaUserWebSite)")
   
        let defaults = NSUserDefaults.standardUserDefaults()
        //defaults.setObject(self.FirstNameFacebook, forKey: "user_Firstname")
        defaults.setObject(self.instaUserProfilePic, forKey: "userImage")
        defaults.synchronize()
        
        
        instaAccessToken = dict.valueForKey("access_token") as! String
        
        self.handleAuth((dict.valueForKey("access_token") as! String))
        
//        let defaultsUser = NSUserDefaults.standardUserDefaults()
//        defaultsUser.setObject((dict.valueForKey("access_token") as! String), forKey: "access_token")
//        defaultsUser.synchronize()
        
        NSUserDefaults.standardUserDefaults().setValue((dict.valueForKey("access_token") as! String), forKeyPath: "access_token")

        NSUserDefaults.standardUserDefaults().setValue(self.instaUserBioDict.valueForKey("website") as! String, forKeyPath: "user_website")
        

        
    }
    
    func handleAuth(authToken: String)
    {
        //print("successfully logged in with Token == \(authToken)")
       // self.dismissViewControllerAnimated(true, completion: { _ in })
        LoginWithInstagramAPI()
    }
    
    
    
    
    
    //MARK:- LoginWithInstagramAPI
    func LoginWithInstagramAPI()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
        var post = NSString(format:"instagram_id=%@&user_name=%@&user_surname=%@&profile_image_url=%@&user_website=%@",self.instagramUserID,self.instaUserName,self.fullNameInstagram,self.instaUserProfilePic,self.instaUserWebSite)
        
        print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        var postLength = String(dataModel.length)
        var url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/instagramlogin")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
                
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()

                        
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                    self.JsonResponseDict = jsonResult as! NSMutableDictionary
                    print("self.JsonResponseArray = \(self.JsonResponseDict)")
                    
                    var status = String()
                    status = self.JsonResponseDict.valueForKey("status")! as! String
                    
                    if status == "success" ||  status == "Already Exists"
                    {
                        var dataDict = NSMutableDictionary()
                        dataDict = self.JsonResponseDict.valueForKey("data")! as! NSMutableDictionary
                        
                        print("userID = \(dataDict.valueForKey("user_id")!)")
                        print("userName = \(dataDict.valueForKey("user_name")!)")
                        print("userEmail = \(dataDict.valueForKey("user_mail")!)")
                        print("userImage = \(dataDict.valueForKey("profile_image_url")!)")
                        
                        let defaults = NSUserDefaults.standardUserDefaults()
                        defaults.setObject(dataDict.valueForKey("user_id")!, forKey: "userID")
                        defaults.setObject(dataDict.valueForKey("user_name")!, forKey: "userName")
                        defaults.setObject(dataDict.valueForKey("user_mail")!, forKey: "userMail")
                        //    defaults.setObject(self.JsonResponseArray.valueForKey("profile_image_url")!, forKey: "userImage")
                        defaults.setObject(dataDict.valueForKey("user_surname")!, forKey: "user_Surname")
                        defaults.synchronize()
                        
                        
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                        //   [[NSUserDefaults standardUserDefaults] setValue:@"user" forKey:@"cons"];
                            
                         NSUserDefaults.standardUserDefaults().setValue("InstagramUser", forKeyPath: "InstagramUser")
                            
//                            if dataDict.valueForKey("user_mail")! as! String == ""
//                            {
                                let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("ProfileScreenViewController") as! ProfileScreenViewController
                                self.navigationController?.pushViewController(profileScreen, animated: true)
//                            }
//                            else
//                            {
//                                let profileScreen = self.storyboard?.instantiateViewControllerWithIdentifier("TutorialsViewController") as! TutorialsViewController
//                                self.navigationController?.pushViewController(profileScreen, animated: true)
//                            }

                            
                            spinningIndicator.hide(true)
                        })
                        
                    }
                    else
                    {
                        
                    }
                    
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }

}
