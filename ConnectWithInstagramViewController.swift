//
//  ConnectWithInstagramViewController.swift
//  Negiysem
//
//  Created by brst on 9/16/16.
//  Copyright © 2016 brst. All rights reserved.
//

import UIKit

class ConnectWithInstagramViewController: UIViewController,UIWebViewDelegate,UIAlertViewDelegate
{

     var alertConnectTrue = UIAlertView()
    var ConnectResultDict = NSMutableDictionary()
    var JsonResponseDict = NSMutableDictionary()
    var dataModel = NSData()
    var instaUserBioDict = NSMutableDictionary()
    var fullNameInstagram = String()
    var instagramUserID = String()
    var instaUserProfilePic = String()
    var instaUserName = String()
    var instaUserWebSite = String()
    var namearray=NSMutableArray()
    var spinningIndicator = MBProgressHUD()
    let appdele=AppDelegate()
    
    @IBOutlet weak var WebView: UIWebView!
    
    var kBaseURL = "https://instagram.com/"
    var kAuthenticationURL="oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=token&scope=likes+comments+basic"
    var kClientID = "bf7c3d28453a411e85e1458413f2d80f"
    var kRedirectURI="http://www.brihaspatiinfotech.com"
    var kAccessToken="access_token"
    var typeOfAuthentication = ""
    
    var sorteddistance=[AnyObject]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        alertConnectTrue.delegate = self
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(animated: Bool)
    {
        UIApplication.sharedApplication().statusBarHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBarHidden = true
        
        var authURL: String? = nil
        
        if (typeOfAuthentication == "UNSIGNED")
        {
            authURL = "\(appdele.INSTAGRAM_AUTHURL)?client_id=\(appdele.INSTAGRAM_CLIENT_ID)&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&response_type=token&scope=\(appdele.INSTAGRAM_SCOPE)&DEBUG=True"
        }
        else
        {
            authURL = "\(appdele.INSTAGRAM_AUTHURL)?client_id=\(appdele.INSTAGRAM_CLIENT_ID)&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&response_type=code&scope=\(appdele.INSTAGRAM_SCOPE)&DEBUG=True"
        }
        WebView.loadRequest(NSURLRequest(URL: NSURL(string: authURL!)!))
        WebView.delegate = self

    }
    
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
        //        let urlString = request.URL!.absoluteString
        //        let Url = request.URL!
        //        let UrlParts = Url.pathComponents!
        //        if UrlParts.count == 1 {
        //            let tokenParam = (urlString as NSString).rangeOfString(kAccessToken)
        //            if tokenParam.location != NSNotFound {
        //                var token = urlString.substringFromIndex(urlString.startIndex.advancedBy(NSMaxRange(tokenParam)))
        //                // If there are more args, don't include them in the token:
        //                let endRange = (token as NSString).rangeOfString("&")
        //                if endRange.location != NSNotFound {
        //                    token = token.substringToIndex(token.startIndex.advancedBy(endRange.location))
        //                }
        //                if token.characters.count > 0 {
        //                    // call the method to fetch the user's Instagram info using access token
        //                   // gAppData.getUserInstagramWithAccessToken(token)
        //                }
        //            }
        //            else {
        //                print("rejected case, user denied request")
        //            }
        //            return false
        //        }
        return self.checkRequestForCallbackURL(request)
        
        
    }
    
    func webViewDidStartLoad(webView: UIWebView)
    {
        spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        WebView.layer.removeAllAnimations()
        WebView.userInteractionEnabled = false
        UIView.animateWithDuration(0.1, animations: {() -> Void in
            //  loginWebView.alpha = 0.2;
        })
    }
    
    func webViewDidFinishLoad(webView: UIWebView)
    {
        dispatch_async(dispatch_get_main_queue(), {
            self.spinningIndicator.hide(true)
        })
        
        WebView.layer.removeAllAnimations()
        WebView.userInteractionEnabled = true
        UIView.animateWithDuration(0.1, animations: {() -> Void in
            //loginWebView.alpha = 1.0;
        })
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?)
    {
        //        print("Code : %d \nError : %@", error!.code, error!.description)
        //        //Error : Error Domain=WebKitErrorDomain Code=102 "Frame load interrupted"
        //        if error!.code == 102 {
        //            return
        //        }
        //        if error!.code == -1009 || error!.code == -1005 {
        //            //        _completion(kNetworkFail,kPleaseCheckYourInternetConnection);
        //        }
        //        else {
        //            //        _completion(kError,error.description);
        //        }
        //      //  UIUtils.networkFailureMessage()
        self.webViewDidFinishLoad(webView)
        
    }
    
    
    func checkRequestForCallbackURL(request: NSURLRequest) -> Bool
    {
        let urlString = request.URL!.absoluteString
        if (typeOfAuthentication == "UNSIGNED")
        {
            // check, if auth was succesfull (check for redirect URL)
            if urlString.hasPrefix(appdele.INSTAGRAM_REDIRECT_URI)
            {
                // extract and handle access token
                let range = (urlString as NSString).rangeOfString("#access_token=")
                self.handleAuth(urlString.substringFromIndex(urlString.startIndex.advancedBy(range.location + range.length)))
                return false
            }
        }
        else
        {
            if urlString.hasPrefix(appdele.INSTAGRAM_REDIRECT_URI)
            {
                // extract and handle access token
                let range = (urlString as NSString).rangeOfString("code=")
                self.makePostRequest(urlString.substringFromIndex(urlString.startIndex.advancedBy(range.location + range.length)))
                return false
            }
        }
        return true
    }
    
    func makePostRequest(code: String)
    {
        
        let post = "client_id=\(appdele.INSTAGRAM_CLIENT_ID)&client_secret=\(appdele.INSTAGRAM_CLIENTSERCRET)&grant_type=authorization_code&redirect_uri=\(appdele.INSTAGRAM_REDIRECT_URI)&code=\(code)"
        let postData = post.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: true)!
        let postLength = "\(UInt(postData.length))"
        let requestData = NSMutableURLRequest(URL: NSURL(string: "https://api.instagram.com/oauth/access_token")!)
        requestData.HTTPMethod = "POST"
        requestData.setValue(postLength, forHTTPHeaderField: "Content-Length")
        requestData.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        requestData.HTTPBody = postData
        
        // var response: NSURLResponse? = nil
        //var responseData = try! NSURLConnection.sendSynchronousRequest(requestData, returningResponse: response)!
        var urldata=NSData()
        var requestError: NSError? = nil
        let response : AutoreleasingUnsafeMutablePointer<NSURLResponse?> = nil
        do{
            let urlData = try NSURLConnection.sendSynchronousRequest(requestData, returningResponse: response)
            urldata=urlData
            print(urlData)
        }
        catch (let e) {
            print(e)
        }
        let dict = try! NSJSONSerialization.JSONObjectWithData(urldata, options: NSJSONReadingOptions.AllowFragments)
        
        //  print("response from instagram = \(dict)")
        
        instaUserBioDict = dict.valueForKey("user") as! NSMutableDictionary
        print("instaUserBioDict = \(instaUserBioDict)")
        
        fullNameInstagram = instaUserBioDict.valueForKey("full_name") as! String
        instagramUserID = instaUserBioDict.valueForKey("id") as! String
        instaUserProfilePic = instaUserBioDict.valueForKey("profile_picture") as! String
        instaUserName = instaUserBioDict.valueForKey("username") as! String
        instaUserWebSite = instaUserBioDict.valueForKey("website") as! String
        
                 print("fullNameInstagram = \(fullNameInstagram)")
                 print("instagramUserID = \(instagramUserID)")
                 print("instaUserProfilePic = \(instaUserProfilePic)")
                 print("instaUserName = \(instaUserName)")
                 print("instaUserWebSite = \(instaUserWebSite)")
        
//        let defaults = NSUserDefaults.standardUserDefaults()
//        //defaults.setObject(self.FirstNameFacebook, forKey: "user_Firstname")
//        defaults.setObject(self.instaUserProfilePic, forKey: "userImage")
//        defaults.synchronize()
        
        
        instaAccessToken = dict.valueForKey("access_token") as! String
        
        self.handleAuth((dict.valueForKey("access_token") as! String))
    

    }
    
    func handleAuth(authToken: String)
    {
        print("successfully logged in with Token == \(authToken)")
        // self.dismissViewControllerAnimated(true, completion: { _ in })
        ConnectWithInstagramAPI()
    }

    
    
    //MARK:- ConnectWithInstagramAPI
    func ConnectWithInstagramAPI()
    {
        
        var spinningIndicator = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        spinningIndicator.labelText = NSLocalizedString("LOADING_TITLE", comment: "The Loading title")
        
         let savedUserID = NSUserDefaults.standardUserDefaults().valueForKey("userID") as! String
        
        var post = NSString(format:"user_id=%@&instagram_id=%@",savedUserID,self.instagramUserID)
        
        print("post details = \(post)")
        
        dataModel = post.dataUsingEncoding(NSASCIIStringEncoding)!
        var postLength = String(dataModel.length)
        var url = NSURL(string: "http://beta.brstdev.com/Negiysem/frontend/web/index.php/api/linkinsta")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        urlRequest.HTTPBody = dataModel
        urlRequest.setValue(postLength, forHTTPHeaderField: "Content-Length")
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data,response,error -> Void in
            
            if (error != nil)
            {
                print("\(error?.localizedDescription)")
                dispatch_async(dispatch_get_main_queue(), {
                    
                    var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_INTERNET", comment: "The no internet title"), message: NSLocalizedString("ALERT_CHECK_INTERNET", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                    alert.show()
                    
                    spinningIndicator.hide(true)
                })
            }
            else
            {
                
                var error:NSError?
                let _ : NSError?
                let jsonResult: AnyObject = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions.MutableContainers)
                
                print("jsonresult === \(jsonResult)")
                
                
                if (error != nil)
                {
                    print("\(error?.localizedDescription)")
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        var alert =  UIAlertView(title: NSLocalizedString("ALERT_NO_DATA", comment: "The no data title"), message: NSLocalizedString("ALERT_TRY_LATER", comment: "The try later title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                        alert.show()
                        
                        
                        spinningIndicator.hide(true)
                    })
                    
                }
                else
                {
                    
                    self.ConnectResultDict = jsonResult as! NSMutableDictionary
                    print("self.ConnectResultDict = \(self.ConnectResultDict)")
                    
                    let statusStr = self.ConnectResultDict.valueForKey("status") as! String
                    print("statusStr=\(statusStr)")
                    
                    if statusStr == "success"
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            NSUserDefaults.standardUserDefaults().setValue("InstaConnected", forKey: "InstaConnected")
                            
                           self.alertConnectTrue  =  UIAlertView(title: "", message: NSLocalizedString("ALERT_CONNECT_INSTA", comment: "The connect insta title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            self.alertConnectTrue.tag = 22
                            self.alertConnectTrue.show()
                            
                            spinningIndicator.hide(true)
                        })

                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            var alert =  UIAlertView(title: "", message: NSLocalizedString("ALERT_CONNECT_FALSE", comment: "The check internet title"), delegate: self, cancelButtonTitle: NSLocalizedString("ALERT_OK", comment: "The ok title"))
                            alert.show()
                            
                            spinningIndicator.hide(true)
                        })
                    }
                    
                    
                }
            }
            
            
        })
        task.resume()
        
        
    }
    
    
    
    @IBAction func BackBtnMethod(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if alertConnectTrue.tag == 22
        {
            self.navigationController?.popViewControllerAnimated(false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
